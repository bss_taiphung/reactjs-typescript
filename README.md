## System Requirement

| Requirement | Version |
| ----------- | ------- |
| `node -v`   | v12     |
| `yarn -v`   | 1.19.2  |
| `npm -v`    | v6      |

- [Quick Start](https://medium.com/@nasterblue/react-boilerplate-cra-template-quickstart-94e2d96cb61b)
- [Generators](https://medium.com/@nasterblue/generators-9ce22cba845e)
- [How to commit source code](https://medium.com/@nasterblue/react-boilerplate-cra-template-how-to-commit-code-to-git-731c0eb222a)

## Context

- [Architecture](https://medium.com/@nasterblue/react-boilerplate-cra-template-architecture-containers-c76c6ed2c646)
- [Container](https://medium.com/@nasterblue/react-boilerplate-cra-template-usage-redux-saga-in-container-b589d8eb86f6)
- [Loadable](https://medium.com/@nasterblue/react-boilerplate-cra-template-loading-components-with-react-lazy-and-suspense-5c50d01d5dda)
- [Saga](https://medium.com/@nasterblue/react-boilerplate-cra-template-saga-usage-example-ea94b46121b4)
- [Reducer](https://medium.com/@nasterblue/react-boilerplate-cra-template-redux-toolkit-reducer-79365c57212c)
- [Types](https://medium.com/@nasterblue/react-boilerplate-cra-template-types-da0393febe75)
- [Validation Schema Form](https://medium.com/@nasterblue/react-boilerplate-cra-template-validation-schema-form-fcf9c52e6c53)
- [Styling](https://medium.com/@nasterblue/react-boilerplate-cra-template-styling-a53da0d956d5)
