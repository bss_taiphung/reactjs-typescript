module.exports = {
  apps: [
    {
      name: 'coliving-frontend',
      script: 'serve -l 5611 -s build',
      args: 'one two',
      instances: 1,
      autorestart: true,
      watch: true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
      },
      out_file: '/dev/null',
      error_file: '/dev/null',
    },
  ],
};
