var filesystem = require('fs');

var _getAllFilesFromFolder = function (dir, des) {
  var results = [];
  filesystem.readdirSync(dir).forEach(function (file) {
    file = dir + '/' + file;
    des = des + '/' + file;

    var stat = filesystem.statSync(file);

    if (stat && stat.isDirectory()) {
      results = results.concat(_getAllFilesFromFolder(file, des));
    } else {
      filesystem.copyFile(file, des, err => {
        if (err) throw err;
        console.log(`copy from ${file} to ${des}\n`);
      });
      results.push(file);
    }
  });
  return results;
};
_getAllFilesFromFolder('./app', './test');
