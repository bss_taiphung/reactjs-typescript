/**
 *
 * Coffee
 *
 */

import React, { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey } from './slice';
import { selectCoffee } from './selectors';
import { coffeeSaga } from './saga';

interface Props {}

export const Coffee = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: coffeeSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const coffee = useSelector(selectCoffee);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <>
      <div>{t('')}</div>
    </>
  );
});
