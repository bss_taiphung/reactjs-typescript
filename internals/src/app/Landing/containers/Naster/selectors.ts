import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.naster || initialState;

export const selectNaster = createSelector(
  [selectDomain],
  nasterState => nasterState,
);
