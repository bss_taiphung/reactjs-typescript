# Loading components with React.lazy and Suspense

This is the content of the file by default:

```JS
import { lazyLoad } from 'utils/loadable';
import React from 'react';

export const ProductDetailPage = lazyLoad(
  () => import('./index'),
  module => module.ProductDetailPage
);

```

In this case, the app won't show anything while loading your component.
You can however make it display a custom loader with:

```JS
import { lazyLoad } from 'utils/loadable';
import { LoadingIndicator } from 'app/components/LoadingIndicator';
import React from 'react';

export const ProductDetailPage = lazyLoad(
  () => import('./index'),
  module => module.ProductDetailPage,
  {
    fallback: <LoadingIndicator />,
  },
);

```
