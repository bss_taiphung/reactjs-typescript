## Architecture: `containers`

Example: `my-app/src/app/containers/ProductDetailPage`

```shell script
my-app
  +src
    +app
      +containers
        +ProductDetailPage
          + Loadable.ts
          + index.tsx
          + saga.ts
          + schema.ts
          + selectors.ts
          + slice.ts
          + types.ts
```
