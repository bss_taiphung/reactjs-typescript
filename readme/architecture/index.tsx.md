# Usage redux-saga in container

## Step 1 - Inject reducer/saga

```typescript jsx
import React, { memo, useEffect, useState } from 'react';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { productDetailPageSaga } from './saga';
import { actions, reducer, sliceKey } from './slice';
export const ProductDetailPage = compose(memo)((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: productDetailPageSaga });
});
```

## Step 2 -Dispatch action to store

```typescript jsx
import { useDispatch, useSelector } from 'react-redux';
import { actions, reducer, sliceKey } from './slice';
export const ProductDetailPage = compose(memo)((props: Props) => {
  const dispatch = useDispatch();
  dispatch(actions.loadProductDetail(id));
});
```

## Step 3 - Retrieve the result from API response

```typescript jsx
import { selectProductDetailPage } from './selectors';
import { useDispatch, useSelector } from 'react-redux';
export const ProductDetailPage = compose(memo)((props: Props) => {
  const { productDetail } = useSelector(selectProductDetailPage);
});
```
