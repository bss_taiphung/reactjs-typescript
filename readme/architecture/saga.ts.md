# Saga Usage Example

- Suppose we have a UI to show the product detail
- The fresh page will fetch the product detail result via API

```typescript jsx
export const ProductDetailPage = compose(
  memo,
)((props: Props) => {
  const { id = '' } = useParams();
  useEffect(() => {
    dispatch(actions.loadProductDetail(id));
  }, [id]);
  );
});

```

## sagas.js

- Allows concurrent: use takeEvery
- Does not allow concurrent: use takeLatest

```typescript jsx
import { put, takeLatest, takeEvery } from 'redux-saga/effects';
export function* getProductDetail(data: PayloadAction<any>) {
  try {
    const { id = '' } = data.payload;
    const response = yield call(
      axiosClient.get,
      `/product/productDetail/${id}`,
    );
    yield put(actions.loadProductDetailSuccess(response?.data));
  } catch (e) {
    if (!isEmpty(e)) {
      yield put(actions.loadProductDetailFail(e));
    }
  }
}

export function* productDetailPageSaga() {
  yield takeLatest(actions.loadProductDetail.type, getProductDetail);
}
```
