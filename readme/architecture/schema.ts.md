# Validation schema

```typescript jsx
import * as yup from 'yup';
import i18n from 'i18next';
import { translations } from 'locales/i18n';
import { actions, reducer, sliceKey } from './slice';
export const schema = (min = 0) =>
  yup.object().shape({
    firstName: yup.required(
      `${i18n.t(translations[sliceKey].firstName.required)}`,
    ),
    age: yup
      .number()
      .positive()
      .integer()
      .required(`${i18n.t(translations[sliceKey].age.required)}`),
  });
```

## Usage validation schema in react-hook-form

```typescript jsx
import React from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import schema from './schema';

export default function App() {
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });
  const onSubmit = data => console.log(data);
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input type="text" name="firstName" ref={register} />
      <p>{errors.firstName?.message}</p>

      <input type="text" name="age" ref={register} />
      <p>{errors.age?.message}</p>

      <input type="submit" />
    </form>
  );
}
```
