# Redux-toolkit - reducer

## Writing the Slice Reducer

```typescript jsx
export const initialState: ContainerState = {
  isLoading: true,
  productDetail: {
    id: '',
    product_name: '',
    price_start: 0,
    price_end: 0,
    is_international: false,
    productToVariants: [],
    productToImages: [],
    productToDocuments: [],
    summary: [],
    company: {
      id: '',
      companyName: '',
    },
    min_buying_qty: 0,
    unit: '',
  },
};
const productDetailPageSlice = createSlice({
  name: 'productDetailPage',
  initialState,
  reducers: {
    loadProductDetail: {
      reducer: () => initialState,
      prepare(id: string) {
        return { payload: { id } };
      },
    },
    loadProductDetailSuccess(state, action: PayloadAction<any>) {
      state.isLoading = false;
      set(state, 'productDetail', action.payload);
    },
    loadProductDetailFail(state, action: PayloadAction<any>) {
      state.isLoading = false;
    },
  },
});

export const { actions, reducer, name: sliceKey } = productDetailPageSlice;
```

## Custom params the Slice Reducer

```typescript jsx
const productDetailPageSlice = createSlice({
  name: 'productDetailPage',
  initialState,
  reducers: {
    loadProductDetail: {
      reducer: () => initialState,
      prepare(id: string) {
        return { payload: { id } };
      },
    },
  },
});

export const { actions, reducer, name: sliceKey } = productDetailPageSlice;
```
