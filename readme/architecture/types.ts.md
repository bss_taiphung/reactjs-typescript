## Interfaces of container

```typescript jsx
import { ICartState, IProductState } from 'types';

export interface ProductDetailPageState {
  productDetail: IProductState;
  error: null;
  relatedProducts: Array<IProductState>;
  errorGetRelatedProducts: null;
  isLoading: boolean;
  expandShippingInfo: boolean;
  openOffer: boolean;
  openReport: boolean;
  openSendReportSuccess: boolean;
  openOrderSharing: boolean;
  dataOffer?: ICartState;
}

export type ContainerState = ProductDetailPageState;
```
