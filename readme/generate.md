# Generators

**1)** Auto generate boilerplate code

# npm run generate

```Shell
➜  my-app git:(master) ✗ npm run generate

> my-app@1.0.1 generate /Volumes/DATA/resources/my-app
> cross-env TS_NODE_PROJECT='./internals/ts-node.tsconfig.json' plop --plopfile internals/generators/plopfile.ts

? [PLOP] Please choose a generator. (Use arrow keys)
❯ component - Add an unconnected component
  container - Add a container component
```

**2)** Generate a container

# npm run generate component

```Shell
➜  my-app git:(master) ✗ npm run generate container

> my-app@1.0.1 generate /Volumes/DATA/resources/my-app
> cross-env TS_NODE_PROJECT='./internals/ts-node.tsconfig.json' plop --plopfile internals/generators/plopfile.ts "container"

? What should it be called? (Form)
```

**3)** Generate a component

# npm run generate component

```Shell
➜  my-app git:(master) ✗ npm run generate component

> my-app@1.0.1 generate /Volumes/DATA/resources/my-app
> cross-env TS_NODE_PROJECT='./internals/ts-node.tsconfig.json' plop --plopfile internals/generators/plopfile.ts "component"

? What should it be called? (Button)
```

**4)** Example Generate signup page: `/signup`

- Run commandline

```shell script
npm run generate container
```

- Type `SignUpPage` at `What should it be called?` question

- Import `SignUpPage` container into `my-app/src/app/containers/App/index.tsx`
- Add new signup route
- Navigate browser to : `http://localhost:3000/sign-up`

```shell script
import { Router } from 'react-router';
import { Switch, Route } from 'react-router-dom';
import { SignUpPage } from '../SignUpPage/Loadable';
import { HomePage } from '../HomePage/Loadable';
export function App() {
  return (
    <>
      <Router history={history}>
        <Switch>
          <Route  exact  path={'/'}   component={HomePage} />
          <Route  exact  path={'/signup'}   component={SignUpPage} />
        </Switch>
     </Router>
    </>
  );
}

```
