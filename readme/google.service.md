# Setup service google (firebase)

1. Go to https://console.firebase.google.com/ and choose a project
2. In left side bar:

- Go Authentication tab

- Choose Sign-in method

- Enable Google provider
  ![Alt text](images/googl-enable.png?raw=true 'Title')

- Add your domain you want to deploy
  ![Alt text](images/domain.png?raw=true 'Title')
