# Quickstart

**1)** Yarn install

```shell
cd my-app
yarn install
```

**2)** Start dev

```shell
npm start
```

The website is hosted at `http://localhost:3000` is default

Config port at `my-app/.env.local`

**3)** Start production

```shell
pm2 start ecosystem.config
```

Config port at `my-app/ecosystem.config.js`
