# Styling

## [Document](https://styled-components.com/)

## Theme

**1)** Config style color by theme : `my-app/src/styles/theme/themes.ts`

```typescript
const lightTheme = {
  primary: 'rgba(215,113,88,1)',
  text: 'rgba(58,52,51,1)',
  textSecondary: 'rgba(58,52,51,0.7)',
  background: 'rgba(255,255,255,1)',
  backgroundVariant: 'rgba(251,249,249,1)',
  border: 'rgba(58,52,51,0.12)',
  borderLight: 'rgba(58,52,51,0.05)',
  ui1ColorFillColor1: '#2a2c30',
  ui1ColorFillColor2: '#03c3ec',
  ui1ColorFillColor3: '#feae43',
  ui1ColorFillColor4: '#f85c1e',
  ui1ColorFillColor5: '#168b99',
  ui2GreysGrey1: '#4a505b',
  ui2GreysGrey2: '#778699',
  ui2GreysGrey3: '#d7e1ea',
  ui2GreysGrey4: '#f0f4f8',
  ui2GreysGrey5: '#fafbfd',
  uiWhite: '#ffffff',
  ui4ServiceColorsError: '#ffffff',
  darkGrey: '#2a2c30',
  ui2ColorFillColor2: '#f73838',
};

const darkTheme: Theme = Object.assign({}, lightTheme);

export type Theme = typeof lightTheme;

export const themes = {
  light: lightTheme,
  dark: darkTheme,
};
```

**2)** Config style constants style: `my-app/src/styles/StyleConstants.ts`

```typescript jsx
export enum StyleConstants {
  NAV_BAR_HEIGHT = '4rem',
  REACT_APP_WIDTH = 1110,
  DF_FS = 14,
}

export function fConvertPxToRem(pixel: number): string {
  return `${pixel / StyleConstants.DF_FS}rem`;
}
```

## Create style for a component/container

**1)** Navigate to `my-app/src/styles`

**2)** Create `filename.styles.ts`

**3)** Style

**4)** Example

- Create `styles/components/add.item.styles`

```typescript
import { createGlobalStyle } from 'styled-components';
// import { StyleConstants } from '../StyleConstants';

export const AddItemStyles = createGlobalStyle`
.add-item-wrapper {
  outline: none;
  height: 100%;
  cursor: pointer;
  //border-radius: 8px;
  background-color: ${p => p.theme.ui2GreysGrey4};
  font-size: 14px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.57;
  letter-spacing: normal;
  text-align: center;
  color: ${p => p.theme.ui2GreysGrey2};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  .icon-wrapper {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: ${p => p.theme.ui2GreysGrey2};
    margin-bottom: 12px;
  }
}
`;
```

- Usage style `styles/components/add.item.styles` in components/containers

```typescript jsx
import { AddItemStyles } from 'styles/components/add.item.styles';
export default function AddItem(props: Props) {
  return (
    <>
      <div className="add-item-wrapper">
        <div>{props.label}</div>
      </div>
      <AddItemStyles />
    </>
  );
}
```
