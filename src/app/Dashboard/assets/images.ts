import logo_white from 'app/Dashboard/assets/images/logos/logo--white.svg';
import logo_dark from 'app/Dashboard/assets/images/logos/logo--dark.svg';
import avatar_1 from 'app/Dashboard/assets/images/avatars/avatar_1.png';
import avatar_2 from 'app/Dashboard/assets/images/avatars/avatar_2.png';
import avatar_3 from 'app/Dashboard/assets/images/avatars/avatar_3.png';
import avatar_4 from 'app/Dashboard/assets/images/avatars/avatar_4.png';
import avatar_5 from 'app/Dashboard/assets/images/avatars/avatar_5.png';
import avatar_6 from 'app/Dashboard/assets/images/avatars/avatar_6.png';
import avatar_7 from 'app/Dashboard/assets/images/avatars/avatar_7.png';
import avatar_8 from 'app/Dashboard/assets/images/avatars/avatar_8.png';
import avatar_9 from 'app/Dashboard/assets/images/avatars/avatar_9.png';
import avatar_10 from 'app/Dashboard/assets/images/avatars/avatar_10.png';
import avatar_11 from 'app/Dashboard/assets/images/avatars/avatar_11.png';
import avatar_12 from 'app/Dashboard/assets/images/avatars/avatar_12.png';
import post_1 from 'app/Dashboard/assets/images/posts/post_1.jpg';
import cover_1 from 'app/Dashboard/assets/images/covers/cover_1.jpg';
import cover_2 from 'app/Dashboard/assets/images/covers/cover_2.jpg';
import presentation_header from 'app/Dashboard/assets/images/presentation/header.jpg';
import presentation_plugins_support from 'app/Dashboard/assets/images/presentation/plugins_support.png';
import presentation_source_files from 'app/Dashboard/assets/images/presentation/source_files.png';
import presentation_user_flows from 'app/Dashboard/assets/images/presentation/user_flows.png';
import product_agency from 'app/Dashboard/assets/images/products/product_agency.svg';
import product_agency_outlined from 'app/Dashboard/assets/images/products/product_agency--outlined.svg';
import product_enterprise from 'app/Dashboard/assets/images/products/product_enterprise.svg';
import product_freelancer from 'app/Dashboard/assets/images/products/product_freelancer.svg';
import project_1 from 'app/Dashboard/assets/images/projects/project_1.jpg';
import project_2 from 'app/Dashboard/assets/images/projects/project_2.jpg';
import image_auth from 'app/Dashboard/assets/images/auth.png';
import image_react from 'app/Dashboard/assets/images/react.png';
import undraw_add_file2_gvbb from 'app/Dashboard/assets/images/undraw_add_file2_gvbb.svg';
import undraw_authentication_fsn5 from 'app/Dashboard/assets/images/undraw_authentication_fsn5.svg';
import undraw_cookie_love_ulvn from 'app/Dashboard/assets/images/undraw_cookie_love_ulvn.svg';
import undraw_empty_xct9 from 'app/Dashboard/assets/images/undraw_empty_xct9.svg';
import undraw_growth_analytics_8btt from 'app/Dashboard/assets/images/undraw_growth_analytics_8btt.svg';
import undraw_no_data_qbuo from 'app/Dashboard/assets/images/undraw_no_data_qbuo.svg';
import undraw_page_not_found_su7k from 'app/Dashboard/assets/images/undraw_page_not_found_su7k.svg';
import undraw_resume_folder_2_arse from 'app/Dashboard/assets/images/undraw_resume_folder_2_arse.svg';
import undraw_server_down_s4lk from 'app/Dashboard/assets/images/undraw_server_down_s4lk.svg';
import undraw_work_chat_erdt from 'app/Dashboard/assets/images/undraw_work_chat_erdt.svg';

export {
  logo_white,
  logo_dark,
  avatar_1,
  avatar_2,
  avatar_3,
  avatar_4,
  avatar_5,
  avatar_6,
  avatar_7,
  avatar_8,
  avatar_9,
  avatar_10,
  avatar_11,
  avatar_12,
  post_1,
  cover_1,
  cover_2,
  presentation_header,
  presentation_plugins_support,
  presentation_source_files,
  presentation_user_flows,
  product_agency,
  product_agency_outlined,
  product_enterprise,
  product_freelancer,
  project_1,
  project_2,
  image_auth,
  image_react,
  undraw_add_file2_gvbb,
  undraw_authentication_fsn5,
  undraw_cookie_love_ulvn,
  undraw_empty_xct9,
  undraw_growth_analytics_8btt,
  undraw_no_data_qbuo,
  undraw_page_not_found_su7k,
  undraw_resume_folder_2_arse,
  undraw_server_down_s4lk,
  undraw_work_chat_erdt,
};
