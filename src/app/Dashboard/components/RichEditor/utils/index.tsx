import { blockRenderMap, setBlockData } from './block';

export { setBlockData, blockRenderMap };
