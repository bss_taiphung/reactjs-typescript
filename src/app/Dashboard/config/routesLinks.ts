const authRoot = '/auth';
const authLogin = 'login';
const authRegister = 'register';

const dashboardRoot = '/dashboard';
const management = 'management';
const customers = 'customers';
const orders = 'orders';
const presentation = 'presentation';
const profile = 'profile';
const projects = 'projects';
const settings = 'settings';
const invoices = 'invoices';
const analytics = 'analytics';
const chat = 'chat';
const overview = 'overview';
const mail = 'mail';
const kanbanBoard = 'kanban-board';
const calendar = 'calendar';
const socialFeed = 'social-feed';
const gettingStarted = 'getting-started';
const changelog = 'changelog';
const errors = 'errors';
const errors401 = 'error-401';
const errors404 = 'error-404';
const errors500 = 'error-500';

const routeLinks = {
  authRoot: authRoot,
  authLogin: `${authRoot}/${authLogin}`,
  authRegister: `${authRoot}/${authRegister}`,

  dashboardRoot: dashboardRoot,
  dashboardDefault: `${dashboardRoot}/default`,
  calendar: `${dashboardRoot}/${calendar}`,
  changelog: `${dashboardRoot}/${changelog}`,
  chat: `${dashboardRoot}/${chat}`,
  chatId: `${dashboardRoot}/${chat}/:id`,
  analytics: `${dashboardRoot}/${analytics}`,
  invoices: `${dashboardRoot}/${invoices}`,
  invoicesId: `${dashboardRoot}/${invoices}/:id`,
  kanbanBoard: `${dashboardRoot}/${kanbanBoard}`,
  mail: `${dashboardRoot}/${mail}`,
  managementCustomers: `${dashboardRoot}/${management}/${customers}`,
  managementCustomersId: `${dashboardRoot}/${management}/${customers}/:id`,
  managementCustomersIdTab: `${dashboardRoot}/${management}/${customers}/:id/:tab`,
  managementProjects: `${dashboardRoot}/${management}/${projects}`,
  managementOrders: `${dashboardRoot}/${management}/${orders}`,
  managementOrdersId: `${dashboardRoot}/${management}/${orders}/:id`,
  overview: `${dashboardRoot}/${overview}`,
  presentation: `${dashboardRoot}/${presentation}`,
  profile: `${dashboardRoot}/${profile}/`,
  profileId: `${dashboardRoot}/${profile}/:id`,
  profileIdTab: `${dashboardRoot}/${profile}/:id/:tab`,
  projects: `${dashboardRoot}/${projects}`,
  projectsCreate: `${dashboardRoot}/${projects}/create`,
  projectsId: `${dashboardRoot}/${projects}/:id`,
  projectsIdTab: `${dashboardRoot}/${projects}/:id/:tab`,
  settings: `${dashboardRoot}/${settings}`,
  settingsTab: `${dashboardRoot}/${settings}/:tab`,
  socialFeed: `${dashboardRoot}/${socialFeed}`,
  gettingStarted: `${dashboardRoot}/${gettingStarted}`,
  errors: `${dashboardRoot}/${errors}`,
  errors401: `${dashboardRoot}/${errors}/${errors401}`,
  errors404: `${dashboardRoot}/${errors}/${errors404}`,
  errors500: `${dashboardRoot}/${errors}/${errors500}`,
};

export default routeLinks;
