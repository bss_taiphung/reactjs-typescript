import React, { lazy } from 'react';
import { Redirect } from 'react-router-dom';
import routeLinksDashboard from 'app/Dashboard/config/routesLinks';
import { DashboardLayout } from 'app/Layout/DashboardLayout/Loadable';
import DashboardAnalyticsView from 'app/Dashboard/views/DashboardAnalytics';
import DashboardDefaultView from 'app/Dashboard/views/DashboardDefault';
import OverviewView from 'app/Dashboard/views/Overview';
import PresentationView from 'app/Dashboard/views/Presentation';
import { KanbanBoard } from 'app/Dashboard/views/KanbanBoard/Loadable';
import Calendar from 'app/Dashboard/views/Calendar';
import Changelog from 'app/Dashboard/views/Changelog';
import Chat from 'app/Dashboard/views/Chat';
import InvoiceDetails from 'app/Dashboard/views/InvoiceDetails';
import Mail from 'app/Dashboard/views/Mail';
import CustomerManagementList from 'app/Dashboard/views/CustomerManagementList';
import CustomerManagementDetails from 'app/Dashboard/views/CustomerManagementDetails';
import ProjectManagementList from 'app/Dashboard/views/ProjectManagementList';
import OrderManagementList from 'app/Dashboard/views/OrderManagementList';
import OrderManagementDetails from 'app/Dashboard/views/OrderManagementDetails';
import Profile from 'app/Dashboard/views/Profile';
import ProjectCreate from 'app/Dashboard/views/ProjectCreate';
import ProjectDetails from 'app/Dashboard/views/ProjectDetails';
import ProjectList from 'app/Dashboard/views/ProjectList';
import Settings from 'app/Dashboard/views/Settings';
import SocialFeed from 'app/Dashboard/views/SocialFeed';
import GettingStarted from 'app/Dashboard/views/GettingStarted';
import Login from 'app/Dashboard/views/Login';
import Register from 'app/Dashboard/views/Register';
import AuthLayout from '../Layout/AuthLayout/AuthLayout';

const dashboardRoutes = [
  {
    path: routeLinksDashboard.authRoot,
    component: AuthLayout,
    routes: [
      {
        path: routeLinksDashboard.authLogin,
        exact: true,
        component: Login,
      },
      {
        path: routeLinksDashboard.authRegister,
        exact: true,
        component: Register,
      },
    ],
  },
  {
    path: routeLinksDashboard.dashboardRoot,
    component: DashboardLayout,
    routes: [
      {
        path: '/',
        exact: true,
        component: DashboardDefaultView,
      },
      {
        path: routeLinksDashboard.calendar,
        exact: true,
        component: Calendar,
      },
      {
        path: routeLinksDashboard.changelog,
        exact: true,
        component: Changelog,
      },
      {
        path: routeLinksDashboard.chat,
        exact: true,
        component: Chat,
      },
      {
        path: routeLinksDashboard.chatId,
        exact: true,
        component: Chat,
      },
      {
        path: routeLinksDashboard.analytics,
        exact: true,
        component: DashboardAnalyticsView,
      },
      {
        path: routeLinksDashboard.invoicesId,
        exact: true,
        component: InvoiceDetails,
      },
      {
        path: routeLinksDashboard.kanbanBoard,
        exact: true,
        component: KanbanBoard,
      },
      {
        path: routeLinksDashboard.mail,
        exact: true,
        component: Mail,
      },
      {
        path: routeLinksDashboard.managementCustomers,
        exact: true,
        component: CustomerManagementList,
      },
      {
        path: routeLinksDashboard.managementCustomersId,
        exact: true,
        component: CustomerManagementDetails,
      },
      {
        path: routeLinksDashboard.managementCustomersIdTab,
        exact: true,
        component: CustomerManagementDetails,
      },
      {
        path: routeLinksDashboard.managementProjects,
        exact: true,
        component: ProjectManagementList,
      },
      {
        path: routeLinksDashboard.managementOrders,
        exact: true,
        component: OrderManagementList,
      },
      {
        path: routeLinksDashboard.managementOrdersId,
        exact: true,
        component: OrderManagementDetails,
      },
      {
        path: routeLinksDashboard.overview,
        exact: true,
        component: OverviewView,
      },
      {
        path: routeLinksDashboard.presentation,
        exact: true,
        component: PresentationView,
      },
      {
        path: routeLinksDashboard.profileId,
        exact: true,
        component: Profile,
      },
      {
        path: routeLinksDashboard.profileIdTab,
        exact: true,
        component: Profile,
      },
      {
        path: routeLinksDashboard.projectsCreate,
        exact: true,
        component: ProjectCreate,
      },
      {
        path: routeLinksDashboard.projectsId,
        exact: true,
        component: ProjectDetails,
      },
      {
        path: routeLinksDashboard.projectsIdTab,
        exact: true,
        component: ProjectDetails,
      },
      {
        path: routeLinksDashboard.projects,
        exact: true,
        component: ProjectList,
      },
      {
        path: routeLinksDashboard.settings,
        exact: true,
        component: Settings,
      },
      {
        path: routeLinksDashboard.settingsTab,
        exact: true,
        component: Settings,
      },
      {
        path: routeLinksDashboard.socialFeed,
        exact: true,
        component: SocialFeed,
      },
      {
        path: routeLinksDashboard.gettingStarted,
        exact: true,
        component: GettingStarted,
      },
      // {
      //   component: () => <Redirect to="/errors/error-404" />,
      // },
    ],
  },

  // {
  //   component: NotFoundPage,
  // },
];
export default dashboardRoutes;
