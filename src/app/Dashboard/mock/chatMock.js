import { v1 as uuid } from 'uuid';
import moment from 'moment';

import mock from 'app/Dashboard/utils/mock';
import {
  avatar_10,
  avatar_11,
  avatar_12,
  avatar_2,
  avatar_3,
  avatar_5,
  avatar_6,
  avatar_7,
  avatar_8,
  avatar_9,
} from 'app/Dashboard/assets/images';
import { project_1 } from '../assets/images';

mock.onGet('/api/chat/conversations').reply(200, {
  conversations: [
    {
      id: uuid(),
      otherUser: {
        name: 'Adam Denisov',
        avatar: avatar_7,
        active: true,
        lastActivity: moment(),
      },
      messages: [
        {
          id: uuid(),
          sender: {
            authUser: false,
            name: 'Adam Denisov',
            avatar: avatar_7,
            lastActivity: moment(),
          },
          content:
            "Hey, nice projects! I really liked the one in react. What's your quote on kinda similar project?",
          contentType: 'text',
          created_at: moment().subtract(10, 'hours'),
        },
        {
          id: uuid(),
          sender: {
            authUser: true,
            name: 'Shen Zhi',
            avatar: avatar_11,
          },
          content:
            'I would need to know more details, but my hourly rate stats at $35/hour. Thanks!',
          contentType: 'text',
          created_at: moment().subtract(2, 'hours'),
        },
        {
          id: uuid(),
          sender: {
            authUser: false,
            name: 'Adam Denisov',
            avatar: avatar_7,
          },
          content:
            "Well it's a really easy one, I'm sure we can make it half of the price.",
          contentType: 'text',
          created_at: moment().subtract(5, 'minutes'),
        },
        {
          id: uuid(),
          sender: {
            authUser: true,
            name: 'Shen Zhi',
            avatar: avatar_11,
          },
          content:
            "Then why don't you make it if it's that easy? Sorry I'm not interetes, have fantastic day Adam!",
          contentType: 'text',
          created_at: moment().subtract(3, 'minutes'),
        },
        {
          id: uuid(),
          sender: {
            authUser: false,
            name: 'Adam Denisov',
            avatar: avatar_7,
          },
          content: 'Last offer, $25 per hour',
          contentType: 'text',
          created_at: moment().subtract(1, 'minute'),
        },
        {
          id: uuid(),
          sender: {
            authUser: false,
            name: 'Adam Denisov',
            avatar: avatar_7,
          },
          content: project_1,
          contentType: 'image',
          created_at: moment().subtract(1, 'minute'),
        },
      ],
      unread: 0,
      created_at: moment().subtract(1, 'minute'),
    },
    {
      id: uuid(),
      otherUser: {
        name: 'Ekaterina Tankova',
        avatar: avatar_2,
        active: true,
        lastActivity: moment(),
      },
      messages: [
        {
          id: uuid(),
          sender: {
            authUser: true,
            name: 'Shen Zhi',
            avatar: avatar_11,
          },
          content: 'Hey, would you like to collaborate?',
          contentType: 'text',
          created_at: moment().subtract(6, 'minutes'),
        },
        {
          id: uuid(),
          sender: {
            authUser: false,
            name: 'Ekaterina Tankova',
            avatar: avatar_2,
          },
          content: "I don't think that's ideal",
          contentType: 'text',
          created_at: moment().subtract(5, 'minutes'),
        },
      ],
      unread: 3,
      created_at: moment().subtract(26, 'minutes'),
    },
    {
      id: uuid(),
      otherUser: {
        name: 'Emilee Simchenko',
        avatar: avatar_9,
        active: false,
        lastActivity: moment().subtract(2, 'minutes'),
      },
      messages: [
        {
          id: uuid(),
          sender: {
            authUser: false,
            name: 'Emilee Simchenko',
            avatar: avatar_9,
          },
          content: 'Hi Shen, we should submit the product today',
          contentType: 'text',
          created_at: moment().subtract(2, 'hours'),
        },
        {
          id: uuid(),
          sender: {
            authUser: true,
            name: 'Shen Zhi',
            avatar: avatar_11,
          },
          content: 'Oh, totally forgot about it',
          contentType: 'text',
          created_at: moment().subtract(1, 'hour').subtract(2, 'minutes'),
        },
        {
          id: uuid(),
          sender: {
            authUser: true,
            name: 'Shen Zhi',
            avatar: avatar_11,
          },
          content: 'Alright then',
          contentType: 'text',
          created_at: moment().subtract(1, 'hour'),
        },
      ],
      unread: 0,
      created_at: moment().subtract(3, 'hours'),
    },
    {
      id: uuid(),
      otherUser: {
        name: 'Kwak Seong-Min',
        avatar: avatar_10,
        active: true,
        lastActivity: moment(),
      },
      messages: [
        {
          id: uuid(),
          sender: {
            authUser: true,
            name: 'Shen Zhi',
            avatar: avatar_11,
          },
          content:
            "Hi Kwak! I've seen your projects and we can work together on a project. Will send you the details later.",
          contentType: 'text',
          created_at: moment().subtract(3, 'days'),
        },
        {
          id: uuid(),
          sender: {
            authUser: false,
            name: 'Kwak Seong-Min',
            avatar: avatar_10,
          },
          content: "Haha, right, we'll do it",
          contentType: 'text',
          created_at: moment().subtract(2, 'days'),
        },
      ],
      unread: 1,
      created_at: moment().subtract(2, 'days'),
    },
    {
      id: uuid(),
      otherUser: {
        name: 'Cao Yu',
        avatar: avatar_3,
        active: false,
        lastActivity: moment().subtract(4, 'hours'),
      },
      messages: [
        {
          id: uuid(),
          sender: {
            authUser: true,
            name: 'Shen Zhi',
            avatar: avatar_11,
          },
          content: 'Did you receive my email about the brief?',
          contentType: 'text',
          created_at: moment().subtract(3, 'days'),
        },
        {
          id: uuid(),
          sender: {
            authUser: false,
            name: 'Cao Yu',
            avatar: avatar_3,
          },
          content: "I'm not sure, but I will check it later",
          contentType: 'text',
          created_at: moment().subtract(2, 'days'),
        },
      ],
      unread: 0,
      created_at: moment().subtract(5, 'days'),
    },
    {
      id: uuid(),
      otherUser: {
        name: 'Clarke Gillebert',
        avatar: avatar_6,
        active: true,
        lastActivity: moment(),
      },
      messages: [
        {
          id: uuid(),
          sender: {
            authUser: false,
            name: 'Clarke Gillebert',
            avatar: avatar_6,
          },
          content: 'Hey Shen! I love your projects!!!',
          contentType: 'text',
          created_at: moment().subtract(2, 'days'),
        },
        {
          id: uuid(),
          sender: {
            authUser: true,
            name: 'Shen Zhi',
            avatar: avatar_11,
          },
          content: "Haha thank you Clarke, I'm doing our best",
          contentType: 'text',
          created_at: moment().subtract(3, 'days'),
        },
      ],
      unread: 0,
      created_at: moment().subtract(5, 'days'),
    },
  ],
});

mock.onGet('/api/chat/activity').reply(200, {
  groups: [
    {
      id: 'clients',
      name: 'Clients',
    },
    {
      id: 'friends',
      name: 'Friends',
    },
  ],
  connections: [
    {
      id: uuid(),
      name: 'Ekaterina Tankova',
      avatar: avatar_2,
      active: false,
      lastActivity: moment().subtract(24, 'minutes'),
      group: 'clients',
    },
    {
      id: uuid(),
      name: 'Cao Yu',
      avatar: avatar_3,
      active: true,
      lastActivity: moment(),
      group: 'clients',
    },
    {
      id: uuid(),
      name: 'Anje Keizer',
      avatar: avatar_5,
      active: false,
      lastActivity: moment().subtract(1, 'minutes'),
      group: 'clients',
    },
    {
      id: uuid(),
      name: 'Ava Gregoraci',
      avatar: avatar_8,
      active: true,
      lastActivity: moment(),
      group: 'clients',
    },
    {
      id: uuid(),
      name: 'Clarke Gillebert',
      avatar: avatar_6,
      active: true,
      lastActivity: moment(),
      group: 'friends',
    },
    {
      id: uuid(),
      name: 'Adam Denisov',
      avatar: avatar_7,
      active: false,
      lastActivity: moment().subtract(24, 'minutes'),
      group: 'friends',
    },
    {
      id: uuid(),
      name: 'Emilee Simchenko',
      avatar: avatar_9,
      active: true,
      lastActivity: moment(),
      group: 'friends',
    },
    {
      id: uuid(),
      name: 'Kwak Seong-Min',
      avatar: avatar_10,
      active: true,
      lastActivity: moment(),
      group: 'friends',
    },
    {
      id: uuid(),
      name: 'Shen Zhi',
      avatar: avatar_11,
      active: true,
      lastActivity: moment(),
      group: 'friends',
    },
    {
      id: uuid(),
      name: 'Merrile Burgett',
      avatar: avatar_12,
      active: false,
      lastActivity: moment().subtract(2, 'days'),
    },
  ],
});
