import { v1 as uuid } from 'uuid';
import moment from 'moment';

import mock from 'app/Dashboard/utils/mock';
import {
  avatar_10,
  avatar_11,
  avatar_12,
  avatar_2,
  avatar_3,
  avatar_4,
  avatar_5,
  avatar_6,
  avatar_7,
  avatar_8,
  avatar_9,
} from '../assets/images';

mock.onGet('/api/management/customers').reply(200, {
  customers: [
    {
      id: uuid(),
      name: 'Ekaterina Tankova',
      email: 'ekaterina.tankova@admin-react-material-ui-theme.io',
      avatar: avatar_2,
      spent: '500.00',
      currency: '$',
      type: 'Agency',
      projects: '1',
      rating: 5,
      location: 'West Virginia, USA',
    },
    {
      id: uuid(),
      name: 'Cao Yu',
      email: 'cao.yu@admin-react-material-ui-theme.io',
      avatar: avatar_3,
      spent: '300.00',
      type: 'Freelancer',
      currency: '$',
      projects: '3',
      rating: 4.3,
      location: 'Bristow',
    },
    {
      id: uuid(),
      name: 'Alexa Richardson',
      email: 'alexa.richardson@admin-react-material-ui-theme.io',
      avatar: avatar_4,
      spent: '2,600.00',
      type: 'Enterprise',
      currency: '$',
      projects: '0',
      rating: 4.5,
      location: 'Georgia, USA',
    },
    {
      id: uuid(),
      name: 'Anje Keizer',
      email: 'anje.keizer@admin-react-material-ui-theme.io',
      avatar: avatar_5,
      spent: '5,600.00',
      type: 'Enterprise',
      currency: '$',
      projects: '6',
      rating: 4,
      location: 'Ohio, USA',
    },
    {
      id: uuid(),
      name: 'Clarke Gillebert',
      email: 'clarke.gillebert@admin-react-material-ui-theme.io',
      avatar: avatar_6,
      spent: '500.00',
      type: 'Agency',
      currency: '$',
      projects: '1',
      rating: 3.5,
      location: 'Texas, USA',
    },
    {
      id: uuid(),
      name: 'Adam Denisov',
      email: 'adam.denisov@admin-react-material-ui-theme.io',
      avatar: avatar_7,
      spent: '5,230.00',
      type: 'Agency',
      currency: '$',
      projects: '0',
      rating: 3,
      location: 'California, USA',
    },
    {
      id: uuid(),
      name: 'Ava Gregoraci',
      email: 'ava.gregoraci@admin-react-material-ui-theme.io',
      avatar: avatar_8,
      spent: '300.00',
      type: 'Freelancer',
      currency: '$',
      projects: '0',
      rating: 4,
      location: 'California, USA',
    },
    {
      id: uuid(),
      name: 'Emilee Simchenko',
      email: 'emilee.simchenko@admin-react-material-ui-theme.io',
      avatar: avatar_9,
      spent: '100.00',
      type: 'Agency',
      currency: '$',
      projects: '4',
      rating: 4.5,
      location: 'Nevada, USA',
    },
    {
      id: uuid(),
      name: 'Kwak Seong-Min',
      email: 'kwak.seong.min@admin-react-material-ui-theme.io',
      avatar: avatar_10,
      spent: '1,000.00',
      type: 'Freelancer',
      currency: '$',
      projects: '2',
      rating: 5,
      location: 'Michigan, USA',
    },
    {
      id: uuid(),
      name: 'Shen Zhi',
      email: 'shen.zhi@admin-react-material-ui-theme.io',
      avatar: avatar_11,
      spent: '2,300.00',
      type: 'Agency',
      currency: '$',
      projects: '0',
      rating: 3.9,
      location: 'Utah, USA',
    },
    {
      id: uuid(),
      name: 'Merrile Burgett',
      email: 'merrile.burgett@admin-react-material-ui-theme.io',
      avatar: avatar_12,
      spent: '200.00',
      type: 'Enterprise',
      currency: '$',
      projects: '7',
      rating: 4.2,
      location: 'Utah, USA',
    },
  ],
});

mock.onGet('/api/management/customers/1/summary').reply(200, {
  summary: {
    name: 'Ekaterina Tankova',
    email: 'ekaterina@admin-react-material-ui-theme.io',
    phone: '+55 748 327 439',
    state: 'Alabama',
    country: 'United States',
    zipCode: '240355',
    address1: 'Street John Wick, no. 7',
    address2: 'House #25',
    iban: '4142 **** **** **** ****',
    autoCC: false,
    verified: true,
    currency: '$',
    invoices: [
      {
        id: uuid(),
        type: 'paid',
        value: 10.0,
      },
      {
        id: uuid(),
        type: 'paid',
        value: 15.0,
      },
      {
        id: uuid(),
        type: 'due',
        value: 5,
      },
      {
        id: uuid(),
        type: 'income',
        value: 10.0,
      },
    ],
    vat: 19,
    balance: 0,
    emails: [
      {
        id: uuid(),
        description: 'Order confirmation',
        created_at: moment()
          .subtract(3, 'days')
          .subtract(5, 'hours')
          .subtract(34, 'minutes'),
      },
      {
        id: uuid(),
        description: 'Order confirmation',
        created_at: moment()
          .subtract(4, 'days')
          .subtract(11, 'hours')
          .subtract(49, 'minutes'),
      },
    ],
  },
});

mock.onGet('/api/management/customers/1/invoices').reply(200, {
  invoices: [
    {
      id: uuid(),
      date: moment(),
      description: 'Freelancer Subscription (12/05/2019 - 11/06/2019)',
      paymentMethod: 'Credit Card',
      value: '5.25',
      currency: '$',
      status: 'paid',
    },
    {
      id: uuid(),
      date: moment(),
      description: 'Freelancer Subscription (12/05/2019 - 11/06/2019)',
      paymentMethod: 'Credit Card',
      value: '5.25',
      currency: '$',
      status: 'paid',
    },
  ],
});

mock.onGet('/api/management/customers/1/logs').reply(200, {
  logs: [
    {
      id: uuid(),
      status: 200,
      method: 'POST',
      route: '/api/purchase',
      desc: 'Purchase',
      IP: '84.234.243.42',
      created_at: moment()
        .subtract(2, 'days')
        .subtract(2, 'minutes')
        .subtract(56, 'seconds'),
    },
    {
      id: uuid(),
      status: 522,
      error: 'Invalid credit card',
      method: 'POST',
      route: '/api/purchase',
      desc: 'Purchase',
      IP: '84.234.243.42',
      created_at: moment()
        .subtract(2, 'days')
        .subtract(2, 'minutes')
        .subtract(56, 'seconds'),
    },
    {
      id: uuid(),
      status: 200,
      method: 'DELETE',
      route: '/api/products/d65654e/remove',
      desc: 'Cart remove',
      IP: '84.234.243.42',
      created_at: moment()
        .subtract(2, 'days')
        .subtract(8, 'minutes')
        .subtract(23, 'seconds'),
    },
    {
      id: uuid(),
      status: 200,
      method: 'GET',
      route: '/api/products/d65654e/add',
      desc: 'Cart add',
      IP: '84.234.243.42',
      created_at: moment()
        .subtract(2, 'days')
        .subtract(20, 'minutes')
        .subtract(54, 'seconds'),
    },
    {
      id: uuid(),
      status: 200,
      method: 'GET',
      route: '/api/products/c85727f/add',
      desc: 'Cart add',
      IP: '84.234.243.42',
      created_at: moment()
        .subtract(2, 'days')
        .subtract(34, 'minutes')
        .subtract(16, 'seconds'),
    },
    {
      id: uuid(),
      status: 200,
      method: 'GET',
      route: '/api/products/c85727f',
      desc: 'View product',
      IP: '84.234.243.42',
      created_at: moment()
        .subtract(2, 'days')
        .subtract(54, 'minutes')
        .subtract(30, 'seconds'),
    },
    {
      id: uuid(),
      status: 200,
      method: 'GET',
      route: '/api/products',
      desc: 'Get products',
      IP: '84.234.243.42',
      created_at: moment()
        .subtract(2, 'days')
        .subtract(56, 'minutes')
        .subtract(40, 'seconds'),
    },
    {
      id: uuid(),
      status: 200,
      method: 'POST',
      route: '/api/login',
      desc: 'Login',
      IP: '84.234.243.42',
      created_at: moment()
        .subtract(2, 'days')
        .subtract(57, 'minutes')
        .subtract(5, 'seconds'),
    },
  ],
});
