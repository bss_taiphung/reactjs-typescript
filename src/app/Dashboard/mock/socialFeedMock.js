import { v1 as uuid } from 'uuid';
import moment from 'moment';

import mock from 'app/Dashboard/utils/mock';
import {
  avatar_10,
  avatar_11,
  avatar_12,
  avatar_4,
  avatar_5,
  avatar_6,
  avatar_8,
  post_1,
} from '../assets/images';

mock.onGet('/api/social-feed').reply(200, {
  posts: [
    {
      id: uuid(),
      author: {
        name: 'Kwak Seong-Min',
        avatar: avatar_10,
      },
      message: "Hey guys! What's your favorite framework?",
      liked: true,
      likes: 1,
      comments: [
        {
          id: uuid(),
          author: {
            name: 'Merrile Burgett',
            avatar: avatar_12,
          },
          message: "I've been using Angular for the past 3 years",
          created_at: moment().subtract(3, 'hours'),
        },
      ],
      created_at: moment().subtract(16, 'minutes'),
    },
    {
      id: uuid(),
      author: {
        name: 'Shen Zhi',
        avatar: avatar_11,
      },
      message: 'Just made this home screen for a project, what-cha thinkin?',
      media: post_1,
      liked: true,
      likes: 24,
      comments: [
        {
          id: uuid(),
          author: {
            name: 'Anje Keizer',
            avatar: avatar_5,
          },
          message: 'Could use some more statistics, but that’s me haha',
          created_at: moment().subtract(3, 'hours'),
        },
        {
          id: uuid(),
          author: {
            name: 'Ava Gregoraci',
            avatar: avatar_8,
          },
          message:
            'Hmm, honestly this looks nice but I would change the shadow though',
          created_at: moment().subtract(2, 'hours'),
        },
      ],
      created_at: moment().subtract(4, 'hours'),
    },
    {
      id: uuid(),
      author: {
        name: 'Shen Zhi',
        avatar: avatar_11,
      },
      message:
        'As a human being, you are designed in a way that makes you incapable of experiencing any positive emotion unless you set an aim and progress towards it. What makes you happy is not, in fact, attaining it, but making progress towards it.',
      liked: false,
      likes: 65,
      comments: [
        {
          id: uuid(),
          author: {
            name: 'Clarke Gillebert',
            avatar: avatar_6,
          },
          message:
            'That’s actually deep. Thanks for the design, would you consider making an interaction?',
          created_at: moment().subtract(3, 'hours'),
        },
        {
          id: uuid(),
          author: {
            name: 'Alexa Richardson',
            avatar: avatar_4,
          },
          message: 'Oh... so sentimental',
          created_at: moment().subtract(2, 'hours'),
        },
      ],
      created_at: moment().subtract(7, 'hours'),
    },
  ],
});
