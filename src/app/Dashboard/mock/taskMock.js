import { v1 as uuid } from 'uuid';
import moment from 'moment';

import mock from 'app/Dashboard/utils/mock';
import {
  avatar_10,
  avatar_12,
  avatar_2,
  avatar_3,
  avatar_4,
  avatar_5,
  avatar_6,
  avatar_7,
  avatar_8,
} from 'app/Dashboard/assets/images';

mock.onGet('/api/tasks').reply(200, {
  tasks: [
    {
      id: uuid(),
      title: 'Update the API for the project',
      deadline: moment().add(1, 'days').add(1, 'hour'),
      members: [avatar_2, avatar_3, avatar_4, avatar_5, avatar_6, avatar_7],
    },
    {
      id: uuid(),
      title: 'Redesign the landing page',
      deadline: moment().add(2, 'day').add(1, 'hour'),
      members: [avatar_8, avatar_10, avatar_12],
    },
    {
      id: uuid(),
      title: 'Solve the bug for the showState',
      deadline: moment(),
      members: [avatar_7],
    },
    {
      id: uuid(),
      title: 'Release v1.0 Beta',
      deadline: null,
      members: [avatar_2, avatar_10],
    },
    {
      id: uuid(),
      title: 'GDPR Compliance',
      deadline: null,
      members: [avatar_5, avatar_2, avatar_6],
    },
    {
      id: uuid(),
      title: 'Redesign Landing Page',
      deadline: null,
      members: [avatar_8],
    },
  ],
});
