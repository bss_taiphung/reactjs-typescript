import { createMuiTheme } from '@material-ui/core';

import palette from './palette';
import typography from './typography';
import overrides from './overrides';

const theme = createMuiTheme({
  // spacing: factor => `${0.25 * factor}rem`, // (Bootstrap strategy)
  palette,
  typography: typography as any,
  overrides,
});

export default theme;
