/**
 *
 * Asynchronously loads the component for App
 *
 */

import { lazyLoad } from 'utils/loadable';

export const KanbanBoard = lazyLoad(
  () => import('./KanbanBoard'),
  module => module.KanbanBoard,
);
