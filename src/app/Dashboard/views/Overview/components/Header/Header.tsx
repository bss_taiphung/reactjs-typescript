import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Button, Grid, Hidden, Typography } from '@material-ui/core';
import BarChartIcon from '@material-ui/icons/BarChart';
import { initialStateSession } from '../../../../config/constant';
import { undraw_growth_analytics_8btt } from '../../../../assets/images';

const useStyles = makeStyles((theme: any) => ({
  root: {},
  summaryButton: {
    backgroundColor: theme.palette.white,
  },
  barChartIcon: {
    marginRight: theme.spacing(1),
  },
  image: {
    width: '100%',
    maxHeight: 400,
  },
}));

const Header = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const session = initialStateSession;

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Grid alignItems="center" container justify="space-between" spacing={3}>
        <Grid item md={6} xs={12}>
          <Typography component="h2" gutterBottom variant="overline">
            Home
          </Typography>
          <Typography component="h1" gutterBottom variant="h3">
            Good Morning, {session.user.first_name}
          </Typography>
          <Typography gutterBottom variant="subtitle1">
            Here’s what’s happening with your projects today
          </Typography>
          <Button className={classes.summaryButton} variant="contained">
            <BarChartIcon className={classes.barChartIcon} />
            View summary
          </Button>
        </Grid>
        <Hidden smDown>
          <Grid item md={6}>
            <img
              alt="Cover"
              className={classes.image}
              src={undraw_growth_analytics_8btt}
            />
          </Grid>
        </Hidden>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  className: PropTypes.string,
};

export default Header;
