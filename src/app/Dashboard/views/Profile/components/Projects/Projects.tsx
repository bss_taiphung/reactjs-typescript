import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import axios from 'app/Dashboard/utils/axios';
import { ProjectCard } from 'app/Dashboard/components';

const useStyles = makeStyles(() => ({
  root: {},
}));

const Projects = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const [projects, setProjects] = useState<any>([]);

  useEffect(() => {
    let mounted = true;

    const fetchProjects = () => {
      if (mounted) {
        axios
          .get('/api/users/1/projects')
          .then(response => setProjects(response.data.projects));
      }
    };

    fetchProjects();

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Grid container spacing={3}>
        {projects.map(project => (
          <Grid item key={project.id} lg={4} xl={4} md={6} xs={12}>
            <ProjectCard project={project} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

Projects.propTypes = {
  className: PropTypes.string,
};

export default Projects;
