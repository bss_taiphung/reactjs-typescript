import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';

import axios from 'app/Dashboard/utils/axios';
import { AddPost, Page, PostCard } from 'app/Dashboard/components';
import { Header } from './components';

const useStyles = makeStyles((theme: any) => ({
  root: {
    width: theme.breakpoints.values.lg,
    maxWidth: '100%',
    margin: '0 auto',
    padding: theme.spacing(3),
  },
  newPost: {
    marginTop: theme.spacing(3),
  },
  posts: {
    marginTop: theme.spacing(3),
  },
  post: {
    marginBottom: theme.spacing(3),
  },
}));

const SocialFeed = () => {
  const classes = useStyles();

  const [posts, setPosts] = useState<any>([]);

  useEffect(() => {
    let mounted = true;

    const fetchPosts = () => {
      axios.get('/api/social-feed').then(response => {
        if (mounted) {
          setPosts(response.data.posts);
        }
      });
    };

    fetchPosts();

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <Page className={classes.root} title="Social Feed">
      <Header />
      <AddPost className={classes.newPost} />
      <div className={classes.posts}>
        {posts.map(post => (
          <PostCard className={classes.post} key={post.id} post={post} />
        ))}
      </div>
    </Page>
  );
};

export default SocialFeed;
