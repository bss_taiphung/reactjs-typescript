/*
 * images.js
 *
 * Import all icon to this file from assets/images
 *
 * To add a new Action:
 * 1) Import your icon image
 * 2) Export this in the last line code
 */

import icBrand from 'app/Landing/assets/images/svg/black.svg';
import icGoogle from 'app/Landing/assets/images/svg/ic-google.svg';
import icProfile from 'app/Landing/assets/images/svg/ic-profile.svg';
import icSearch from 'app/Landing/assets/images/svg/ic-search.svg';
import icShoppingCart from 'app/Landing/assets/images/svg/ic-shopping-cart.svg';
import icWishList from 'app/Landing/assets/images/svg/ic-wishlist.svg';
import icLaunch from 'app/Landing/assets/images/svg/launch.svg';
import icLogin from 'app/Landing/assets/images/svg/login.svg';
import icSale from 'app/Landing/assets/images/svg/sale.svg';
import icEng from 'app/Landing/assets/images/svg/ic_eng.svg';
import icTur from 'app/Landing/assets/images/svg/ic_tur.svg';
import icOffersBuyer from 'app/Landing/assets/images/svg/ic-offers-buyer.svg';
import icDoc from 'app/Landing/assets/images/svg/doc.svg';
import icPdf from 'app/Landing/assets/images/svg/pdf.svg';
import icZip from 'app/Landing/assets/images/svg/zip.svg';
import icAudio from 'app/Landing/assets/images/svg/audio.svg';
import icExcel from 'app/Landing/assets/images/svg/excel.svg';
import icJPG from 'app/Landing/assets/images/svg/jpg.svg';
import icUndefinedType from 'app/Landing/assets/images/svg/undefined-file.svg';
import icPowerPoint from 'app/Landing/assets/images/svg/power-point.svg';
import icNotFound from 'app/Landing/assets/images/svg/404.svg';
import icSentReportSuccess from 'app/Landing/assets/images/svg/dispute-opened-illustration.svg';
import icSell from 'app/Landing/assets/images/svg/selling-cta.svg';
import icDefaultAvatar from 'app/Landing/assets/images/svg/default_avatar.png';
import sendSfferSuccessfully from 'app/Landing/assets/images/svg/send-offer-successfully.svg';
import imgEmptyPurchased from 'app/Landing/assets/images/svg/man-sitting-on-ottoman.svg';
import icVisa from 'app/Landing/assets/images/svg/ic_visa.svg';
import icMastercard from 'app/Landing/assets/images/svg/ic_mastercard.svg';
import imgNoPayments from 'app/Landing/assets/images/svg/no-payments.svg';
import icClose from 'app/Landing/assets/images/svg/ic-close.svg';
import imgCheckoutSuccess from 'app/Landing/assets/images/svg/checkoutsuccsess.svg';
import imgWorking from 'app/Landing/assets/images/svg/working.svg';
import icLoadMore from 'app/Landing/assets/images/svg/ic-loadmore-2.svg';
import imgEmptySold from 'app/Landing/assets/images/svg/no-sold-products-illustration.svg';
import imgDeleteProduct from 'app/Landing/assets/images/svg/ill-deleting-product.svg';
import imgPleaseWaitUpload from 'app/Landing/assets/images/svg/please-wait.svg';
import imgSuccessImport from 'app/Landing/assets/images/svg/success.svg';
import imgNoProduct from 'app/Landing/assets/images/svg/no-products-added.svg';
import imgAvatar from 'app/Landing/assets/images/avatar.jpg';
import imgContactBG from 'app/Landing/assets/images/contact-bg.jpg';
import imgIntro01 from 'app/Landing/assets/images/intro-01.jpg';
import imgLanguageEN from 'app/Landing/assets/images/language-en.png';
import imgLanguageVI from 'app/Landing/assets/images/language-vi.png';
import imgProduct01 from 'app/Landing/assets/images/pro-01.jpg';
import imgProduct02 from 'app/Landing/assets/images/pro-02.jpg';
import imgProduct03 from 'app/Landing/assets/images/pro-03.jpg';
import imgSlider01 from 'app/Landing/assets/images/slider-01.jpg';
import imgSlider02 from 'app/Landing/assets/images/slider-02.jpg';
import imgSlider03 from 'app/Landing/assets/images/slider-03.jpg';
import imgSlider04 from 'app/Landing/assets/images/slider-04.jpg';
import imgDay from 'app/Landing/assets/images/day-bg.jpg';
import imgBookingDetail01 from 'app/Landing/assets/images/booking-detail-01.jpg';
import imgBlog01 from 'app/Landing/assets/images/blog-01.jpg';
import imgBlog02 from 'app/Landing/assets/images/blog-02.jpg';
import imgBlog03 from 'app/Landing/assets/images/blog-03.jpg';
import imgBlog04 from 'app/Landing/assets/images/blog-04.jpg';
import imgBlog05 from 'app/Landing/assets/images/blog-05.jpg';
import imgBlog06 from 'app/Landing/assets/images/blog-06.jpg';

export {
  icWishList,
  icProfile,
  icShoppingCart,
  icBrand,
  icGoogle,
  icSearch,
  icLaunch,
  icSale,
  icLogin,
  icEng,
  icTur,
  icOffersBuyer,
  icDoc,
  icPdf,
  icZip,
  icAudio,
  icExcel,
  icUndefinedType,
  icPowerPoint,
  icJPG,
  icNotFound,
  icSentReportSuccess,
  icSell,
  icDefaultAvatar,
  sendSfferSuccessfully,
  imgEmptyPurchased,
  icVisa,
  icMastercard,
  imgNoPayments,
  icClose,
  imgCheckoutSuccess,
  imgWorking,
  icLoadMore,
  imgEmptySold,
  imgDeleteProduct,
  imgPleaseWaitUpload,
  imgSuccessImport,
  imgNoProduct,
  imgAvatar,
  imgContactBG,
  imgIntro01,
  imgLanguageEN,
  imgLanguageVI,
  imgProduct01,
  imgProduct02,
  imgProduct03,
  imgSlider01,
  imgSlider02,
  imgSlider03,
  imgSlider04,
  imgDay,
  imgBookingDetail01,
  imgBlog01,
  imgBlog02,
  imgBlog03,
  imgBlog04,
  imgBlog05,
  imgBlog06,
};
