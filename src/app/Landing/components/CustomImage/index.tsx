import React, { memo } from 'react';
import { Img } from 'react-image';
import { FileService } from 'app/Landing/utils/files/file.service';

interface Props {
  path: string;
  alt?: string;
}

export const CustomImage = memo((props: Props) => {
  return (
    <Img src={FileService.getInstance().buildURL(props.path)} alt={props.alt} />
  );
});
