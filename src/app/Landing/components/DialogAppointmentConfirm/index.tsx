/**
 *
 * DialogAppointmentConfirm
 *
 */
import React, { memo } from 'react';
import { useTranslation } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import { translations } from 'app/Landing/locales/i18n';
import DialogContent from '@material-ui/core/DialogContent';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';

interface Props {
  open: boolean;
  handleClose: () => void;
  data: IRoomTypeInterface | null;
}

export const DialogAppointmentConfirm = memo((props: Props) => {
  const { t, i18n } = useTranslation();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="responsive-dialog-title"
      className="modal-custom"
    >
      <button
        className="btn btn-icon ml-auto"
        autoFocus
        onClick={props.handleClose}
      >
        <span className="icon-close"></span>
      </button>
      <DialogTitle id="responsive-dialog-title">
        {t(translations.roomsAppointmentSummary.dialog.confirm.title)}
      </DialogTitle>
      <DialogContent>
        <div className="list-striped mt-10">
          <div className="d-sm-flex justify-content-between row-striped">
            <div className="text-left">
              {t(
                translations.roomsAppointmentSummary.dialog.confirm.form
                  .buildingName,
              )}
            </div>
            <div className="text-sm-right">{props.data?.building?.name}</div>
          </div>
          <div className="d-sm-flex justify-content-between row-striped">
            <div className="text-left">
              {t(
                translations.roomsAppointmentSummary.dialog.confirm.form
                  .buildingAddress,
              )}
            </div>
            <div className="text-sm-right">{props.data?.building?.address}</div>
          </div>
          <div className="d-sm-flex justify-content-between row-striped">
            <div className="text-left">
              {t(
                translations.roomsAppointmentSummary.dialog.confirm.form
                  .hotline,
              )}
            </div>
            <div className="text-sm-right">
              {' '}
              {props.data?.building?.hotline}
            </div>
          </div>
        </div>
        <div className="form-custom">
          <div className="text-muteds">
            {t(translations.roomsAppointmentSummary.dialog.outro)}
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
});
