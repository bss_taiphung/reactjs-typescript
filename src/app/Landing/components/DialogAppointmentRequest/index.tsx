/**
 *
 * DialogAppointmentRequest
 *
 */
import React, { memo } from 'react';
import { useTranslation } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import { translations } from 'app/Landing/locales/i18n';
import DialogContent from '@material-ui/core/DialogContent';
import FormControl from '@material-ui/core/FormControl';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { FormProvider, useForm } from 'react-hook-form';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';
import { yupResolver } from '@hookform/resolvers/yup';
import { ObjectSchema } from 'yup';
import { MuTextField } from 'app/Landing/components/MuTextField';
import { ICreateAppointmentRequest } from 'app/Landing/interfaces/i.appointment.interface';

interface Props {
  open: boolean;
  handleClose: () => void;
  showContinueDialog: () => void;
  onSubmit: (request: ICreateAppointmentRequest) => Promise<void>;
  data: IRoomTypeInterface | null;
  schemaValidation: ObjectSchema;
}

export const DialogAppointmentRequest = memo((props: Props) => {
  const { t } = useTranslation();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const methods = useForm({
    defaultValues: {
      // fullName: 'naster blue',
      // phoneNumber: '0902466104',
      // email: 'tai.phung@beesightsoft.com',

      fullName: '',
      phoneNumber: '',
      email: '',
    },
    mode: 'onBlur',
    reValidateMode: 'onChange',
    resolver: yupResolver(props.schemaValidation),
  });
  const { register, errors } = methods;
  const onSubmit = async data => {
    console.log(data);
    await props.onSubmit(data);
    // props.showContinueDialog();
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="responsive-dialog-title"
      className="modal-custom"
    >
      <button
        className="btn btn-icon ml-auto"
        autoFocus
        onClick={props.handleClose}
      >
        <span className="icon-close"></span>
      </button>
      <DialogTitle id="responsive-dialog-title">
        {t(translations.roomsAppointmentSummary.dialog.request.title)}
      </DialogTitle>
      <DialogContent>
        <div className="subtext mt-10">
          {t(translations.roomsAppointmentSummary.dialog.request.subTitle)}
        </div>
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(onSubmit)}>
            <div className="form-custom">
              <FormControl className="d-flex form-group">
                <MuTextField
                  name={'fullName'}
                  register={register}
                  errors={errors}
                  label={t(
                    translations.roomsAppointmentSummary.dialog.request.form
                      .fullName,
                  )}
                  placeholder={t(
                    translations.roomsAppointmentSummary.dialog.request.form
                      .fullName,
                  )}
                />
              </FormControl>

              <FormControl className="d-flex form-group">
                <MuTextField
                  name={'phoneNumber'}
                  register={register}
                  errors={errors}
                  label={t(
                    translations.roomsAppointmentSummary.dialog.request.form
                      .phoneNumber,
                  )}
                  placeholder={t(
                    translations.roomsAppointmentSummary.dialog.request.form
                      .phoneNumber,
                  )}
                />
              </FormControl>

              <FormControl className="d-flex form-group">
                <MuTextField
                  name={'email'}
                  register={register}
                  errors={errors}
                  label={t(
                    translations.roomsAppointmentSummary.dialog.request.form
                      .email,
                  )}
                  placeholder={t(
                    translations.roomsAppointmentSummary.dialog.request.form
                      .email,
                  )}
                />
              </FormControl>

              <div className="get-action mt-30">
                <button type={'submit'} className="btn btn-blue">
                  {t(
                    translations.roomsAppointmentSummary.dialog.request.form
                      .continueText,
                  )}
                </button>
              </div>

              <div className="text-muteds">
                {t(translations.roomsAppointmentSummary.dialog.outro)}
              </div>
            </div>
          </form>
        </FormProvider>
      </DialogContent>
    </Dialog>
  );
});
