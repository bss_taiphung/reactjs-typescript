import React, { memo } from 'react';
import './EditorControl.scss';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import InputLabel from '@material-ui/core/InputLabel';
import { ErrorMessage } from '@hookform/error-message';
import { FieldErrors } from 'react-hook-form';

interface Props {
  name: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  register?: any;
  setValue?: any;
  errors?: FieldErrors;
  isViewOnly?: boolean;
  isHideToolbar?: boolean;
}

export const EditorControl = memo((props: Props) => {
  const {
    name,
    label,
    placeholder,
    defaultValue = '',
    isViewOnly,
    isHideToolbar,
    register,
    setValue,
    errors,
  } = props;

  if (isViewOnly) {
    return (
      <div className="view-editor-control-wrapper">
        <InputLabel shrink htmlFor={name}>
          {label}
        </InputLabel>
        <ReactQuill
          key={new Date().getTime().toString()}
          readOnly={true}
          theme="snow"
          value={defaultValue}
          modules={hideToolBarModules}
          formats={formats}
        />
      </div>
    );
  } else {
    return (
      <div className="editor-control-wrapper">
        <InputLabel shrink htmlFor={name}>
          {label}
        </InputLabel>
        <ReactQuill
          theme="snow"
          value={defaultValue}
          onChange={(content, delta, source, editor) => {
            setValue(name, content);
          }}
          modules={isHideToolbar ? hideToolBarModules : modules}
          formats={formats}
          placeholder={placeholder}
        />
        <textarea
          style={{ display: 'none' }}
          value={defaultValue}
          ref={register}
          name={name}
          id={name}
        />
        <ErrorMessage
          errors={errors}
          name={name}
          as={<p className="text-error" />}
        />
      </div>
    );
  }
});
const hideToolBarModules = { toolbar: false };
const modules = {
  toolbar: [
    [{ header: [1, 2, 3, 4, false] }],
    ['bold', 'italic', 'underline', 'strike'],
    ['blockquote', 'code-block'],
    [{ color: [] }, { background: [] }],
    [{ align: [] }],
    [
      { list: 'ordered' },
      { list: 'bullet' },
      { indent: '-1' },
      { indent: '+1' },
    ],
    ['link', 'image', 'video'],
    ['clean'],
  ],
};

const formats = [
  'header',
  'font',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'color',
  'background',
  'align',
  'code',
  'script',
  'list',
  'bullet',
  'indent',
  'link',
  'image',
  'video',
];
