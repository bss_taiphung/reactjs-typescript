import React, { memo } from 'react';
import { LazyLoadImage as ReactLazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

interface Props {
  alt?: string;
  height?: string;
  src: any;
  width?: string;
}

export const LazyLoadImage = memo((image: Props) => {
  return (
    <ReactLazyLoadImage
      effect="blur"
      alt={image?.alt}
      height={image?.height}
      src={image.src}
      width={image?.width}
    />
  );
});
