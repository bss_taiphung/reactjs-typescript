import React, { memo } from 'react';
import { BeatLoader } from 'react-spinners';
import './style.scss';

interface Props {
  loading: boolean;
}

export const LoadingSpinner = memo((props: Props) => {
  return (
    <div className="sweet-loading">
      <BeatLoader size={30} color={'#123abc'} loading={props.loading} />
    </div>
  );
});
