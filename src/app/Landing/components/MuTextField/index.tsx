import React, { memo } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import InputBase from '@material-ui/core/InputBase';
import { ErrorMessage } from '@hookform/error-message';
import { FieldErrors } from 'react-hook-form';

interface Props {
  name: string;
  label?: string;
  placeholder?: string;
  register: () => void;
  errors: FieldErrors;
}

export const MuTextField = memo((props: Props) => {
  const { name, label, register, placeholder, errors } = props;
  return (
    <>
      <InputLabel shrink htmlFor={name}>
        {label}
      </InputLabel>
      <InputBase
        name={name}
        inputRef={register}
        placeholder={placeholder}
        id={name}
      />
      <ErrorMessage
        errors={errors}
        name={name}
        as={<p className="text-error" />}
      />
    </>
  );
});
