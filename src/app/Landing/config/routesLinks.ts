const landingRoot = '/';
const findYourHome: string = 'find-your-home';
const roomType: string = 'room-type';
const blogs: string = 'blogs';
const faq: string = 'faq';
const contact: string = 'contact';
const arrival: string = 'arrival';
const minimumStay: string = 'minimum-stay';
const roomsForRent: string = 'rooms-for-rent';
const roomTypeAppointment: string = 'room-type-appointment';

const routeLinks = {
  landingRoot: landingRoot,
  arrival: `${landingRoot}${findYourHome}/${arrival}`,
  minimumStay: `${landingRoot}${findYourHome}/${minimumStay}`,
  roomsForRent: `${landingRoot}${findYourHome}/${roomsForRent}`,
  roomsAppointmentSummary: `${landingRoot}${findYourHome}/${roomTypeAppointment}/:id`,
  roomTypeDetail: `${landingRoot}${roomType}/:id`,
  faq: `${landingRoot}${faq}`,
  contact: `${landingRoot}${contact}`,
  blogs: `${landingRoot}${blogs}`,
  blogsId: `${landingRoot}${blogs}/:id`,
};

export default routeLinks;
