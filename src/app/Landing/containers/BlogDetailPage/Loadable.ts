/**
 *
 * Asynchronously loads the component for BlogDetailPage
 *
 */

import { lazyLoad } from 'utils/loadable';

export const BlogDetailPage = lazyLoad(
  () => import('./index'),
  module => module.BlogDetailPage,
);
