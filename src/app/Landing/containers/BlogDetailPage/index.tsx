/**
 *
 * BlogDetailPage
 *
 */

import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import {
  fFindBlogByIdRequestSlice,
  fFindBlogOthersByIdRequestSlice,
  reducer,
  sliceKey,
} from './slice';
import { selectBlogDetailPage } from './selectors';
import { blogDetailPageSaga } from './saga';
import { FindRoomDetailPageStyles } from 'app/Landing/styles/containers/find.room.detail.styles';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { LazyLoadImage } from 'app/Landing/components/LazyLoadImage';
import { useParams } from 'react-router-dom';
import { actions } from 'app/Landing/containers/FindYourHomeRoomsAppointmentSummaryPage/slice';
import { BlogDtoBuilder } from 'app/Landing/services/builder/blog.dto.builder';
import { fDisplayDateFormat } from 'app/Landing/utils/date/date.utils';
import { IBlogInterface } from 'app/Landing/interfaces/i.blog.interface';
import history from 'utils/history';
import routeLinks from 'app/Landing/config/routesLinks';
import { translations } from 'app/Landing/locales/i18n';
import { BlogJsonLd } from 'app/Landing/utils/json-ld/blog.json-ld';
import { EditorControl } from 'app/Landing/components/EditorControl';

interface Props {}

export const BlogDetailPage = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: blogDetailPageSaga });

  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();

  const { id = '' } = useParams();

  const { isLoading, dtoBlog, dtoBlogOthers } = useSelector(
    selectBlogDetailPage,
  );
  BlogDtoBuilder.getInstance().setLocale(i18n.language);
  BlogDtoBuilder.getInstance().setDto(dtoBlog);
  BlogDtoBuilder.getInstance().setDtoBlogs(dtoBlogOthers);

  useEffect(() => {
    (async () => {
      await dispatch(fFindBlogByIdRequestSlice(id));
      await dispatch(fFindBlogOthersByIdRequestSlice(id));
    })();
    return () => {
      dispatch(actions.clearData());
    };
  }, [id]);

  BlogJsonLd.getInstance().setDto(BlogDtoBuilder.getInstance().getDto());

  return (
    <>
      <Helmet script={BlogJsonLd.getInstance().toJsonLd()}>
        <title>{BlogDtoBuilder.getInstance().getDto()?.translated?.name}</title>
        <meta
          name="description"
          content={BlogDtoBuilder.getInstance().getDto()?.translated?.name}
        />
      </Helmet>
      <div className="section section-booking-detail-carousel">
        <div className="container-fuild">
          <div className="img-section">
            <LazyLoadImage
              src={BlogDtoBuilder.getInstance().getDto()?.image?.path}
            />
          </div>
        </div>
        <div className="container position-relative">
          <div className="content-section">
            <div className="title-section">
              {BlogDtoBuilder.getInstance().getDto()?.translated?.name}
            </div>
            <div className="d-flex mt-30">
              <div className="d-flex align-items-center">
                <span className="icon-date_range"></span>
                <i>
                  {fDisplayDateFormat(
                    BlogDtoBuilder.getInstance().getDto()?.createdAt,
                  )}
                </i>
              </div>
              <div className="ml-5">
                <i>By Admin</i>
              </div>
            </div>
            <div className="text mt-30">
              <EditorControl
                isViewOnly={true}
                name={'content'}
                defaultValue={
                  BlogDtoBuilder.getInstance().getDto()?.translated?.content
                }
              />
            </div>
          </div>
        </div>
      </div>
      <div className="section section-product blog">
        <div className="container">
          <div className="title-section">
            {' '}
            {t(translations.blogDetail.outro.recentBlog)}
          </div>
          <div className="row">
            {BlogDtoBuilder.getInstance()
              .getDtoBlogs()
              .map((item: IBlogInterface) => (
                <div className="col-sm-4" key={item.id}>
                  <div
                    className="item-product"
                    onClick={() => {
                      history.push({
                        pathname: routeLinks.blogsId.replace(':id', item.id),
                      });
                    }}
                  >
                    <div className="catalogue">
                      {' '}
                      {item?.blogCategory?.translated?.name}
                    </div>
                    <div className="img-item">
                      <LazyLoadImage src={item?.image?.path} />
                    </div>
                    <div className="content-item">
                      <div className="place d-flex align-items-center">
                        <span className="icon-date_range"></span>
                        <i>{fDisplayDateFormat(item?.createdAt)}</i>
                      </div>
                      <div className="name">
                        <a>{item?.translated?.name}</a>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
      <FindRoomDetailPageStyles />
    </>
  );
});
