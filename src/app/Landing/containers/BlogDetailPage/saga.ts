// import { take, call, put, select, takeLatest } from 'redux-saga/effects';
// import { actions } from './slice';

// export function* doSomething() {}

import { AxiosGet } from 'utils/axios/axiosGet';
import apiLinks, { RESOURCE_BLOG_NAME } from 'app/Landing/config/apiLinks';

export function fFindBlogByIdRequestSaga(id: string) {
  return AxiosGet(apiLinks[RESOURCE_BLOG_NAME].findById.replace(':id', id));
}

export function fFindBlogOthersByIdRequestSaga(id: string) {
  return AxiosGet(apiLinks[RESOURCE_BLOG_NAME].findOthers.replace(':id', id));
}

export function* blogDetailPageSaga() {
  // yield takeLatest(actions.someAction.type, doSomething);
}
