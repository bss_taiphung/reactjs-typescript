import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.blogDetailPage || initialState;

export const selectBlogDetailPage = createSelector(
  [selectDomain],
  blogDetailPageState => blogDetailPageState,
);
