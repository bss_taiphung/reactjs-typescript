import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';
import { set } from 'lodash';
import {
  fFindBlogByIdRequestSaga,
  fFindBlogOthersByIdRequestSaga,
} from 'app/Landing/containers/BlogDetailPage/saga';
import { IBlogInterface } from 'app/Landing/interfaces/i.blog.interface';

// The initial state of the BlogDetailPage container
export const initialState: ContainerState = {
  isLoading: false,
  dtoBlog: null,
  dtoBlogOthers: [],
};

const blogDetailPageSlice = createSlice({
  name: 'blogDetailPage',
  initialState,
  reducers: {
    clearData: () => initialState,
    showLoading(state, action: PayloadAction<boolean>) {
      set(state, 'isLoading', action.payload);
    },
    setDtoBlog(state, action: PayloadAction<IBlogInterface | null>) {
      set(state, 'dtoBlog', action.payload);
    },
    setDtoBlogOthers(state, action: PayloadAction<IBlogInterface[]>) {
      set(state, 'dtoBlogOthers', action.payload);
    },
  },
});

export const { actions, reducer, name: sliceKey } = blogDetailPageSlice;

// Asynchronous thunk action
export function fFindBlogByIdRequestSlice(id: string) {
  return async dispatch => {
    // dispatch(actions.showLoading(true));
    try {
      const data = await fFindBlogByIdRequestSaga(id);
      dispatch(actions.setDtoBlog(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}

export function fFindBlogOthersByIdRequestSlice(id: string) {
  return async dispatch => {
    // dispatch(actions.showLoading(true));
    try {
      const data = await fFindBlogOthersByIdRequestSaga(id);
      dispatch(actions.setDtoBlogOthers(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}
