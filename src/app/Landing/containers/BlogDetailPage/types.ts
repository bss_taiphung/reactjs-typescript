/* --- STATE --- */
import { IBlogInterface } from 'app/Landing/interfaces/i.blog.interface';

export interface BlogDetailPageState {
  isLoading: boolean;
  dtoBlog: IBlogInterface | null;
  dtoBlogOthers: IBlogInterface[];
}

export type ContainerState = BlogDetailPageState;
