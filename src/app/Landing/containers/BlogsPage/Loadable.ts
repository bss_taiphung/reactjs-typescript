/**
 *
 * Asynchronously loads the component for BlogsPage
 *
 */

import { lazyLoad } from 'utils/loadable';

export const BlogsPage = lazyLoad(
  () => import('./index'),
  module => module.BlogsPage,
);
