/**
 *
 * BlogsPage
 *
 */

import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { fGetBlogsRequestSlice, reducer, sliceKey } from './slice';
import { selectBlogsPage } from './selectors';
import { blogsPageSaga } from './saga';
import { HomePageStyles } from 'app/Landing/styles/containers/homepage.styles';
import { LazyLoadImage } from 'app/Landing/components/LazyLoadImage';
import { actions } from 'app/Landing/containers/FindYourHomeRoomsForRentPage/slice';
import { IBlogInterface } from 'app/Landing/interfaces/i.blog.interface';
import { BlogDtoBuilder } from 'app/Landing/services/builder/blog.dto.builder';
import { fDisplayDateFormat } from 'app/Landing/utils/date/date.utils';
import { LoadingSpinner } from 'app/Landing/components/LoadingSpinner';
import Pagination from '@material-ui/lab/Pagination';
import history from 'utils/history';
import routeLinks from 'app/Landing/config/routesLinks';
import { translations } from 'app/Landing/locales/i18n';
import queryString from 'query-string';
import * as _ from 'lodash';

interface Props {}

export const BlogsPage = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: blogsPageSaga });

  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const { blogCategoryId = '' } = queryString.parse(
    _.get(props, 'history.location.search', ''),
  );
  console.log(blogCategoryId);

  const { isLoading, dtoBlogs, currentPage, totalPages, take } = useSelector(
    selectBlogsPage,
  );
  BlogDtoBuilder.getInstance().setLocale(i18n.language);
  BlogDtoBuilder.getInstance().setDtoBlogs(dtoBlogs);
  const loadPage = async (page: number) => {
    await dispatch(
      fGetBlogsRequestSlice({
        skip: (page - 1) * take,
        take,
        blogCategoryId,
      }),
    );
  };
  useEffect(() => {
    (async () => {
      await loadPage(1);
    })();
    return () => {
      dispatch(actions.clearData());
    };
  }, []);

  useEffect(() => {
    (async () => {
      await loadPage(1);
    })();
    return () => {
      dispatch(actions.clearData());
    };
  }, [blogCategoryId]);

  const handleChangePage = async (
    event: React.ChangeEvent<unknown>,
    value: number,
  ) => {
    await loadPage(value);
  };

  return (
    <>
      <Helmet>
        <title>{t(translations.blogs.title)}</title>
        <meta name="description" content={t(translations.blogs.description)} />
      </Helmet>
      <div>{t('')}</div>
      <div className="section section-title">
        <div className="container">
          <div className="title-section">
            {t(translations.blogs.intro.title)}
          </div>
        </div>
      </div>
      <div className="section section-product blog">
        <div className="container">
          <div className="row">
            {BlogDtoBuilder.getInstance()
              .getDtoBlogs()
              .map((item: IBlogInterface) => (
                <div className="col-sm-4" key={item.id}>
                  <div
                    className="item-product"
                    onClick={() => {
                      history.push({
                        pathname: routeLinks.blogsId.replace(':id', item.id),
                      });
                    }}
                  >
                    <div className="catalogue">
                      {item?.blogCategory?.translated?.name}
                    </div>
                    <div className="img-item">
                      <LazyLoadImage src={item?.image?.path} />
                    </div>
                    <div className="content-item">
                      <div className="place d-flex align-items-center">
                        <span className="icon-date_range"></span>
                        <i>{fDisplayDateFormat(item?.createdAt)}</i>
                      </div>
                      <div className="name">
                        <a>{item?.translated?.name}</a>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
      <HomePageStyles />
      <div className={'row-pagination'}>
        <Pagination
          count={totalPages}
          page={currentPage}
          variant="outlined"
          onChange={handleChangePage}
        />
      </div>

      <LoadingSpinner loading={isLoading}></LoadingSpinner>
    </>
  );
});
