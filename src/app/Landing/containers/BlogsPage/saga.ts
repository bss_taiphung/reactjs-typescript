// import { take, call, put, select, takeLatest } from 'redux-saga/effects';
// import { actions } from './slice';

// export function* doSomething() {}
import { AxiosPost } from 'utils/axios/axiosPost';
import apiLinks, { RESOURCE_BLOG_NAME } from 'app/Landing/config/apiLinks';

export function fGetBlogsRequestSaga(request) {
  return AxiosPost(apiLinks[RESOURCE_BLOG_NAME].findAll, request);
}

export function* blogsPageSaga() {
  // yield takeLatest(actions.someAction.type, doSomething);
}
