import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.blogsPage || initialState;

export const selectBlogsPage = createSelector(
  [selectDomain],
  blogsPageState => blogsPageState,
);
