/* --- STATE --- */
import { IBlogInterface } from 'app/Landing/interfaces/i.blog.interface';

export interface BlogsPageState {
  isLoading: boolean;
  take: number;
  totalPages: number;
  currentPage: number;
  dtoBlogs: IBlogInterface[];
}

export type ContainerState = BlogsPageState;
