import React, { memo } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { fCreateContactRequestSlice, reducer, sliceKey } from './slice';
import { selectContactPage } from './selectors';
import { contactPageSaga } from './saga';
import FormControl from '@material-ui/core/FormControl';

import { ContactPageStyles } from 'app/Landing/styles/containers/contactpage.styles';
import { MuTextField } from 'app/Landing/components/MuTextField';
import { translations } from 'app/Landing/locales/i18n';
import { FormProvider, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { EditorControl } from 'app/Landing/components/EditorControl';
import { getSchemaValidation } from './schema.validation';
import { ICreateContactRequest } from 'app/Landing/interfaces/i.contact.interface';
import { LoadingSpinner } from 'app/Landing/components/LoadingSpinner';
import { useSnackbar } from 'notistack';

interface Props {}

export const ContactPage = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: contactPageSaga });

  const { isLoading } = useSelector(selectContactPage);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();

  const defaultValues: ICreateContactRequest = {
    // fullName: 'naster blue',
    // phoneNumber: '0902466104',
    // email: 'test@beesightsoft.com',
    // content:
    //   'Define maximum number of snackbars that can be stacked on top of each other.',
    fullName: '',
    phoneNumber: '',
    email: '',
    content: '',
  };
  const methods = useForm({
    defaultValues: defaultValues,
    mode: 'onBlur',
    reValidateMode: 'onBlur',
    resolver: yupResolver(getSchemaValidation(t)),
  });
  const { register, errors, setValue, reset } = methods;
  const onSubmit = async (request: ICreateContactRequest) => {
    console.log({ request });
    await dispatch(fCreateContactRequestSlice(request));
    enqueueSnackbar(t(translations.contact.form.sendSuccessfullyMsg), {
      variant: 'success',
    });
    reset(defaultValues);
  };

  return (
    <>
      <Helmet>
        <title>{t(translations.contact.title)}</title>
        <meta
          name="description"
          content={t(translations.contact.description)}
        />
      </Helmet>
      <div className="section section-title">
        <div className="container">
          <div className="title-section">
            {t(translations.contact.intro.title)}
          </div>
        </div>
      </div>
      <div className="section section-contact">
        <div className="container">
          <div className="row">
            <div className="col-sm-6">
              <div className="content-section">
                <p>{t(translations.contact.intro.subTitle)}</p>
                <p> {t(translations.contact.intro.hotline1)} : 0901.236.547</p>
                <p>{t(translations.contact.intro.hotline2)}: 0901.236.546</p>
                <div className="d-flex align-items-start">
                  <span className="icon-location"></span>
                  <p>
                    335 Madison Avenue,
                    <br />
                    Suite 6A New York, <br />
                    NY 10017
                  </p>
                </div>
                <div className="d-flex align-items-start">
                  <span className="icon-phone"></span>
                  <p>(844) 612-6697</p>
                </div>
                <FormProvider {...methods}>
                  <form onSubmit={methods.handleSubmit(onSubmit)}>
                    <div className="form-custom">
                      <FormControl className="d-flex form-group">
                        <MuTextField
                          name={'fullName'}
                          register={register}
                          errors={errors}
                          label={t(
                            translations.roomsAppointmentSummary.dialog.request
                              .form.fullName,
                          )}
                          placeholder={t(
                            translations.roomsAppointmentSummary.dialog.request
                              .form.fullName,
                          )}
                        />
                      </FormControl>

                      <FormControl className="d-flex form-group">
                        <MuTextField
                          name={'phoneNumber'}
                          register={register}
                          errors={errors}
                          label={t(
                            translations.roomsAppointmentSummary.dialog.request
                              .form.phoneNumber,
                          )}
                          placeholder={t(
                            translations.roomsAppointmentSummary.dialog.request
                              .form.phoneNumber,
                          )}
                        />
                      </FormControl>

                      <FormControl className="d-flex form-group">
                        <MuTextField
                          name={'email'}
                          register={register}
                          errors={errors}
                          label={t(
                            translations.roomsAppointmentSummary.dialog.request
                              .form.email,
                          )}
                          placeholder={t(
                            translations.roomsAppointmentSummary.dialog.request
                              .form.email,
                          )}
                        />
                      </FormControl>

                      <FormControl className="d-flex form-group">
                        <EditorControl
                          name={'content'}
                          defaultValue={defaultValues.content}
                          register={register}
                          setValue={setValue}
                          errors={errors}
                          placeholder={t(translations.contact.form.content)}
                          label={t(translations.contact.form.content)}
                          isHideToolbar={true}
                        />
                      </FormControl>

                      <div className="get-action mt-30">
                        <button type={'submit'} className="btn btn-blue">
                          {t(translations.contact.form.sendText)}
                        </button>
                      </div>
                    </div>
                  </form>
                </FormProvider>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="map-section">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.3663671317895!2d106.6851702509186!3d10.783226561977745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f3204c0582b%3A0x9c3d8ddb161f199b!2zVsO1IFRo4buLIFPDoXUsIHBoxrDhu51uZyA3LCBRdeG6rW4gMywgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1604549392809!5m2!1svi!2s"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ContactPageStyles />
      <LoadingSpinner loading={isLoading}></LoadingSpinner>
    </>
  );
});
