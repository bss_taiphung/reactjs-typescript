import { AxiosPost } from 'utils/axios/axiosPost';
import apiLinks, { RESOURCE_CONTACT_NAME } from 'app/Landing/config/apiLinks';
import { ICreateContactRequest } from 'app/Landing/interfaces/i.contact.interface';

export function fCreateContactRequestSaga(request: ICreateContactRequest) {
  return AxiosPost(apiLinks[RESOURCE_CONTACT_NAME].create, request);
}

export function* contactPageSaga() {
  // yield takeLatest(actions.someAction.type, doSomething);
}
