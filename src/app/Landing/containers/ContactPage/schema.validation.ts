import * as Yup from 'yup';
import * as yup from 'yup';
import { ObjectSchema } from 'yup';
import { translations } from 'app/Landing/locales/i18n';
import {
  EMAIL_REG_EXP,
  NUMBER_REG_EXP,
  VALIDATION_FORM_VARIANTS,
} from 'app/Landing/config/constant';
import stripHtml from 'string-strip-html';

export const getSchemaValidation = (t): ObjectSchema => {
  const schema = Yup.object().shape({
    fullName: Yup.string()
      .trim()
      .required(
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .fullName.required,
        ),
      )
      .min(
        VALIDATION_FORM_VARIANTS.fullName.min,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .fullName.length,
          {
            min: VALIDATION_FORM_VARIANTS.fullName.min,
            max: VALIDATION_FORM_VARIANTS.fullName.max,
          },
        ),
      )
      .max(
        VALIDATION_FORM_VARIANTS.fullName.max,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .fullName.length,
          {
            min: VALIDATION_FORM_VARIANTS.fullName.min,
            max: VALIDATION_FORM_VARIANTS.fullName.max,
          },
        ),
      ),
    content: Yup.string()
      .trim()
      .transform(value => stripHtml(value).result)
      .required(t(translations.contact.validation.content.required))
      .min(
        VALIDATION_FORM_VARIANTS.content.min,
        t(translations.contact.validation.content.length, {
          min: VALIDATION_FORM_VARIANTS.content.min,
          max: VALIDATION_FORM_VARIANTS.content.max,
        }),
      )
      .max(
        VALIDATION_FORM_VARIANTS.content.max,
        t(translations.contact.validation.content.length, {
          min: VALIDATION_FORM_VARIANTS.content.min,
          max: VALIDATION_FORM_VARIANTS.content.max,
        }),
      ),
    email: yup
      .string()
      .trim()
      .required(
        t(
          translations.roomsAppointmentSummary.dialog.request.validation.email
            .required,
        ),
      )
      .matches(
        EMAIL_REG_EXP,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation.email
            .typeError,
        ),
      ),
    phoneNumber: yup
      .string()
      .trim()
      .required(
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .phoneNumber.required,
        ),
      )
      .matches(
        NUMBER_REG_EXP,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .phoneNumber.length,
          {
            min: VALIDATION_FORM_VARIANTS.phoneNumber.min,
            max: VALIDATION_FORM_VARIANTS.phoneNumber.max,
          },
        ),
      )
      .min(
        VALIDATION_FORM_VARIANTS.phoneNumber.min,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .phoneNumber.length,
          {
            min: VALIDATION_FORM_VARIANTS.phoneNumber.min,
            max: VALIDATION_FORM_VARIANTS.phoneNumber.max,
          },
        ),
      )
      .max(
        VALIDATION_FORM_VARIANTS.phoneNumber.max,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .phoneNumber.length,
          {
            min: VALIDATION_FORM_VARIANTS.phoneNumber.min,
            max: VALIDATION_FORM_VARIANTS.phoneNumber.max,
          },
        ),
      ),
  });
  return schema;
};
