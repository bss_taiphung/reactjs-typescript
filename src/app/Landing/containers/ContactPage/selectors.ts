import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.contactPage || initialState;

export const selectContactPage = createSelector(
  [selectDomain],
  contactPageState => contactPageState,
);
