import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';
import { fCreateContactRequestSaga } from 'app/Landing/containers/ContactPage/saga';
import { ICreateContactRequest } from 'app/Landing/interfaces/i.contact.interface';
import { set } from 'lodash';

// The initial state of the ContactPage container
export const initialState: ContainerState = {
  isLoading: false,
};

const contactPageSlice = createSlice({
  name: 'contactPage',
  initialState,
  reducers: {
    clearData: () => initialState,
    showLoading(state, action: PayloadAction<boolean>) {
      set(state, 'isLoading', action.payload);
    },
  },
});

export const { actions, reducer, name: sliceKey } = contactPageSlice;

export function fCreateContactRequestSlice(request: ICreateContactRequest) {
  return async dispatch => {
    dispatch(actions.showLoading(true));
    try {
      const data = await fCreateContactRequestSaga(request);
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}
