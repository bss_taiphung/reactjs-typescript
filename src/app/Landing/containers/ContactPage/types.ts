/* --- STATE --- */
export interface ContactPageState {
  isLoading: boolean;
}

export type ContainerState = ContactPageState;
