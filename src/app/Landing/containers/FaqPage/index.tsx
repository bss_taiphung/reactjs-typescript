/**
 *
 * FaqPage
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import {
  actions,
  fGetFaqCategoriesRequestSlice,
  reducer,
  sliceKey,
} from './slice';
import { selectFaq } from './selectors';
import { faqPageSaga } from './saga';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { FaqPageStyles } from 'app/Landing/styles/containers/faqpage.styles';
import { FaqCategoryDtoBuilder } from 'app/Landing/services/builder/faq.category.dto.builder';
import { a11yProps, TabPanel, useStyles } from './tab';
import { IFaqCategoryInterface } from 'app/Landing/interfaces/i.faq.category.interface';
import { IFaqInterface } from 'app/Landing/interfaces/i.faq.interface';
import { translations } from 'app/Landing/locales/i18n';

interface Props {}

export const FaqPage = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: faqPageSaga });

  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();

  const { dtoFaqCategories } = useSelector(selectFaq);

  const classes = useStyles();
  const [value, setValue] = useState<number>(0);
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const syncData = () => {
    FaqCategoryDtoBuilder.getInstance().setLocale(i18n.language);
    FaqCategoryDtoBuilder.getInstance().setDtoFaqCategories(dtoFaqCategories);
  };
  syncData();
  useEffect(() => {
    (async () => {
      await dispatch(fGetFaqCategoriesRequestSlice());
    })();
    return () => {
      dispatch(actions.clearData());
    };
  }, []);
  return (
    <>
      <Helmet>
        <title> {t(translations.faq.title)}</title>
        <meta name="description" content={t(translations.faq.description)} />
      </Helmet>
      <div className="section section-title">
        <div className="container">
          <div className="title-section">
            {' '}
            {t(translations.faq.intro.title)}
          </div>
        </div>
      </div>
      <div className="section section-faq">
        <div className="container">
          <div className="customtabs">
            <AppBar position="static">
              <Tabs
                value={value}
                onChange={handleChange}
                variant="scrollable"
                scrollButtons="auto"
              >
                {FaqCategoryDtoBuilder.getInstance()
                  .getDtoFaqCategories()
                  .map((item: IFaqCategoryInterface, index: number) => (
                    <Tab
                      key={item.id}
                      label={item?.translated?.name}
                      {...a11yProps(index)}
                    />
                  ))}
              </Tabs>
            </AppBar>
            {FaqCategoryDtoBuilder.getInstance()
              .getDtoFaqCategories()
              .map((item: IFaqCategoryInterface, index: number) => (
                <div className={'customaccordion'} key={item.id}>
                  <TabPanel value={value} index={index}>
                    {item.faqs.map((faq: IFaqInterface) => (
                      <Accordion key={faq.id}>
                        <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="panel1a-content"
                          id="panel1a-header"
                        >
                          <div className={classes.headingaccordion}>
                            {faq?.translated?.name}
                          </div>
                        </AccordionSummary>
                        <AccordionDetails>
                          {faq?.translated?.content}
                        </AccordionDetails>
                      </Accordion>
                    ))}
                  </TabPanel>
                </div>
              ))}
          </div>
        </div>
      </div>
      <FaqPageStyles />
    </>
  );
});
