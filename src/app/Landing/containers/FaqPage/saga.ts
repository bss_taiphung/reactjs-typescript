// import { take, call, put, select, takeLatest } from 'redux-saga/effects';
// import { actions } from './slice';

import { AxiosPost } from 'utils/axios/axiosPost';
import apiLinks, {
  RESOURCE_FAQ_CATEGORY_NAME,
} from 'app/Landing/config/apiLinks';

export function fGetFaqCategoriesRequestSaga() {
  return AxiosPost(apiLinks[RESOURCE_FAQ_CATEGORY_NAME].findAll, {});
}

export function* faqPageSaga() {
  // yield takeLatest(actions.someAction.type, doSomething);
}
