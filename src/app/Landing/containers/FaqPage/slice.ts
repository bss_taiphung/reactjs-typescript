import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';
import { fGetFaqCategoriesRequestSaga } from './saga';
import { set } from 'lodash';
import { IFaqCategoryInterface } from 'app/Landing/interfaces/i.faq.category.interface';
// The initial state of the FaqPage container
export const initialState: ContainerState = {
  loading: false,
  dtoFaqCategories: [],
};

const faqPageSlice = createSlice({
  name: 'faqPage',
  initialState,
  reducers: {
    clearData: () => initialState,
    showLoading(state, action: PayloadAction<boolean>) {
      set(state, 'isLoading', action.payload);
    },
    setDtoFaqCategories(state, action: PayloadAction<IFaqCategoryInterface[]>) {
      set(state, 'dtoFaqCategories', action.payload);
    },
  },
});

export const { actions, reducer, name: sliceKey } = faqPageSlice;

export function fGetFaqCategoriesRequestSlice() {
  return async dispatch => {
    dispatch(actions.showLoading(true));
    try {
      const data = await fGetFaqCategoriesRequestSaga();
      dispatch(actions.setDtoFaqCategories(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}
