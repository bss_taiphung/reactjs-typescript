/* --- STATE --- */
import { IFaqCategoryInterface } from 'app/Landing/interfaces/i.faq.category.interface';

export interface FaqPageState {
  loading: boolean;
  dtoFaqCategories: IFaqCategoryInterface[];
}

export type ContainerState = FaqPageState;
