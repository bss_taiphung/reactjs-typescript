/**
 *
 * Asynchronously loads the component for FindYourHomeArrivalPage
 *
 */

import { lazyLoad } from 'utils/loadable';

export const FindYourHomeArrivalPage = lazyLoad(
  () => import('./index'),
  module => module.FindYourHomeArrivalPage,
);
