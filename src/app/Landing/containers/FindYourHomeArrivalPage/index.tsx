/**
 *
 * FindYourHomeArrivalPage
 *
 */
import 'date-fns';
import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey } from './slice';
import { selectFindYourHomeArrivalPage } from './selectors';
import { findYourHomeArrivalPageSaga } from './saga';
import { DatePicker } from '@material-ui/pickers';
import { ArrivalPageStyles } from 'app/Landing/styles/containers/arrivalpage.styles';
import { imgDay } from 'app/Landing/assets/images';
import history from 'utils/history';
import routeLinks from 'app/Landing/config/routesLinks';
import { translations } from 'app/Landing/locales/i18n';
import { LazyLoadImage } from 'app/Landing/components/LazyLoadImage';
import { CookiesUtils } from 'app/Landing/utils/cookies/cookies.utils';

interface Props {}

export const FindYourHomeArrivalPage = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: findYourHomeArrivalPageSaga });

  const findYourHomeArrivalPage = useSelector(selectFindYourHomeArrivalPage);
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const [selectedDate, setSelectedDate] = React.useState<any>(new Date());

  const handleDateChange = (date: any) => {
    CookiesUtils.setDateArrival(date.toISOString());
    setSelectedDate(date);
  };
  useEffect(() => {
    const defaultValue = CookiesUtils.getDateArrival();
    if (defaultValue) {
      setSelectedDate(new Date(defaultValue));
    }
  }, []);

  return (
    <>
      <Helmet>
        <title>{t(translations.arrival.title)}</title>
        <meta
          name="description"
          content={t(translations.arrival.description)}
        />
      </Helmet>
      <div>{t('')}</div>
      <div className="section section-arrival">
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-7">
              <div className="img-section">
                <LazyLoadImage src={imgDay} />
              </div>
            </div>
            <div className="col-sm-5">
              <div className="content-section">
                <div className="title-section">
                  {t(translations.arrival.intro.title)}
                </div>
                <div className="text mt-15">
                  {t(translations.arrival.intro.subTitle)}
                </div>
                <div className="datepicker-wapper">
                  <DatePicker
                    disableToolbar
                    disablePast
                    variant="static"
                    value={selectedDate}
                    onChange={handleDateChange}
                  />
                </div>
                <div className="get-action mt-30">
                  <div className="float-right">
                    <button
                      className="btn btn-blue"
                      onClick={() => {
                        history.push({
                          pathname: routeLinks.minimumStay,
                        });
                      }}
                    >
                      {t(translations.arrival.intro.continueText)}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ArrivalPageStyles />
    </>
  );
});
