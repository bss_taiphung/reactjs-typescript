import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) =>
  state.findYourHomeArrivalPage || initialState;

export const selectFindYourHomeArrivalPage = createSelector(
  [selectDomain],
  findYourHomeArrivalPageState => findYourHomeArrivalPageState,
);
