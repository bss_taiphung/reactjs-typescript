import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the FindYourHomeArrivalPage container
export const initialState: ContainerState = {};

const findYourHomeArrivalPageSlice = createSlice({
  name: 'findYourHomeArrivalPage',
  initialState,
  reducers: {
    someAction(state, action: PayloadAction<any>) {},
  },
});

export const {
  actions,
  reducer,
  name: sliceKey,
} = findYourHomeArrivalPageSlice;
