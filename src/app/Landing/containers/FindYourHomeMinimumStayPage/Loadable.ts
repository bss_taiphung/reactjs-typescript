/**
 *
 * Asynchronously loads the component for FindYourHomeMinimumStayPage
 *
 */

import { lazyLoad } from 'utils/loadable';

export const FindYourHomeMinimumStayPage = lazyLoad(
  () => import('./index'),
  module => module.FindYourHomeMinimumStayPage,
);
