/**
 *
 * FindYourHomeMinimumStayPage
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { fGetMinimumStaysRequestSlice, reducer, sliceKey } from './slice';
import { selectFindYourHomeMinimumStayPage } from './selectors';
import { findYourHomeMinimumStayPageSaga } from './saga';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { ArrivalPageStyles } from 'app/Landing/styles/containers/arrivalpage.styles';
import { imgDay } from 'app/Landing/assets/images';
import history from 'utils/history';
import { translations } from 'app/Landing/locales/i18n';
import { LazyLoadImage } from 'app/Landing/components/LazyLoadImage';
import routeLinks from 'app/Landing/config/routesLinks';
import { CookiesUtils } from 'app/Landing/utils/cookies/cookies.utils';

interface Props {}

export const FindYourHomeMinimumStayPage = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: findYourHomeMinimumStayPageSaga });

  const { dtoMinimumStays } = useSelector(selectFindYourHomeMinimumStayPage);
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();

  const [value, setValue] = useState('day1today7');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    CookiesUtils.setMinimumStay((event.target as HTMLInputElement).value);
    setValue((event.target as HTMLInputElement).value);
  };

  useEffect(() => {
    (async () => {
      await dispatch(fGetMinimumStaysRequestSlice());
    })();
    const defaultValue = CookiesUtils.getMinimumStay();
    if (defaultValue) {
      setValue(defaultValue);
    }
    return () => {};
  }, []);

  return (
    <>
      <Helmet>
        <title>{t(translations.minimumStay.title)}</title>
        <meta
          name="description"
          content={t(translations.minimumStay.description)}
        />
      </Helmet>
      <div className="section section-arrival">
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-7">
              <div className="img-section">
                <LazyLoadImage src={imgDay} />
              </div>
            </div>
            <div className="col-sm-5">
              <div className="content-section">
                <div className="title-section">
                  {t(translations.minimumStay.intro.title)}
                </div>
                <div className="text mt-15">
                  {t(translations.minimumStay.intro.subTitle)}
                </div>
                <div className="mt-30 form-radio-days">
                  <FormControl component="fieldset">
                    <RadioGroup
                      aria-label="stay"
                      name="stay1"
                      value={value}
                      onChange={handleChange}
                    >
                      {Object.keys(dtoMinimumStays).map((key: string) => {
                        const item: any = dtoMinimumStays[key];
                        return (
                          <FormControlLabel
                            key={key}
                            value={key}
                            control={<Radio color="primary" />}
                            label={item[i18n.language]}
                          />
                        );
                      })}
                    </RadioGroup>
                  </FormControl>
                </div>
                <div className="get-action mt-30">
                  <div className="float-right">
                    <button
                      className="btn btn-blue"
                      onClick={() => {
                        history.push({
                          pathname: routeLinks.roomsForRent,
                        });
                      }}
                    >
                      {t(translations.minimumStay.intro.continueText)}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ArrivalPageStyles />
    </>
  );
});
