// import { take, call, put, select, takeLatest } from 'redux-saga/effects';
// import { actions } from './slice';

// export function* doSomething() {}
import { AxiosGet } from 'utils/axios/axiosGet';
import apiLinks, {
  RESOURCE_APPOINTMENT_NAME,
} from 'app/Landing/config/apiLinks';

export function fGetMinimumStaysRequestSaga() {
  return AxiosGet(apiLinks[RESOURCE_APPOINTMENT_NAME].getMinimumStays);
}

export function* findYourHomeMinimumStayPageSaga() {
  // yield takeLatest(actions.someAction.type, doSomething);
}
