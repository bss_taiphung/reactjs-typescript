import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) =>
  state.findYourHomeMinimumStayPage || initialState;

export const selectFindYourHomeMinimumStayPage = createSelector(
  [selectDomain],
  findYourHomeMinimumStayPageState => findYourHomeMinimumStayPageState,
);
