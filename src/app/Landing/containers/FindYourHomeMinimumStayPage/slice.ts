import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState, IMinimumStayInterface } from './types';
import { fGetMinimumStaysRequestSaga } from 'app/Landing/containers/FindYourHomeMinimumStayPage/saga';
import { set } from 'lodash';
// The initial state of the FindYourHomeMinimumStayPage container
export const initialState: ContainerState = {
  dtoMinimumStays: {},
};

const findYourHomeMinimumStayPageSlice = createSlice({
  name: 'findYourHomeMinimumStayPage',
  initialState,
  reducers: {
    clearData: () => initialState,
    showLoading(state, action: PayloadAction<boolean>) {
      set(state, 'isLoading', action.payload);
    },
    setDtoMinimumStays(state, action: PayloadAction<IMinimumStayInterface[]>) {
      set(state, 'dtoMinimumStays', action.payload);
    },
  },
});

export const {
  actions,
  reducer,
  name: sliceKey,
} = findYourHomeMinimumStayPageSlice;

// Asynchronous thunk action
export function fGetMinimumStaysRequestSlice() {
  return async dispatch => {
    dispatch(actions.showLoading(true));
    try {
      const data = await fGetMinimumStaysRequestSaga();
      dispatch(actions.setDtoMinimumStays(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}
