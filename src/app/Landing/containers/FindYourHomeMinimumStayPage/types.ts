/* --- STATE --- */
export interface FindYourHomeMinimumStayPageState {
  dtoMinimumStays: IMinimumStayInterface;
}

export type ContainerState = FindYourHomeMinimumStayPageState;

export interface IMinimumStayInterface {
  [key: string]: {
    [local: string]: string;
  };
}
