/**
 *
 * Asynchronously loads the component for FindYourHomeRoomsAppointmentSummaryPage
 *
 */

import { lazyLoad } from 'utils/loadable';

export const FindYourHomeRoomsAppointmentSummaryPage = lazyLoad(
  () => import('./index'),
  module => module.FindYourHomeRoomsAppointmentSummaryPage,
);
