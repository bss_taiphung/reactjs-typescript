import React, { memo, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import {
  actions,
  fCreateAppointmentRequestSlice,
  fFindOthersRoomTypeByIdRequestSlice,
  fFindRoomTypeByIdRequestSlice,
  reducer,
  sliceKey,
} from './slice';
import { selectFindYourHomeRoomsAppointmentSummaryPage } from './selectors';
import { findYourHomeRoomsAppointmentSummaryPageSaga } from './saga';
import { FindRoomDetailPageStyles } from 'app/Landing/styles/containers/find.room.detail.styles';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import history from 'utils/history';
import routeLinks from 'app/Landing/config/routesLinks';
import * as _ from 'lodash';
import { translations } from 'app/Landing/locales/i18n';
import { LazyLoadImage } from 'app/Landing/components/LazyLoadImage';
import { settings } from 'app/Landing/containers/FindYourHomeRoomsAppointmentSummaryPage/constant';
import { RoomTypeDtoBuilder } from 'app/Landing/services/builder/room-type.dto.builder';
import { IAmenityInterface } from 'app/Landing/interfaces/i.amenity.interface';
import {
  IRoomTypeImageInterface,
  IRoomTypeInterface,
} from 'app/Landing/interfaces/i.room.type.interface';
import { DialogAppointmentRequest } from 'app/Landing/components/DialogAppointmentRequest';
import { DialogAppointmentConfirm } from 'app/Landing/components/DialogAppointmentConfirm';
import { getSchemaValidation } from 'app/Landing/containers/FindYourHomeRoomsAppointmentSummaryPage/schema.validation';
import { LoadingSpinner } from 'app/Landing/components/LoadingSpinner';
import { ICreateAppointmentRequest } from 'app/Landing/interfaces/i.appointment.interface';
import { CookiesUtils } from 'app/Landing/utils/cookies/cookies.utils';
import { RoomTypeJsonLd } from 'app/Landing/utils/json-ld/room-type.json-ld';
import { EditorControl } from 'app/Landing/components/EditorControl';

interface Props {
  [key: string]: any;
}

export const FindYourHomeRoomsAppointmentSummaryPage = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({
    key: sliceKey,
    saga: findYourHomeRoomsAppointmentSummaryPageSaga,
  });
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const { id = '' } = useParams();

  const { isLoading, dtoRoomType, dtoRoomTypeOthers } = useSelector(
    selectFindYourHomeRoomsAppointmentSummaryPage,
  );

  RoomTypeDtoBuilder.getInstance().setDto(dtoRoomType);
  RoomTypeDtoBuilder.getInstance().setDtoRoomTypeOthers(dtoRoomTypeOthers);
  RoomTypeDtoBuilder.getInstance().setLocale(i18n.language);
  RoomTypeDtoBuilder.getInstance().syncTranslation();

  const [open, setOpen] = useState<boolean>(false);
  const [isShowAppointment, setIsShowAppointment] = useState<boolean>(
    props?.match?.path === routeLinks.roomsAppointmentSummary,
  );
  const [isShowContinue, setIsShowContinue] = useState<boolean>(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const showContinueDialog = () => {
    setIsShowContinue(true);
    handleClose();
  };

  const closeContinueDialog = () => {
    setIsShowContinue(false);
  };

  const onSubmit = async (request: ICreateAppointmentRequest) => {
    Object.assign(request, {
      roomTypeId: dtoRoomType?.id,
      dateExpectedIn: CookiesUtils.getDateExpectedIn(),
      dateArrival: CookiesUtils.getDateArrival(),
      minimumStay: CookiesUtils.getMinimumStay(),
    });
    await dispatch(fCreateAppointmentRequestSlice(request));
    showContinueDialog();
  };

  const goToRoomTypePage = (item: IRoomTypeInterface) => {
    const defaultValue: string = isShowAppointment
      ? routeLinks.roomsAppointmentSummary
      : routeLinks.roomTypeDetail;
    history.push({
      pathname: defaultValue.replace(':id', item.id),
    });
  };

  useEffect(() => {
    (async () => {
      await dispatch(fFindRoomTypeByIdRequestSlice(id));
      await dispatch(fFindOthersRoomTypeByIdRequestSlice(id));
    })();
    return () => {
      dispatch(actions.clearData());
    };
  }, [id]);

  RoomTypeJsonLd.getInstance().setDto(
    RoomTypeDtoBuilder.getInstance().getDto(),
  );
  return (
    <>
      <Helmet script={RoomTypeJsonLd.getInstance().toJsonLd()}>
        <title>{RoomTypeDtoBuilder.getInstance().getDto()?.name}</title>
        <meta
          name="description"
          content={
            RoomTypeDtoBuilder.getInstance().getDtoTranslated()?.description
          }
        />
      </Helmet>

      {RoomTypeDtoBuilder.getInstance().getDto() && (
        <>
          <div className="section section-booking-detail-carousel">
            <div className="container-fuild">
              <div className="carousel-section">
                <Slider {...settings}>
                  {_.get(
                    RoomTypeDtoBuilder.getInstance().getDto(),
                    'images',
                    [],
                  ).map((item: IRoomTypeImageInterface) => (
                    <div key={item.id}>
                      <LazyLoadImage src={item?.image?.path} />
                    </div>
                  ))}
                </Slider>
              </div>
            </div>
            <div className="container position-relative">
              <div className="content-section">
                <div className="title-section">
                  {_.get(RoomTypeDtoBuilder.getInstance().getDto(), 'name', '')}
                </div>
                <div className="d-sm-flex align-items-center justify-content-between mt-30">
                  <div className="price-text">
                    <b className="d-block">
                      {_.get(
                        RoomTypeDtoBuilder.getInstance().getDto(),
                        'pricePerMonth',
                        '',
                      )}{' '}
                      {_.get(
                        RoomTypeDtoBuilder.getInstance().getDto(),
                        'priceUnit',
                        '',
                      )}
                      {'/'}
                      {t(translations.common.month)}
                    </b>
                    <b className="d-block">
                      {_.get(
                        RoomTypeDtoBuilder.getInstance().getDto(),
                        'pricePerNight',
                        '',
                      )}{' '}
                      {_.get(
                        RoomTypeDtoBuilder.getInstance().getDto(),
                        'priceUnit',
                        '',
                      )}
                      {'/'}
                      {t(translations.common.night)}
                    </b>
                  </div>
                  <div className="service-list">
                    <ul className="list-inline mb-0">
                      {_.get(
                        RoomTypeDtoBuilder.getInstance(),
                        'dtoAmenities',
                        [],
                      ).map((item: IAmenityInterface) => (
                        <li className="list-inline-item" key={item.id}>
                          <LazyLoadImage src={item.image.path} />
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
                {isShowAppointment && (
                  <div className="get-action mt-30">
                    <button className="btn btn-blue" onClick={handleClickOpen}>
                      {t(
                        translations.roomsAppointmentSummary.intro.applyNowText,
                      )}
                    </button>
                  </div>
                )}

                <div className="text mt-30">
                  <EditorControl
                    isViewOnly={true}
                    name={'description'}
                    defaultValue={
                      RoomTypeDtoBuilder.getInstance().getDtoTranslated()
                        ?.description
                    }
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="section section-product">
            <div className="container">
              <div className="title-section">
                {t(translations.roomsAppointmentSummary.outro.otherPlaces)}
              </div>
              <div className="row">
                {RoomTypeDtoBuilder.getInstance()
                  .getDtoRoomTypeOthers()
                  .map((item: IRoomTypeInterface) => (
                    <div
                      className="col-sm-4"
                      key={item.id}
                      onClick={() => {
                        goToRoomTypePage(item);
                      }}
                    >
                      <div className="item-product">
                        <div className="catalogue">{item.name}</div>
                        <div className="img-item">
                          <LazyLoadImage src={item?.image?.path} />
                        </div>
                        <div className="content-item">
                          <div className="price">
                            {_.get(item, 'pricePerMonth', 'N/A')}{' '}
                            {_.get(item, 'priceUnit', 'N/A')}
                          </div>
                          <div className="place d-flex align-items-center">
                            <span className="icon-location"></span>
                            <span>{item?.building?.address}</span>
                          </div>
                          <div className="name">
                            <a>{item?.building?.name}</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </>
      )}
      <DialogAppointmentRequest
        open={open}
        handleClose={handleClose}
        showContinueDialog={showContinueDialog}
        data={RoomTypeDtoBuilder.getInstance().getDto()}
        schemaValidation={getSchemaValidation(t)}
        onSubmit={onSubmit}
      ></DialogAppointmentRequest>
      <DialogAppointmentConfirm
        open={isShowContinue}
        handleClose={closeContinueDialog}
        data={RoomTypeDtoBuilder.getInstance().getDto()}
      ></DialogAppointmentConfirm>
      <FindRoomDetailPageStyles />
      <LoadingSpinner loading={isLoading}></LoadingSpinner>
    </>
  );
});
