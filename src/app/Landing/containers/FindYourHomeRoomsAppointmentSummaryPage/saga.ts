// import { take, call, put, select, takeLatest } from 'redux-saga/effects';
// import { actions } from './slice';

// export function* doSomething() {}
import { AxiosGet } from 'utils/axios/axiosGet';
import apiLinks, {
  RESOURCE_APPOINTMENT_NAME,
  RESOURCE_ROOM_TYPE_NAME,
} from 'app/Landing/config/apiLinks';
import { AxiosPost } from 'utils/axios/axiosPost';
import { ICreateAppointmentRequest } from 'app/Landing/interfaces/i.appointment.interface';

export function fFindRoomTypeByIdRequestSaga(id: string) {
  return AxiosGet(
    apiLinks[RESOURCE_ROOM_TYPE_NAME].findById.replace(':id', id),
  );
}

export function fFindOthersRoomTypeByIdRequestSaga(id: string) {
  return AxiosGet(
    apiLinks[RESOURCE_ROOM_TYPE_NAME].findOthers.replace(':id', id),
  );
}

export function fCreateAppointmentRequestSaga(
  request: ICreateAppointmentRequest,
) {
  return AxiosPost(apiLinks[RESOURCE_APPOINTMENT_NAME].create, request);
}

export function* findYourHomeRoomsAppointmentSummaryPageSaga() {
  // yield takeLatest(actions.someAction.type, doSomething);
}
