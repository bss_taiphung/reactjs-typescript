import * as Yup from 'yup';
import * as yup from 'yup';
import { ObjectSchema } from 'yup';
import { translations } from 'app/Landing/locales/i18n';
import {
  EMAIL_REG_EXP,
  NUMBER_REG_EXP,
  VALIDATION_FORM_VARIANTS,
} from 'app/Landing/config/constant';

export const getSchemaValidation = (t): ObjectSchema => {
  const schema = Yup.object().shape({
    fullName: Yup.string()
      .required(
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .fullName.required,
        ),
      )
      .min(
        VALIDATION_FORM_VARIANTS.fullName.min,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .fullName.length,
          {
            min: VALIDATION_FORM_VARIANTS.fullName.min,
            max: VALIDATION_FORM_VARIANTS.fullName.max,
          },
        ),
      )
      .max(
        VALIDATION_FORM_VARIANTS.fullName.max,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .fullName.length,
          {
            min: VALIDATION_FORM_VARIANTS.fullName.min,
            max: VALIDATION_FORM_VARIANTS.fullName.max,
          },
        ),
      ),
    email: yup
      .string()
      .required(
        t(
          translations.roomsAppointmentSummary.dialog.request.validation.email
            .required,
        ),
      )
      .matches(
        EMAIL_REG_EXP,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation.email
            .typeError,
        ),
      ),
    phoneNumber: yup
      .string()
      .required(
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .phoneNumber.required,
        ),
      )
      .matches(
        NUMBER_REG_EXP,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .phoneNumber.length,
          {
            min: VALIDATION_FORM_VARIANTS.phoneNumber.min,
            max: VALIDATION_FORM_VARIANTS.phoneNumber.max,
          },
        ),
      )
      .max(
        VALIDATION_FORM_VARIANTS.phoneNumber.min,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .phoneNumber.length,
          {
            min: VALIDATION_FORM_VARIANTS.phoneNumber.min,
            max: VALIDATION_FORM_VARIANTS.phoneNumber.max,
          },
        ),
      )
      .max(
        VALIDATION_FORM_VARIANTS.phoneNumber.max,
        t(
          translations.roomsAppointmentSummary.dialog.request.validation
            .phoneNumber.length,
          {
            min: VALIDATION_FORM_VARIANTS.phoneNumber.min,
            max: VALIDATION_FORM_VARIANTS.phoneNumber.max,
          },
        ),
      ),
  });
  return schema;
};
