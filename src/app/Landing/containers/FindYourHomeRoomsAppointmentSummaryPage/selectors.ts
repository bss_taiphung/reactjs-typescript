import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) =>
  state.findYourHomeRoomsAppointmentSummaryPage || initialState;

export const selectFindYourHomeRoomsAppointmentSummaryPage = createSelector(
  [selectDomain],
  findYourHomeRoomsAppointmentSummaryPageState =>
    findYourHomeRoomsAppointmentSummaryPageState,
);
