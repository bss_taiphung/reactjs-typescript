import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';
import { set } from 'lodash';
import {
  fCreateAppointmentRequestSaga,
  fFindOthersRoomTypeByIdRequestSaga,
  fFindRoomTypeByIdRequestSaga,
} from 'app/Landing/containers/FindYourHomeRoomsAppointmentSummaryPage/saga';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';
import { ICreateAppointmentRequest } from 'app/Landing/interfaces/i.appointment.interface';

// The initial state of the FindYourHomeRoomsAppointmentSummaryPage container
export const initialState: ContainerState = {
  isLoading: false,
  dtoRoomType: null,
  dtoRoomTypeOthers: [],
};

const findYourHomeRoomsAppointmentSummaryPageSlice = createSlice({
  name: 'findYourHomeRoomsAppointmentSummaryPage',
  initialState,
  reducers: {
    clearData: () => initialState,
    showLoading(state, action: PayloadAction<boolean>) {
      set(state, 'isLoading', action.payload);
    },
    setDtoRoomType(state, action: PayloadAction<IRoomTypeInterface | null>) {
      set(state, 'dtoRoomType', action.payload);
    },
    setDtoRoomTypeOthers(state, action: PayloadAction<IRoomTypeInterface[]>) {
      set(state, 'dtoRoomTypeOthers', action.payload);
    },
  },
});

export const {
  actions,
  reducer,
  name: sliceKey,
} = findYourHomeRoomsAppointmentSummaryPageSlice;

// Asynchronous thunk action
export function fFindRoomTypeByIdRequestSlice(id: string) {
  return async dispatch => {
    // dispatch(actions.showLoading(true));
    try {
      const data = await fFindRoomTypeByIdRequestSaga(id);
      dispatch(actions.setDtoRoomType(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}

export function fFindOthersRoomTypeByIdRequestSlice(id: string) {
  return async dispatch => {
    // dispatch(actions.showLoading(true));
    try {
      const data = await fFindOthersRoomTypeByIdRequestSaga(id);
      dispatch(actions.setDtoRoomTypeOthers(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}

export function fCreateAppointmentRequestSlice(
  request: ICreateAppointmentRequest,
) {
  return async dispatch => {
    dispatch(actions.showLoading(true));
    try {
      const data = await fCreateAppointmentRequestSaga(request);
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}
