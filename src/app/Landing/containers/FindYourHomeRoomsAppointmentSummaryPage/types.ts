/* --- STATE --- */
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';

export interface FindYourHomeRoomsAppointmentSummaryPageState {
  isLoading: boolean;
  dtoRoomType: IRoomTypeInterface | null;
  dtoRoomTypeOthers: IRoomTypeInterface[];
}

export type ContainerState = FindYourHomeRoomsAppointmentSummaryPageState;
