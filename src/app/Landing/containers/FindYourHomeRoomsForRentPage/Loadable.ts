/**
 *
 * Asynchronously loads the component for FindYourHomeRoomsForRentPage
 *
 */

import { lazyLoad } from 'utils/loadable';

export const FindYourHomeRoomsForRentPage = lazyLoad(
  () => import('./index'),
  module => module.FindYourHomeRoomsForRentPage,
);
