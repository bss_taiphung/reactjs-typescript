/**
 *
 * FindYourHomeRoomsForRentPage
 *
 */

import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { actions, fGetRoomTypesRequestSlice, reducer, sliceKey } from './slice';
import { selectFindYourHomeRoomsForRentPage } from './selectors';
import { findYourHomeRoomsForRentPageSaga } from './saga';
import { HomePageStyles } from 'app/Landing/styles/containers/homepage.styles';
import history from 'utils/history';
import { translations } from 'app/Landing/locales/i18n';
import { LazyLoadImage } from 'app/Landing/components/LazyLoadImage';
import routeLinks from 'app/Landing/config/routesLinks';
import * as _ from 'lodash';
import Pagination from '@material-ui/lab/Pagination';
import { BuildingDtoBuilder } from 'app/Landing/services/builder/building.dto.builder';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';
import { LoadingSpinner } from 'app/Landing/components/LoadingSpinner';

interface Props {}

export const FindYourHomeRoomsForRentPage = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: findYourHomeRoomsForRentPageSaga });

  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const {
    isLoading,
    dtoRoomTypes,
    currentPage,
    totalPages,
    take,
  } = useSelector(selectFindYourHomeRoomsForRentPage);
  BuildingDtoBuilder.getInstance().setLocale(i18n.language);
  BuildingDtoBuilder.getInstance().setDtoRoomTypes(dtoRoomTypes || []);

  const loadPage = async (page: number) => {
    await dispatch(
      fGetRoomTypesRequestSlice({
        skip: (page - 1) * take,
        take,
      }),
    );
  };
  useEffect(() => {
    (async () => {
      await loadPage(1);
    })();
    return () => {
      dispatch(actions.clearData());
    };
  }, []);
  const handleChangePage = async (
    event: React.ChangeEvent<unknown>,
    value: number,
  ) => {
    await loadPage(value);
  };
  return (
    <>
      <Helmet>
        <title>{t(translations.roomsForRent.title)}</title>
        <meta
          name="description"
          content={t(translations.roomsForRent.description)}
        />
      </Helmet>
      <div className="section section-title">
        <div className="container">
          <div className="title-section">
            {t(translations.roomsForRent.intro.title)}
          </div>
        </div>
      </div>
      <div className="section section-product">
        <div className="container">
          <div className="row">
            {BuildingDtoBuilder.getInstance()
              .getDtoRoomTypes()
              .map((item: IRoomTypeInterface) => (
                <div
                  className="col-sm-4"
                  key={item.id}
                  onClick={() => {
                    history.push({
                      pathname: routeLinks.roomsAppointmentSummary.replace(
                        ':id',
                        item.id,
                      ),
                    });
                  }}
                >
                  <div className="item-product">
                    <div className="catalogue">{item?.name}</div>
                    <div className="img-item">
                      <LazyLoadImage src={item?.image?.path} />
                    </div>
                    <div className="content-item">
                      <div className="price">
                        {_.get(item, 'pricePerMonth', '')}{' '}
                        {_.get(item, 'priceUnit', '')}
                      </div>
                      <div className="place d-flex align-items-center">
                        <span className="icon-location"></span>
                        <span>{item?.building?.address}</span>
                      </div>
                      <div className="name">
                        <a> {item?.building?.name}</a>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
      <div className={'row-pagination'}>
        <Pagination
          count={totalPages}
          page={currentPage}
          variant="outlined"
          onChange={handleChangePage}
        />
      </div>
      <HomePageStyles />
      <LoadingSpinner loading={isLoading}></LoadingSpinner>
    </>
  );
});
