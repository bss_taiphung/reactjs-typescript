// import { take, call, put, select, takeLatest } from 'redux-saga/effects';
// import { actions } from './slice';

// export function* doSomething() {}

import apiLinks, { RESOURCE_ROOM_TYPE_NAME } from 'app/Landing/config/apiLinks';
import { AxiosPost } from 'utils/axios/axiosPost';
export function fGetRoomTypesRequestSaga(request) {
  return AxiosPost(
    apiLinks[RESOURCE_ROOM_TYPE_NAME].findAvailableForRentPage,
    request,
  );
}
export function* findYourHomeRoomsForRentPageSaga() {
  // yield takeLatest(actions.someAction.type, doSomething);
}
