import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) =>
  state.findYourHomeRoomsForRentPage || initialState;

export const selectFindYourHomeRoomsForRentPage = createSelector(
  [selectDomain],
  findYourHomeRoomsForRentPageState => findYourHomeRoomsForRentPageState,
);
