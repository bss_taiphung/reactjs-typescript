import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';
import { fGetRoomTypesRequestSaga } from './saga';
import { set } from 'lodash';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';

// The initial state of the FindYourHomeRoomsForRentPage container
export const initialState: ContainerState = {
  isLoading: false,
  take: 6,
  currentPage: 1,
  totalPages: 0,
  dtoRoomTypes: [],
};

const findYourHomeRoomsForRentPageSlice = createSlice({
  name: 'findYourHomeRoomsForRentPage',
  initialState,
  reducers: {
    clearData: () => initialState,
    showLoading(state, action: PayloadAction<boolean>) {
      set(state, 'isLoading', action.payload);
    },
    setDtoRoomTypes(
      state,
      action: PayloadAction<{
        data: IRoomTypeInterface[];
        options: {
          totalPages: number;
          totalItems: number;
          currentPage: number;
        };
      }>,
    ) {
      set(state, 'dtoRoomTypes', action.payload.data);
      set(state, 'totalPages', action.payload.options.totalPages);
      set(state, 'totalItems', action.payload.options.totalItems);
      set(state, 'currentPage', action.payload.options.currentPage);
    },
  },
});

export const {
  actions,
  reducer,
  name: sliceKey,
} = findYourHomeRoomsForRentPageSlice;

export function fGetRoomTypesRequestSlice(request) {
  return async dispatch => {
    dispatch(actions.showLoading(true));
    try {
      const data = await fGetRoomTypesRequestSaga(request);
      dispatch(actions.setDtoRoomTypes(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}
