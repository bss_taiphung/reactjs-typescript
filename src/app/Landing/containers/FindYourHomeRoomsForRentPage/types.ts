/* --- STATE --- */
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';

export interface FindYourHomeRoomsForRentPageState {
  isLoading: boolean;
  take: number;
  totalPages: number;
  currentPage: number;
  dtoRoomTypes: IRoomTypeInterface[];
}

export type ContainerState = FindYourHomeRoomsForRentPageState;
