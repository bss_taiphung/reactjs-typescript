import {
  imgSlider01,
  imgSlider02,
  imgSlider03,
  imgSlider04,
} from 'app/Landing/assets/images';

export const wrapperClass = 'home-page-wrapper';
export const settings = {
  dots: false,
  arrows: false,
  infinite: true,
  speed: 500,
  slidesToShow: 4,
  swipeToSlide: true,
  autoplay: true,
  autoplaySpeed: 3000,
  responsive: [
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 2,
      },
    },
  ],
};
export const settings2 = {
  dots: false,
  arrows: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  swipeToSlide: true,
};

export const imgSliders = [
  imgSlider01,
  imgSlider02,
  imgSlider03,
  imgSlider04,
  imgSlider01,
  imgSlider02,
  imgSlider03,
  imgSlider04,
];
