import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import {
  fGetHomeRequestSlice,
  fGetQuotesRequestSlice,
  fGetRoomTypesRequestSlice,
  reducer,
  sliceKey,
} from './slice';
import { selectHomePage } from './selectors';
import { homePageSaga } from './saga';
import { HomePageStyles } from 'app/Landing/styles/containers/homepage.styles';
import { imgContactBG, imgIntro01 } from 'app/Landing/assets/images';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import history from 'utils/history';
import { translations } from 'app/Landing/locales/i18n';
import { LazyLoadImage } from 'app/Landing/components/LazyLoadImage';
import routeLinks from 'app/Landing/config/routesLinks';
import * as _ from 'lodash';
import {
  settings,
  settings2,
  wrapperClass,
} from 'app/Landing/containers/HomePage/constant';
import { BuildingDtoBuilder } from 'app/Landing/services/builder/building.dto.builder';
import { IAmenityInterface } from 'app/Landing/interfaces/i.amenity.interface';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';
import { IBuildingImageInterface } from 'app/Landing/interfaces/i.building.interface';
import { IQuoteInterface } from 'app/Landing/interfaces/i.quote.interface';
import { BuildingJsonLd } from 'app/Landing/utils/json-ld/building.json-ld';

interface Props {}

export const HomePage = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: homePageSaga });
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();

  const { isLoading, dtoBuilding, dtoRoomTypes, dtoQuotes } = useSelector(
    selectHomePage,
  );
  BuildingDtoBuilder.getInstance().setLocale(i18n.language);
  BuildingDtoBuilder.getInstance().setDto(dtoBuilding);
  BuildingDtoBuilder.getInstance().setDtoRoomTypes(dtoRoomTypes || []);
  BuildingDtoBuilder.getInstance().setDtoQuotes(dtoQuotes);
  BuildingJsonLd.getInstance().setDto(dtoBuilding);

  useEffect(() => {
    (async () => {
      await dispatch(fGetHomeRequestSlice());
      await dispatch(fGetRoomTypesRequestSlice());
      await dispatch(fGetQuotesRequestSlice());
    })();
  }, []);
  return (
    <>
      <div className={`${wrapperClass}`}>
        <Helmet script={BuildingJsonLd.getInstance().toJsonLd()}>
          <title>{t(translations.home.title)}</title>
          <meta name="description" content={t(translations.home.description)} />
        </Helmet>
        <div className="section section-intro">
          <div className="container-fluid">
            <div className="row align-items-center">
              <div className="col-sm-7">
                <div className="img-section">
                  <LazyLoadImage src={imgIntro01} />
                </div>
              </div>
              <div className="col-sm-5">
                <div className="content-section">
                  <div className="title-section">
                    {t(translations.home.intro.title)}
                  </div>
                  <div className="text mt-15">
                    {t(translations.home.intro.subTitle)}
                  </div>
                  <div className="get-action mt-40">
                    <button
                      className="btn btn-blue"
                      onClick={() => {
                        history.push({
                          pathname: routeLinks.arrival,
                        });
                      }}
                    >
                      {t(translations.home.intro.takeATourText)}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="section section-carousel">
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-3 offset-sm-1">
                <div className="title-section">{dtoBuilding?.name}</div>
                <div className="text mt-15">
                  {_.get(
                    BuildingDtoBuilder.getInstance().getDtoTranslated(),
                    'description',
                    '',
                  )}
                </div>
                <div className="list-check mt-40">
                  <ul>
                    {BuildingDtoBuilder.getInstance()
                      .getDtoAmenities()
                      .map((amenity: IAmenityInterface) => (
                        <li key={amenity.translated?.id}>
                          {amenity.translated?.name}
                        </li>
                      ))}
                  </ul>
                </div>
              </div>
              <div className="col-sm-8">
                <div className="content-section">
                  <Slider {...settings}>
                    {_.get(
                      BuildingDtoBuilder.getInstance().getDto(),
                      'images',
                      [],
                    ).map((item: IBuildingImageInterface) => (
                      <div className="w-slider" key={item.id}>
                        <LazyLoadImage src={item?.image?.path} />
                      </div>
                    ))}
                  </Slider>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="section section-product">
          <div className="container">
            <div className="row">
              {BuildingDtoBuilder.getInstance()
                .getDtoRoomTypes()
                .map((item: IRoomTypeInterface) => (
                  <div
                    className="col-sm-4"
                    key={item.id}
                    onClick={() => {
                      history.push({
                        pathname: routeLinks.roomTypeDetail.replace(
                          ':id',
                          item.id,
                        ),
                      });
                    }}
                  >
                    <div className="item-product">
                      <div className="catalogue">{item?.name}</div>
                      <div className="img-item">
                        <LazyLoadImage src={item?.image?.path} />
                      </div>
                      <div className="content-item">
                        <div className="price">
                          {_.get(item, 'pricePerMonth', 'N/A')}{' '}
                          {_.get(item, 'priceUnit', 'N/A')}
                        </div>
                        <div className="place d-flex align-items-center">
                          <span className="icon-location"></span>
                          <span>{item?.building?.address}</span>
                        </div>
                        <div className="name">
                          <a> {item?.building?.name}</a>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
        <div className="section section-testimonial">
          <div className="container">
            <div className="title-section text-center">
              {' '}
              {t(translations.home.quotes.title)}
            </div>
            <div className="testimonial-carousel">
              <Slider {...settings2}>
                {BuildingDtoBuilder.getInstance()
                  .getDtoQuotes()
                  .map((item: IQuoteInterface) => (
                    <div className="item-testimonial" key={item.id}>
                      <div className="wrapper-item">
                        <div className="img-item">
                          <LazyLoadImage src={item?.image.path} />
                        </div>
                        <div className="text-item mt-10">
                          {item?.translated?.description}
                        </div>
                        <div className="name mt-10">{item?.customerName}</div>
                      </div>
                    </div>
                  ))}
              </Slider>
            </div>
          </div>
        </div>
        <div className="section section-approach">
          <div className="background-section">
            <LazyLoadImage src={imgContactBG} />
          </div>
          <div className="content-section">
            <div className="container">
              <div className="row">
                <div className="col-sm-8 mx-auto text-center">
                  <div className="title-section">
                    {t(translations.home.outro.title)}
                  </div>
                  <div className="text mt-30">
                    {t(translations.home.outro.subTitle)}
                  </div>
                  <div className="get-action mt-30">
                    <button
                      className="btn btn-blue"
                      onClick={() => {
                        history.push({
                          pathname: routeLinks.arrival,
                        });
                      }}
                    >
                      {t(translations.home.outro.startText)}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <HomePageStyles />
    </>
  );
});
