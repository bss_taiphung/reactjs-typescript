// import { take, call, put, select, takeLatest } from 'redux-saga/effects';
// import { actions } from './slice';

// export function* doSomething() {}
import apiLinks, {
  RESOURCE_BUILDING_NAME,
  RESOURCE_QUOTE_NAME,
  RESOURCE_ROOM_TYPE_NAME,
} from 'app/Landing/config/apiLinks';
import { AxiosGet } from 'utils/axios/axiosGet';
import { AxiosPost } from 'utils/axios/axiosPost';

export function fGetHomeRequestSaga() {
  return AxiosGet(apiLinks[RESOURCE_BUILDING_NAME].findOne);
}

export function fGetRoomTypesRequestSaga() {
  return AxiosGet(apiLinks[RESOURCE_ROOM_TYPE_NAME].findAvailableHomePage);
}

export function fGetQuotesRequestSaga() {
  return AxiosPost(apiLinks[RESOURCE_QUOTE_NAME].findAll, {});
}

export function* homePageSaga() {
  // yield takeLatest(actions.someAction.type, doSomething);
}
