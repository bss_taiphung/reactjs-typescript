import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { set } from 'lodash';
import { ContainerState } from './types';
import {
  fGetHomeRequestSaga,
  fGetQuotesRequestSaga,
  fGetRoomTypesRequestSaga,
} from 'app/Landing/containers/HomePage/saga';
import { IBuildingInterface } from 'app/Landing/interfaces/i.building.interface';
import { IQuoteInterface } from 'app/Landing/interfaces/i.quote.interface';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';

// The initial state of the HomePage container
export const initialState: ContainerState = {
  isLoading: false,
  dtoBuilding: null,
  dtoRoomTypes: [],
  dtoQuotes: [],
};

const homePageSlice = createSlice({
  name: 'homePage',
  initialState,
  reducers: {
    clearData: () => initialState,
    showLoading(state, action: PayloadAction<boolean>) {
      set(state, 'isLoading', action.payload);
    },
    setDtoBuilding(state, action: PayloadAction<IBuildingInterface | null>) {
      set(state, 'dtoBuilding', action.payload);
    },
    setDtoRoomTypes(state, action: PayloadAction<IRoomTypeInterface[]>) {
      set(state, 'dtoRoomTypes', action.payload);
    },
    setDtoQuotes(state, action: PayloadAction<IQuoteInterface[]>) {
      set(state, 'dtoQuotes', action.payload);
    },
  },
});

export const { actions, reducer, name: sliceKey } = homePageSlice;

// Asynchronous thunk action
export function fGetHomeRequestSlice() {
  return async dispatch => {
    dispatch(actions.showLoading(true));
    try {
      const data = await fGetHomeRequestSaga();
      dispatch(actions.setDtoBuilding(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}

export function fGetRoomTypesRequestSlice() {
  return async dispatch => {
    dispatch(actions.showLoading(true));
    try {
      const data = await fGetRoomTypesRequestSaga();
      dispatch(actions.setDtoRoomTypes(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.setDtoRoomTypes([]));
      dispatch(actions.showLoading(false));
    }
  };
}

export function fGetQuotesRequestSlice() {
  return async dispatch => {
    dispatch(actions.showLoading(true));
    try {
      const data = await fGetQuotesRequestSaga();
      dispatch(actions.setDtoQuotes(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}
