/* --- STATE --- */
import { IBuildingInterface } from 'app/Landing/interfaces/i.building.interface';
import { IQuoteInterface } from 'app/Landing/interfaces/i.quote.interface';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';

export interface HomePageState {
  isLoading: boolean;
  dtoBuilding: IBuildingInterface | null;
  dtoRoomTypes: IRoomTypeInterface[];
  dtoQuotes: IQuoteInterface[];
}

export type ContainerState = HomePageState;
