import { Footer } from 'app/Layout/LandingLayout/components/Footer';
import React from 'react';

const withFooter = Component => props => {
  return (
    <>
      <Component {...props} />
      <Footer />
    </>
  );
};
export default withFooter;
