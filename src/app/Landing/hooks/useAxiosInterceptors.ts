import { useEffect, useState } from 'react';
import { interceptorsAxiosListener } from 'utils/axios/axios-client';

const useAxiosInterceptors = () => {
  const [loadingCount, setLoadingCount] = useState(0);
  useEffect(() => {
    interceptorsAxiosListener(setLoadingCount);
  }, []);

  return loadingCount;
};

export default useAxiosInterceptors;
