import { isEmpty } from 'lodash';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getCookie } from 'utils/helper';
import useAxiosInterceptors from './useAxiosInterceptors';

const useFetchCookiesAccessToken = () => {
  const [accessToken, setAccessToken] = useState();
  const dispatch = useDispatch();
  useAxiosInterceptors();

  useEffect(() => {
    const token = getCookie('accessToken');
    setAccessToken(token);
    if (isEmpty(token)) {
      // call api get accessToken for guest
      // dispatch(actions.fetchGuestAccessToken());
    }
  }, [dispatch]);

  return accessToken;
};

export default useFetchCookiesAccessToken;
