import { HomePage } from 'app/Landing/containers/HomePage/Loadable';
import { FindYourHomeArrivalPage } from 'app/Landing/containers/FindYourHomeArrivalPage/Loadable';
import { FindYourHomeMinimumStayPage } from 'app/Landing/containers/FindYourHomeMinimumStayPage/Loadable';
import { FindYourHomeRoomsForRentPage } from 'app/Landing/containers/FindYourHomeRoomsForRentPage/Loadable';
import { FindYourHomeRoomsAppointmentSummaryPage } from 'app/Landing/containers/FindYourHomeRoomsAppointmentSummaryPage/Loadable';
import { FaqPage } from 'app/Landing/containers/FaqPage/Loadable';
import { ContactPage } from 'app/Landing/containers/ContactPage/Loadable';
import { BlogsPage } from 'app/Landing/containers/BlogsPage/Loadable';
import { BlogDetailPage } from 'app/Landing/containers/BlogDetailPage/Loadable';
// import { NotFoundPage } from 'app/Landing/containers/NotFoundPage/Loadable';
import routeLinksLanding from 'app/Landing/config/routesLinks';
import { LandingLayout } from 'app/Layout/LandingLayout/Loadable';
import SubRoutesLayout from 'app/Shared/Route/sub.routes.layout';

const landingRoutes = [
  {
    path: '/',
    component: LandingLayout,
    routes: [
      {
        path: routeLinksLanding.landingRoot,
        exact: true,
        component: HomePage,
      },
      {
        path: routeLinksLanding.arrival,
        component: FindYourHomeArrivalPage,
      },
      {
        path: routeLinksLanding.minimumStay,
        component: FindYourHomeMinimumStayPage,
      },
      {
        path: routeLinksLanding.roomsForRent,
        component: FindYourHomeRoomsForRentPage,
      },
      {
        path: routeLinksLanding.roomsAppointmentSummary,
        component: FindYourHomeRoomsAppointmentSummaryPage,
      },
      {
        exact: true,
        path: routeLinksLanding.roomTypeDetail,
        component: FindYourHomeRoomsAppointmentSummaryPage,
      },
      {
        exact: true,
        path: routeLinksLanding.faq,
        component: FaqPage,
      },
      {
        path: routeLinksLanding.contact,
        component: ContactPage,
      },
      {
        path: routeLinksLanding.blogs,
        component: SubRoutesLayout,
        routes: [
          {
            exact: true,
            path: routeLinksLanding.blogs,
            component: BlogsPage,
          },
          {
            exact: true,
            path: routeLinksLanding.blogsId,
            component: BlogDetailPage,
          },
        ],
      },
    ],
  },
  // {
  //   component: NotFoundPage,
  // },
];
export default landingRoutes;
