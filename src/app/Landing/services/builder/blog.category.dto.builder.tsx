import * as _ from 'lodash';
import { IBlogCategoryInterface } from 'app/Landing/interfaces/i.blog.category.interface';

export class BlogCategoryDtoBuilder {
  dtoBlogCategories: IBlogCategoryInterface[] = [];

  locale: string = 'vi';

  private static instance: BlogCategoryDtoBuilder;

  public static getInstance(): BlogCategoryDtoBuilder {
    if (!BlogCategoryDtoBuilder.instance) {
      BlogCategoryDtoBuilder.instance = new BlogCategoryDtoBuilder();
    }
    return BlogCategoryDtoBuilder.instance;
  }

  public setLocale(locale: string): void {
    BlogCategoryDtoBuilder.getInstance().locale = locale;
  }

  public getLocale(): string {
    return BlogCategoryDtoBuilder.getInstance().locale;
  }

  public setDtoBlogCategories(
    dtoBlogCategories: IBlogCategoryInterface[],
  ): void {
    BlogCategoryDtoBuilder.getInstance().dtoBlogCategories = dtoBlogCategories;
    this.getTranslationDtoBlogCategories();
  }

  public getDtoBlogCategories(): IBlogCategoryInterface[] {
    return _.get(BlogCategoryDtoBuilder.getInstance(), 'dtoBlogCategories', []);
  }

  public getTranslationDtoBlogCategories(): void {
    BlogCategoryDtoBuilder.getInstance().dtoBlogCategories = _.get(
      BlogCategoryDtoBuilder.getInstance(),
      'dtoBlogCategories',
      [],
    ).map((item: IBlogCategoryInterface) => {
      return {
        ...item,
        translated: _.find(item.translations, {
          locale: BlogCategoryDtoBuilder.getInstance().locale,
        }),
      };
    });
  }
}
