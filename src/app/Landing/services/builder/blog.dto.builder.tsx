import * as _ from 'lodash';
import { IBlogInterface } from 'app/Landing/interfaces/i.blog.interface';

export class BlogDtoBuilder {
  dto: IBlogInterface | null = null;
  dtoBlogs: IBlogInterface[] = [];

  locale: string = 'vi';

  private static instance: BlogDtoBuilder;

  public static getInstance(): BlogDtoBuilder {
    if (!BlogDtoBuilder.instance) {
      BlogDtoBuilder.instance = new BlogDtoBuilder();
    }
    return BlogDtoBuilder.instance;
  }

  public setLocale(locale: string): void {
    BlogDtoBuilder.getInstance().locale = locale;
  }

  public getLocale(): string {
    return BlogDtoBuilder.getInstance().locale;
  }

  public setDto(dto: IBlogInterface | null): void {
    BlogDtoBuilder.getInstance().dto = dto;
    this.getTranslation();
  }

  public getDto(): IBlogInterface | null {
    return _.get(BlogDtoBuilder.getInstance(), 'dto', null);
  }

  public getTranslation(): void {
    if (BlogDtoBuilder.getInstance().dto) {
      BlogDtoBuilder.getInstance().dto = {
        ..._.get(BlogDtoBuilder.getInstance(), 'dto', {}),
        translated: _.find(
          _.get(BlogDtoBuilder.getInstance().dto, 'translations', []),
          {
            locale: BlogDtoBuilder.getInstance().locale,
          },
        ),
      };
    }
  }

  public setDtoBlogs(dtoBlogs: IBlogInterface[]): void {
    BlogDtoBuilder.getInstance().dtoBlogs = dtoBlogs;
    this.getTranslationDtoBlogs();
  }

  public getDtoBlogs(): IBlogInterface[] {
    return _.get(BlogDtoBuilder.getInstance(), 'dtoBlogs', {});
  }

  public getTranslationDtoBlogs(): void {
    BlogDtoBuilder.getInstance().dtoBlogs = _.get(
      BlogDtoBuilder.getInstance(),
      'dtoBlogs',
      [],
    ).map((item: IBlogInterface) => {
      return {
        ...item,
        translated: _.find(item.translations, {
          locale: BlogDtoBuilder.getInstance().locale,
        }),
        blogCategory: {
          ...item.blogCategory,
          translated: _.find(item.blogCategory.translations, {
            locale: BlogDtoBuilder.getInstance().locale,
          }),
        },
      };
    });
  }
}
