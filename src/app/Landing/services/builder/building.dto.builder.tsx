import * as _ from 'lodash';
import {
  IBuildingAmenityInterface,
  IBuildingInterface,
  IBuildingTranslationInterface,
} from 'app/Landing/interfaces/i.building.interface';
import { IAmenityInterface } from 'app/Landing/interfaces/i.amenity.interface';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';
import { IQuoteInterface } from 'app/Landing/interfaces/i.quote.interface';

export class BuildingDtoBuilder {
  dto: IBuildingInterface | null = null;
  dtoTranslated: IBuildingTranslationInterface | null = null;
  dtoQuotes: IQuoteInterface[] = [];
  dtoAmenities: IAmenityInterface[] = [];
  dtoRoomTypes: IRoomTypeInterface[] = [];

  locale: string = 'vi';

  private static instance: BuildingDtoBuilder;

  public static getInstance(): BuildingDtoBuilder {
    if (!BuildingDtoBuilder.instance) {
      BuildingDtoBuilder.instance = new BuildingDtoBuilder();
    }
    return BuildingDtoBuilder.instance;
  }

  public setLocale(locale: string): void {
    BuildingDtoBuilder.getInstance().locale = locale;
  }

  public getLocale(): string {
    return BuildingDtoBuilder.getInstance().locale;
  }

  public setDto(dto: IBuildingInterface | null): void {
    BuildingDtoBuilder.getInstance().dto = dto;
    this.getTranslation();
    this.getTranslationAmenities();
  }

  public getDto(): IBuildingInterface | null {
    return BuildingDtoBuilder.getInstance().dto;
  }

  public getDtoTranslated(): IBuildingTranslationInterface | null {
    return BuildingDtoBuilder.getInstance().dtoTranslated;
  }

  public setDtoRoomTypes(dtoRoomTypes: IRoomTypeInterface[]): void {
    BuildingDtoBuilder.getInstance().dtoRoomTypes = dtoRoomTypes;
    this.getTranslationRoomTypes();
  }

  public getDtoRoomTypes(): IRoomTypeInterface[] {
    return _.get(BuildingDtoBuilder.getInstance(), 'dtoRoomTypes', []);
  }

  public setDtoQuotes(dtoQuotes: IQuoteInterface[]): void {
    BuildingDtoBuilder.getInstance().dtoQuotes = dtoQuotes;
    this.getTranslationQuotes();
  }

  public getDtoQuotes(): IQuoteInterface[] {
    return _.get(BuildingDtoBuilder.getInstance(), 'dtoQuotes', []);
  }

  public getDtoAmenities(): IAmenityInterface[] {
    return _.get(BuildingDtoBuilder.getInstance(), 'dtoAmenities', []);
  }

  public getTranslation(): void {
    BuildingDtoBuilder.getInstance().dtoTranslated = _.find(
      _.get(BuildingDtoBuilder.getInstance().dto, 'translations', []),
      {
        locale: BuildingDtoBuilder.getInstance().locale,
      },
    );
  }

  public getTranslationAmenities(): void {
    BuildingDtoBuilder.getInstance().dtoAmenities = _.get(
      BuildingDtoBuilder.getInstance().dto,
      'amenities',
      [],
    ).map((item: IBuildingAmenityInterface) => {
      return {
        ...item.amenity,
        translated: _.find(item.amenity.translations, {
          locale: BuildingDtoBuilder.getInstance().locale,
        }),
      };
    });
  }

  public getTranslationQuotes(): void {
    BuildingDtoBuilder.getInstance().dtoQuotes = _.get(
      BuildingDtoBuilder.getInstance(),
      'dtoQuotes',
      [],
    ).map((item: IQuoteInterface) => {
      return {
        ...item,
        translated: _.find(item.translations, {
          locale: BuildingDtoBuilder.getInstance().locale,
        }),
      };
    });
  }

  public getTranslationRoomTypes(): void {
    BuildingDtoBuilder.getInstance().dtoRoomTypes = _.get(
      BuildingDtoBuilder.getInstance(),
      'dtoRoomTypes',
      [],
    ).map((item: IRoomTypeInterface) => {
      return {
        ...item,
        translated: _.find(item.translations, {
          locale: BuildingDtoBuilder.getInstance().locale,
        }),
      };
    });
  }
}
