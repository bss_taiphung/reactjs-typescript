import * as _ from 'lodash';
import { IFaqCategoryInterface } from 'app/Landing/interfaces/i.faq.category.interface';
import { IFaqInterface } from 'app/Landing/interfaces/i.faq.interface';

export class FaqCategoryDtoBuilder {
  dtoFaqCategories: IFaqCategoryInterface[] = [];

  locale: string = 'vi';

  private static instance: FaqCategoryDtoBuilder;

  public static getInstance(): FaqCategoryDtoBuilder {
    if (!FaqCategoryDtoBuilder.instance) {
      FaqCategoryDtoBuilder.instance = new FaqCategoryDtoBuilder();
    }
    return FaqCategoryDtoBuilder.instance;
  }

  public setLocale(locale: string): void {
    FaqCategoryDtoBuilder.getInstance().locale = locale;
  }

  public getLocale(): string {
    return FaqCategoryDtoBuilder.getInstance().locale;
  }

  public setDtoFaqCategories(dtoFaqCategories: IFaqCategoryInterface[]): void {
    FaqCategoryDtoBuilder.getInstance().dtoFaqCategories = dtoFaqCategories;
    this.getTranslationDtoFaqCategories();
  }

  public getDtoFaqCategories(): IFaqCategoryInterface[] {
    return _.get(FaqCategoryDtoBuilder.getInstance(), 'dtoFaqCategories', []);
  }

  public getTranslationDtoFaqCategories(): void {
    FaqCategoryDtoBuilder.getInstance().dtoFaqCategories = _.get(
      FaqCategoryDtoBuilder.getInstance(),
      'dtoFaqCategories',
      [],
    ).map((item: IFaqCategoryInterface) => {
      return {
        ...item,
        translated: _.find(item.translations, {
          locale: FaqCategoryDtoBuilder.getInstance().locale,
        }),
        faqs: item.faqs.map((faq: IFaqInterface) => {
          return {
            ...faq,
            translated: _.find(faq.translations, {
              locale: FaqCategoryDtoBuilder.getInstance().locale,
            }),
          };
        }),
      };
    });
  }
}
