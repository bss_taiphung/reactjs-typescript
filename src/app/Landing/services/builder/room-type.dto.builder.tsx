import * as _ from 'lodash';
import {
  IBuildingAmenityInterface,
  IBuildingTranslationInterface,
} from 'app/Landing/interfaces/i.building.interface';
import { IAmenityInterface } from 'app/Landing/interfaces/i.amenity.interface';
import { IRoomTypeInterface } from 'app/Landing/interfaces/i.room.type.interface';

export class RoomTypeDtoBuilder {
  dto: IRoomTypeInterface | null = null;
  dtoTranslated: IBuildingTranslationInterface | null = null;
  dtoAmenities: IAmenityInterface[] = [];

  dtoRoomTypeOthers: IRoomTypeInterface[] = [];

  locale: string = 'vi';

  private static instance: RoomTypeDtoBuilder;

  public static getInstance(): RoomTypeDtoBuilder {
    if (!RoomTypeDtoBuilder.instance) {
      RoomTypeDtoBuilder.instance = new RoomTypeDtoBuilder();
    }
    return RoomTypeDtoBuilder.instance;
  }

  public setLocale(locale: string): void {
    RoomTypeDtoBuilder.getInstance().locale = locale;
  }

  public getLocale(): string {
    return RoomTypeDtoBuilder.getInstance().locale;
  }

  public setDto(dto: IRoomTypeInterface | null): void {
    RoomTypeDtoBuilder.getInstance().dto = dto;
  }

  public getDto(): IRoomTypeInterface | null {
    return RoomTypeDtoBuilder.getInstance().dto;
  }

  public getDtoTranslated(): IBuildingTranslationInterface | null {
    return RoomTypeDtoBuilder.getInstance().dtoTranslated;
  }

  public setDtoRoomTypeOthers(dtoRoomTypeOthers: IRoomTypeInterface[]): void {
    RoomTypeDtoBuilder.getInstance().dtoRoomTypeOthers = dtoRoomTypeOthers;
  }

  public getDtoRoomTypeOthers(): IRoomTypeInterface[] {
    return RoomTypeDtoBuilder.getInstance().dtoRoomTypeOthers;
  }

  public syncTranslation(): void {
    this.getTranslation();
    this.getAmenities();
  }

  public getTranslation(): void {
    RoomTypeDtoBuilder.getInstance().dtoTranslated = _.find(
      _.get(RoomTypeDtoBuilder.getInstance().dto, 'translations', []),
      {
        locale: RoomTypeDtoBuilder.getInstance().locale,
      },
    );
  }

  public getAmenities(): void {
    RoomTypeDtoBuilder.getInstance().dtoAmenities = _.get(
      RoomTypeDtoBuilder.getInstance().dto,
      'amenities',
      [],
    ).map((item: IBuildingAmenityInterface) => {
      return {
        ...item.amenity,
        translated: _.find(item.amenity.translations, {
          locale: RoomTypeDtoBuilder.getInstance().locale,
        }),
      };
    });
  }
}
