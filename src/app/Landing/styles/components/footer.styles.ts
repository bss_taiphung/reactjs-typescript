import { createGlobalStyle } from 'styled-components';
import { sizes } from 'styles/media-screens';
import { toRem } from 'styles/StyleConstants';

export const FooterStyles = createGlobalStyle`
.footer-wrapper {
  padding: ${toRem(20)} 0;
  .navbar-footer{
    padding: ${toRem(20)} 0;
    @media(max-width: ${sizes.small}px){
      display: block;
    }
    + .navbar-footer{
      border-top: 1px solid rgba(205, 211, 220, 0.5);
      font-size: ${toRem(14)};
      color: ${p => p.theme.textmenu};

    }
    .nav{
      & + .nav{
        .nav-link{
          font-size: ${toRem(20)};
          padding: 0 ${toRem(12)};
        }
      }
      .nav-link{
        font-size: ${toRem(14)};
        color: ${p => p.theme.textmenu};
        padding: 0 ${toRem(35)};
        @media(max-width: ${sizes.small}px){
          padding: ${toRem(15)};
          &:first-child{
            padding-left: 0;
          }
        }
      }
    }
  }
}

`;
