import { createGlobalStyle } from 'styled-components';
import { sizes } from 'styles/media-screens';
import { toRem } from 'styles/StyleConstants';

export const HeaderStyles = createGlobalStyle`
.header-wrapper {
  z-index: 1000;
}
.navbar-coliving{
  padding: ${toRem(26)} 0;
  @media(max-width: ${sizes.small}px){
    padding: ${toRem(26)} ${toRem(15)};
  }
  .navbar-brand{
    margin-right: ${toRem(30)};
  }
  .navbar-nav {
    .active {
      > .nav-link{
        color: ${p => p.theme.textmenuActive};
        font-weight: bold;
      }
    }
    .nav-link {
      font-size: ${toRem(14)};
      color: ${p => p.theme.textmenu};
      padding: 0 ${toRem(35)};
      @media(max-width: ${sizes.small}px){
        padding: ${toRem(15)} 0;
      }
      &.show, &.active {
        color: ${p => p.theme.textmenuActive};
      }
      &:hover, &:focus {
        color: ${p => p.theme.textmenuActive};
      }
      img{
        margin-right: ${toRem(5)};
        width: ${toRem(20)};
      }
    }
    .dropdown-menu{
      top: ${toRem(30)};
      left: ${toRem(35)};
      min-width: inherit;
      padding: 0;
      margin: 0;
      img{
        margin-right: ${toRem(5)};
        width: ${toRem(20)};
      }
      a{
        padding: ${toRem(10)} ;
        font-size: ${toRem(14)};
      }
    }
    .dropdown-language a{
      color: ${p => p.theme.textmenu};
      font-weight: bold;
    }
  }
}
`;
