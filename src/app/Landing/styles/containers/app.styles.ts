import { createGlobalStyle } from 'styled-components';
// import { StyleConstants } from '../StyleConstants';

export const AppStyles = createGlobalStyle`
.loading-custom {
  &-wrapper {
    height: 100%;
    width: 100%;
    overflow: auto;
    position: fixed;
    &--active {
      z-index: 1;
    }
  }

  &-overlay {
    z-index: 2;
  }
}

.my-toast-container {
  padding: 0;
  bottom: 0;
  width: auto;
}

.my-toast {
  bottom: 0;
  margin: 0 auto;
  padding: 2px 20px;
  border-radius: 8px;
  background-color: ${p => p.theme.ui1ColorFillColor4};
  height: auto;
  min-height: unset;
  width: auto;

  &.black-theme {
    bottom: 50px;
    background-color: ${p => p.theme.ui1ColorFillColor1};
    padding: 6px 20px;
    .my-toast-body {
      display: flex;
      align-items: center;
      font-family: Gilroy;
      font-size: 14px;
      font-weight: 600;
      font-stretch: normal;
      font-style: normal;
      line-height: 1.57;
      letter-spacing: normal;
      color: ${p => p.theme.uiWhite};
    }
  }

  .my-toast-body {
    margin: 0 auto;
    font-size: 14px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.57;
    letter-spacing: normal;
    text-align: center;
    color: ${p => p.theme.uiWhite};

    .icon {
      margin-left: 20px;
    }
  }
}

.cursor-pointer {
  cursor: pointer;
}
.text-error {
  font-family: Gilroy;
  font-size: 12px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  background-color: ${p => p.theme.ui1ColorFillColor4};
}

`;
