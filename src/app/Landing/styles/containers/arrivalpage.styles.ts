import { createGlobalStyle } from 'styled-components';
import { sizes } from 'styles/media-screens';
import { toRem } from 'styles/StyleConstants';

export const ArrivalPageStyles = createGlobalStyle`
	.section{
		&.section-arrival{
			padding: 0;
			.img-section{
				margin-left: ${toRem(-15)};
				@media(max-width: ${sizes.small}px){
			    margin-left: 0;
			  }
			}
			.content-section{
				padding: ${toRem(30)} 0;
			}
			.title-section{
				color: ${p => p.theme.textmenuActive};
			}
			.datepicker-wapper{
				margin-top: ${toRem(50)};
				.MuiPickersBasePicker-pickerView{
					margin: 0 auto;
					min-width: ${toRem(438)};
    			min-height: ${toRem(355)};
				}
				.MuiPickersCalendarHeader-transitionContainer{
					height: ${toRem(26)};
					> *{
						font-weight: bold;
						color: ${p => p.theme.textBlack};
						font-size: ${toRem(16)};
					}
				}
				.MuiTypography-caption{
					font-size: ${toRem(14)};
				}
				.MuiPickersCalendarHeader-dayLabel{
					margin: 0 ${toRem(7)};
					width: ${toRem(48)};
				}
				.MuiPickersDay-day{
					width: ${toRem(48)};
			    height: ${toRem(48)};
			    margin: 0 ${toRem(7)};
			    font-size: ${toRem(16)};
				}
				.MuiPickersDay-day .MuiIconButton-label > *{
					font-size: ${toRem(16)};
				}
				.MuiPickersDay-daySelected{
					background-color: ${p => p.theme.textmenuActive};
				}
			}
			.form-radio-days{
				.MuiFormControlLabel-label{
					font-family: 'SVN-Poppins';
					font-weight: 600;
  				font-size: ${toRem(14)};
  				margin-left: ${toRem(10)};
  				color: ${p => p.theme.textTitle};
				}
				.MuiFormControlLabel-root {
			    margin-left: 0;
			    margin-right: 0;
			    margin-bottom: ${toRem(15)};
				}
				.MuiRadio-root{
					padding: 0;
				}
			}
		}
	}
`;
