import { createGlobalStyle } from 'styled-components';
import { sizes } from 'styles/media-screens';
import { toRem } from 'styles/StyleConstants';

export const ContactPageStyles = createGlobalStyle`
	.section{
		&.section-contact{
			color: ${p => p.theme.textBody2};
			.map-section{
				overflow: hidden;
		    padding-bottom: 100%;
		    position: relative;
		    height: 0;
		    -ms-flex: 0 0 100%;
		    -webkit-box-flex: 0;
		    flex: 0 0 100%;
		    max-width: 100%;
		    @media (max-width: ${sizes.small}px) {
		    	margin-top: ${toRem(30)};
		    }
		    iframe {
			    left: 0;
			    top: 0;
			    height: 100%;
			    width: 100%;
			    position: absolute;
			    border: 0;
				}
			}
			.content-section{
				[class^='icon-'],
				[class*=' icon-']{
					font-size: ${toRem(24)};
					margin-right: ${toRem(15)};
				}
			}
		}
	}
`;
