import { createGlobalStyle } from 'styled-components';
import { sizes } from 'styles/media-screens';
import { toRem } from 'styles/StyleConstants';

export const FaqPageStyles = createGlobalStyle`
	.section{
		&.section-faq{
			.customtabs {
				.MuiAppBar-colorPrimary{
					text-transform: uppercase;
					box-shadow: none;
					border-bottom: 1px solid #E6E9ED;
					background-color: ${p => p.theme.textWhite};
					color: ${p => p.theme.textmenu};
				}
				.MuiTab-root{
					padding: 0;
					font-weight: normal;
					letter-spacing: normal;
					line-height: 1.3;
					min-width: inherit;
					padding-bottom: ${toRem(35)};
					padding-right: ${toRem(35)};
					font-size: ${toRem(16)};
					@media (max-width: ${sizes.small}px) {
						padding-right: ${toRem(15)};
					}
				}
				.MuiTab-textColorInherit{
					opacity: 1;
					font-family: 'SVN-Poppins';
					text-align: left;
					&.Mui-selected{
						font-weight: bold;
						color: #0093E6;
					}
				}
				.MuiTabs-indicator{
					background-color: transparent;
				}
				.MuiTouchRipple-root{
					display: none;
				}
				.MuiBox-root{
					padding-left: 0;
					padding-right: 0;
					padding-top: ${toRem(30)};
				}
			}
			.customaccordion{
				.MuiTypography-root{
					letter-spacing: 0;
					font-weight: normal;
					font-family: 'SVN-Poppins';
					line-height: 1.3;
					font-size: ${toRem(16)};
				}
				.MuiAccordionSummary-root{
					&.Mui-expanded {
						border-bottom: 1px solid #E6E9ED;
					}
					.MuiTypography-root{
						font-weight: 600;
						color: ${p => p.theme.textmenu};
					}
				}
				.MuiAccordionDetails-root{
					padding-left:  ${toRem(45)};
					padding-right:  ${toRem(45)};
					@media (max-width: ${sizes.small}px) {
						padding-left:  ${toRem(15)};
						padding-right:  ${toRem(15)};
					}
				}
				.MuiAccordionSummary-content{
					margin: ${toRem(15)} 0;
				}
				.MuiIconButton-root{
					color: ${p => p.theme.textmenu};
				}
				.MuiAccordion-root{
					box-shadow: none;
					background-color: #F5F4F7;
					border-radius: 5px;
					&:before{
						display: none;
					}
					+ .MuiAccordion-root{
						margin-top: ${toRem(15)};
					}
				}
			}
		}
	}
`;
