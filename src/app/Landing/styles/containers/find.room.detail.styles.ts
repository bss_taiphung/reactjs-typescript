import { createGlobalStyle } from 'styled-components';
import { sizes } from 'styles/media-screens';
import { toRem } from 'styles/StyleConstants';

export const FindRoomDetailPageStyles = createGlobalStyle`
	.section{
		&.section-booking-detail-carousel{
			padding-top: 0;
			position: relative;
			background: ${p => p.theme.bgDetail};
			.carousel-section{
				.slick-slide{
					cursor: grab;
					> div{
						overflow: hidden;
	          position: relative;
					}
				}
				.slick-dots{
					bottom: ${toRem(130)};
					@media(max-width: ${sizes.small}px){
						bottom: ${toRem(30)};
					}
				}
			}
			.content-section{
				margin-top: ${toRem(-120)};
				background: ${p => p.theme.textWhite};
				padding: ${toRem(100)};
				border-top: ${toRem(8)} solid ${p => p.theme.textmenu};
				@media(max-width: ${sizes.small}px){
					padding: ${toRem(15)};
					margin-top: ${toRem(-30)};
				}
				.price-text{
					font-weight: bold;
					color: ${p => p.theme.textBody2};
					font-size: ${toRem(24)};
					.d-block + .d-block{
						margin-top: ${toRem(5)};
					}
				}
				.service-list{
					ul li{
					  width: 25px;
            height: 25px;
						color: ${p => p.theme.textBody2};
						font-size: ${toRem(24)};
						+ li{
						  width: 25px;
              height: 25px;
							margin-left: ${toRem(30)};
							@media(max-width: ${sizes.small}px){
								margin-left: ${toRem(5)};
								margin-top: ${toRem(30)};
							}
						}
					}
				}
				.text{
					img{
						box-shadow: 0px ${toRem(10)} ${toRem(20)} rgba(0, 0, 0, 0.25);
					}
					b{
						display: block;
						color: ${p => p.theme.textBody2};
					}
				}
			}
			.carousel-section,
			.img-section{
				.lazy-load-image-background{
					position: relative;
					overflow: hidden;
	        height: ${toRem(570)};
	        display: -ms-flexbox;
	        display: flex !important;
	        -ms-flex-pack: center;
	        justify-content: center;
	        -ms-flex-align: center;
	        align-items: center;
	        @media(max-width: ${sizes.small}px){
	        	height: ${toRem(250)};
	        }
	        img{
	          height: auto;
            width: 100%;
            position: absolute;
	        }
	      }
			}
		}

		&.section-product{
			&.blog{
				.item-product .content-item {
					.name{
						font-weight: bold;
					}
					.place{
						color: ${p => p.theme.textBody};
						font-size: ${toRem(14)};
						[class^="icon-"], [class*=" icon-"] {
							color: ${p => p.theme.textBody};
							font-size: ${toRem(18)};
						}
					}
				}
			}
			padding-top: ${toRem(50)};
			.item-product{
				position: relative;
				margin-top: ${toRem(30)};
				.catalogue{
				  z-index: 99;
					position: absolute;
					width: auto;
					height: auto;
					font-weight: 600;
					top: ${toRem(15)};
					right: ${toRem(15)};
					padding: ${toRem(12)} ${toRem(18)};
			    background-color: ${p => p.theme.textBody2};
			    color: ${p => p.theme.textWhite};
			    font-size: ${toRem(14)};
				}
				.content-item{
					font-size: ${toRem(14)};
					> div{
						margin-top: ${toRem(15)};
					}
					.price{
						display: inline-block;
						padding: ${toRem(12)} ${toRem(18)};
				    background-color: ${p => p.theme.textmenuActive};
				    color: ${p => p.theme.textWhite};
					}
					.place{
						color: ${p => p.theme.textBody2};
						font-weight: 600;
						[class^="icon-"], [class*=" icon-"] {
							font-size: ${toRem(24)};
							color: ${p => p.theme.textmenuActive};
							margin-right: ${toRem(5)};
						}
					}
					.name{
						color: ${p => p.theme.textmenuActive};
						font-size: ${toRem(24)};
					}
				}
				.img-item{
					overflow: hidden;
          position: relative;
				}
				.lazy-load-image-background{
          height: ${toRem(523)};
          display: -ms-flexbox;
          display: flex !important;
          -ms-flex-pack: center;
          justify-content: center;
          -ms-flex-align: center;
          align-items: center;
          img{
            height: 100%;
            width: auto;
            display: inline-block;
          }
        }
			}
		}
	}
	.list-striped{
    > .row-striped{
      padding: ${toRem(20)};
      background-color: #F4F5F4;
      margin-top: ${toRem(5)};
      &:nth-of-type(odd){
        background-color: #E6E9ED;
      }
    }
  }
`;
