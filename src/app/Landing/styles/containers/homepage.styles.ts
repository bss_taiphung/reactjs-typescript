import { createGlobalStyle } from 'styled-components';
import { sizes } from 'styles/media-screens';
import { toRem } from 'styles/StyleConstants';

export const HomePageStyles = createGlobalStyle`
	.section{
		&.section-intro{
			padding: 0;
			.title-section{
			  color: ${p => p.theme.textTitle};
			}
			.img-section{
				margin-left: ${toRem(-15)};
				@media(max-width: ${sizes.small}px){
					margin-left: 0;
				}
			}
			.content-section{
				padding-right: ${toRem(80)};
				padding-top: ${toRem(15)};
				padding-bottom: ${toRem(15)};
				@media(max-width: ${sizes.small}px){
					padding-right: 0;
				}
			}
		}
		&.section-carousel{
			.title-section{
			  font-size: ${toRem(32)};
			  color: ${p => p.theme.textTitle};
			}
			.content-section{
				margin-right: ${toRem(-15)};
				@media(max-width: ${sizes.small}px){
					margin-right: 0;
				}
			}
			/*.slick-slider.slick-initialized{
				background-color: pink;
				min-height: ${toRem(600)};
				.slick-list, .slick-track{
					min-height: ${toRem(600)};
				}
			}*/
			.slick-slide{
				&:hover{
					.w-slider .lazy-load-image-background: before{
						display: none;
					}
				}
				.w-slider{
					position: relative;
					cursor: grab;
					.lazy-load-image-background{
						position: relative;
						overflow: hidden;
						height: ${toRem(523)};
						display: -ms-flexbox;
				    display: flex;
				    -ms-flex-pack: center;
				    justify-content: center;
				    -ms-flex-align: center;
    				align-items: center;
						&:before{
							content: '';
							position: absolute;
							bottom: 0;
							left: 0;
							width: 100%;
							height: 100%;
							background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.260417) 50%, rgba(0, 0, 0, 0.5) 100%);
						}
						img{
							height: 100%;
							width: auto;
							display: inline-block;
						}
					}
				}
			}
		}
		&.section-product{
			padding-top: ${toRem(50)};
			&.blog{
				.item-product .content-item {
					.name{
						font-weight: bold;

					}
					.place{
						color: ${p => p.theme.textBody};
						font-size: ${toRem(14)};
						[class^="icon-"], [class*=" icon-"] {
							color: ${p => p.theme.textBody};
							font-size: ${toRem(18)};
						}
					}
				}
			}
			.item-product{
				position: relative;
				margin-top: ${toRem(30)};
				.catalogue{
				  z-index: 99;
					position: absolute;
					width: auto;
					height: auto;
					font-weight: 600;
					top: ${toRem(15)};
					right: ${toRem(15)};
					padding: ${toRem(12)} ${toRem(18)};
			    background-color: ${p => p.theme.textBody2};
			    color: ${p => p.theme.textWhite};
			    font-size: ${toRem(14)};
				}
				.content-item{
					font-size: ${toRem(14)};
					> div{
						margin-top: ${toRem(15)};
					}
					.price{
						font-weight: bold;
						display: inline-block;
						padding: ${toRem(12)} ${toRem(18)};
				    background-color: ${p => p.theme.textmenuActive};
				    color: ${p => p.theme.textWhite};
					}
					.place{
						color: ${p => p.theme.textBody2};
						font-weight: 600;
						[class^="icon-"], [class*=" icon-"] {
							font-size: ${toRem(24)};
							color: ${p => p.theme.textmenuActive};
							margin-right: ${toRem(5)};
						}
					}
					.name{
						color: ${p => p.theme.textmenuActive};
						font-size: ${toRem(24)};
					}
				}
				.img-item{
					overflow: hidden;
          position: relative;
				}
				.lazy-load-image-background{
          height: ${toRem(523)};
          display: -ms-flexbox;
          display: flex !important;
          -ms-flex-pack: center;
          justify-content: center;
          -ms-flex-align: center;
          align-items: center;
          img{
            height: 100%;
            width: auto;
            display: inline-block;
          }
        }
			}
		}
		&.section-testimonial{
			.title-section{
			  color: ${p => p.theme.textTitle};
			}
			.item-testimonial{
				text-align: center;
				.wrapper-item{
					max-width: 66.66667%;
					margin: 0 auto;
					@media(max-width: ${sizes.small}px){
						max-width: 80%;
					}
				}
				.img-item{
					overflow: hidden;
					margin: 0 auto;
					margin-top: ${toRem(30)};
					width: ${toRem(65)};
					-webkit-border-radius: 100%;
					-moz-border-radius: 100%;
					border-radius: 100%;
				}
				.text-item{
					color: ${p => p.theme.textBody2};
				}
			}
		}
		&.section-approach{
			padding: 0;
			position: relative;
			color: ${p => p.theme.textWhite};
			.background-section{
				overflow: hidden;
				height: ${toRem(485)};
				img{
					height: 100%;
					width: auto;
				}
			}
			.content-section{
				position: absolute;
				left: 50%;
  			top: 50%;
  			transform: translate(-50%, -50%);
  			width: 100%;
			}
			.title-section{
			  font-size: ${toRem(44)};
			  color: ${p => p.theme.textWhite};
			}
		}
	}
`;
