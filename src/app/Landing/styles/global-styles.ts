import { createGlobalStyle } from 'styled-components';
import { sizes } from 'styles/media-screens';
import {
  fGetDefaultFontSize,
  fGetFontSizeByMediaSize,
  toRem,
} from 'styles/StyleConstants';
/* istanbul ignore next */

export const GlobalStyle = createGlobalStyle`
html,
body {
  height: 100%;
  width: 100%;
}

html {
  font-size: ${fGetDefaultFontSize()};
  @media (max-width: ${sizes.small}px) {
    font-size:  ${fGetFontSizeByMediaSize(sizes.small)};
  }
}

body {
  font-family: 'SVN-Poppins';
  font-size: ${toRem(16)};
  line-height: 1.3;
  color: ${p => p.theme.textBody};
}

#app {
  background-color: @c-fff;
  min-height: 100%;
  min-width: 100%;
}

#root{
  min-width: inherit;
}

img {
  width: 100%;
  height: auto;
}
.mt-10{
  margin-top: ${toRem(10)};
}
.mt-15{
  margin-top: ${toRem(15)};
}
.mt-20{
  margin-top: ${toRem(20)};
}
.mt-30{
  margin-top: ${toRem(30)};
}
.mt-40{
  margin-top: ${toRem(40)};
}
.visible-desktop{
  @media(max-width: ${sizes.small}px){
    display: none !important;
  }
}
.visible-mobi{
  @media(min-width: ${sizes.small + 1}px){
    display: none !important;
  }
}
a{
  color: inherit;
}
a:hover{
  text-decoration: none;
  cursor: pointer;
  color: inherit;
}
button:focus,.btn.focus, .btn:focus, *:focus{
  outline: none ;
  box-shadow: none;
}
.container-fluid{
  padding-left: ${toRem(15)};
  padding-right: ${toRem(15)};
}
.container {
  max-width: ${toRem(1200)};
  padding-left: ${toRem(15)};
  padding-right: ${toRem(15)};
}
.row {
  margin-right: ${toRem(-15)};
  margin-left: ${toRem(-15)};
}
[class^="col-"], [class*=" col-"] {
  padding-right: ${toRem(15)};
  padding-left: ${toRem(15)};
}
.box-shadow{
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.1);
}
.title-section{
  font-weight: bold;
  font-size: ${toRem(44)};
  color: ${p => p.theme.textmenuActive};
  @media(max-width: ${sizes.small}px){
    font-size: ${toRem(32)};
  }
}
.section{
  padding-top: ${toRem(80)};
  padding-bottom: ${toRem(80)};
  @media(max-width: ${sizes.small}px){
    padding-top: ${toRem(40)};
    padding-bottom: ${toRem(40)};
  }
  &.section-title{
    padding-top: ${toRem(100)};
    padding-bottom: 0;
    @media(max-width: ${sizes.small}px){
      padding-top: 0;
    }
  }
}
.btn{
  line-height: 1;
  font-weight: 600;
  border-radius: 0;
  &.btn-blue{
    padding: ${toRem(15)} ${toRem(20)};
    background-color: ${p => p.theme.textmenuActive};
    color: ${p => p.theme.textWhite};
    font-size: ${toRem(16)};
  }
  &.btn-icon{
    padding: ${toRem(30)} ${toRem(30)};
    @media(max-width: ${sizes.small}px){
      padding: ${toRem(20)} ${toRem(15)};
    }
    span{
      font-size: ${toRem(24)};
    }
  }
}
.list-check{
  ul{
    padding: 0;
    list-style: none;
    li{
      position: relative;
      color: ${p => p.theme.textBody2};
      &:before{
        content: '\\e905';
        font-family: 'icon-coliving';
        color: ${p => p.theme.textmenuActive};
        font-size: ${toRem(24)};
        vertical-align: ${toRem(-5)};
        margin-right: ${toRem(5)};
      }
      + li{
        margin-top: ${toRem(5)};
      }
    }
  }
}
.slick-slider{
  .slick-prev, .slick-next{
    width: auto;
    height: auto;
    &:before{
      color: ${p => p.theme.textBody2};
      font-family: 'icon-coliving';
      font-size: ${toRem(52)};
      @media(max-width: ${sizes.small}px){
        font-size: ${toRem(25)};
      }
    }
  }
  .slick-prev:before {
    content: "\\e914";
  }
  .slick-next:before {
    content: "\\e913";
  }
  .slick-next{
    @media(max-width: ${sizes.small}px){
      right: 0;
    }
  }
  .slick-prev{
    @media(max-width: ${sizes.small}px){
      left: 0;
    }
  }
}

.modal-custom{
  @media(max-width: ${sizes.small}px){
    padding: ${toRem(15)};
  }
  .MuiDialog-paperWidthSm{
    max-width: ${toRem(770)};
    border-radius: 16px;
  }
  .MuiDialogTitle-root{
    padding: 0 ${toRem(70)};
    @media(max-width: ${sizes.small}px){
      padding: 0 ${toRem(30)};
    }
    h2{
      line-height: 1.3;
      letter-spacing: normal;
      color: ${p => p.theme.textmenuActive};
      font-weight: bold;
      font-size: ${toRem(24)};
      font-family: 'SVN-Poppins';
    }
  }
  .MuiDialogContent-root{
    padding: ${toRem(70)};
    padding-top: 0;
    @media(max-width: ${sizes.small}px){
      padding: ${toRem(30)};
      padding-top: 0;
    }
  }
}
.form-custom{
  .form-group{
    margin-top: ${toRem(20)};
    .MuiFormLabel-root {
      text-transform: uppercase;
      font-family: 'SVN-Poppins';
      font-size: ${toRem(14)};
      color: ${p => p.theme.textBody2};
    }
    label + *{
      margin-top: ${toRem(15)};
    }
    textarea.MuiInputBase-input{
      width: ${toRem(550)};
      @media(max-width: ${sizes.small}px){
        width: ${toRem(325)};
      }
    }
    .MuiInputBase-input{
      font-family: 'SVN-Poppins';
      color: ${p => p.theme.textBody2};
      position: relative;
      border: 1px solid #ced4da;
      padding: ${toRem(10)};
      border-radius: 4px;
      transition: border-color 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
    }
  }
  .text-muteds{
    font-style: italic;
    color: ${p => p.theme.textMuted};
    font-size: ${toRem(14)};
    margin-top: ${toRem(10)};
  }
  .text-error{
    background-color: transparent;
    color: #b13c3c;
    margin-bottom: 0;
    font-size: ${toRem(12)};
    font-family: 'SVN-Poppins';
  }
  .ql-editor.ql-blank:before {
    font-style: normal;
    font-family: 'SVN-Poppins';
    color: #A9A9A9;
    font-size: ${toRem(16)};
  }
}

.row-pagination {
  margin: auto auto;
  text-align: center;
  .MuiPagination-ul{
    -ms-flex-pack: center;
    justify-content: center;
    -ms-flex-align: center;
    align-items: center;
    li{
      margin: 0 ${toRem(10)};
      button,
      .MuiPaginationItem-root{
        height: ${toRem(30)};
        min-width: ${toRem(30)};
        border-radius: 100%;
        font-family: 'SVN-Poppins';
        font-size: ${toRem(16)};
        border: none;
        background-color: transparent;
        color: ${p => p.theme.textMuted};
        &.Mui-selected{
          font-weight: bold;
          background-color: ${p => p.theme.textmenu};
          color: ${p => p.theme.textWhite};
        }
      }
    }
    li:first-child,
    li:last-child{
      margin: 0 ${toRem(40)};
      button,
      .MuiPaginationItem-root{
        height: ${toRem(40)};
        min-width: ${toRem(40)};
        background-color: rgba(0,0,0,.2);
        border: none;
        .MuiPaginationItem-icon{
          color: ${p => p.theme.textWhite};
        }
      }
    }
  }
}
`;
