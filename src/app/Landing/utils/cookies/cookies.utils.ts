// @ts-ignore
import Cookies from 'js-cookie';
import { DATE_ARRIVAL, DATE_EXPECTED_IN, MINIMUM_STAY } from './constant';

export class CookiesUtils {
  public static get(key: string, defaultValue?: any): any {
    const value = Cookies.get(key);
    if (value) {
      return value;
    } else {
      return defaultValue;
    }
  }

  public static getJSON(key: string, defaultValue?: any): any {
    const value = Cookies.getJSON(key);
    if (value) {
      return value;
    } else {
      return defaultValue;
    }
  }

  public static set(key, value): void {
    Cookies.set(key, value);
  }

  public static remove(key): void {
    Cookies.remove(key);
  }

  /*
   * Implement
   * */
  public static setDateArrival(value: string): void {
    CookiesUtils.set(DATE_ARRIVAL, value);
  }

  public static getDateArrival(): string {
    return CookiesUtils.get(DATE_ARRIVAL);
  }

  public static setDateExpectedIn(value: string): void {
    CookiesUtils.set(DATE_EXPECTED_IN, value);
  }

  public static getDateExpectedIn(): string {
    return CookiesUtils.get(DATE_EXPECTED_IN);
  }

  public static setMinimumStay(value: string): void {
    CookiesUtils.set(MINIMUM_STAY, value);
  }

  public static getMinimumStay(): string {
    return CookiesUtils.get(MINIMUM_STAY);
  }
}
