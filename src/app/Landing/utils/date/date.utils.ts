import moment from 'moment';
import { DISPLAY_DATE_FORMAT } from 'app/Landing/utils/date/constant';

export function fDisplayDateFormat(
  dateString: string | null | undefined,
): string {
  return moment(dateString).format(DISPLAY_DATE_FORMAT);
}
