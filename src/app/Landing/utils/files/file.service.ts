import { IAbstractFile } from 'app/Landing/utils/files/abstract.file.service';
import { LocalFileService } from 'app/Landing/utils/files/local.file.service';

// import { ImgixImageService } from 'app/Landing/utils/files/imgix.image.service';

export class FileService extends LocalFileService implements IAbstractFile {
  //Singleton
  private static instance: FileService;

  public static getInstance(): FileService {
    if (!FileService.instance) {
      FileService.instance = new FileService();
    }
    return FileService.instance;
  }
}
