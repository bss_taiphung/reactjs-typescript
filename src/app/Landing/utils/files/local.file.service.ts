import { IAbstractFile } from 'app/Landing/utils/files/abstract.file.service';

export class LocalFileService implements IAbstractFile {
  buildURL(path: string | undefined): string {
    if (path) {
      return process.env.REACT_APP_FILE_LOCAL_URL + path;
    } else {
      return '';
    }
  }
}
