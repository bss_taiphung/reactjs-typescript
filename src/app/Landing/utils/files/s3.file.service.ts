import { IAbstractFile } from 'app/Landing/utils/files/abstract.file.service';

export class S3FileService implements IAbstractFile {
  buildURL(path: string | undefined): string {
    if (path) {
      return process.env.REACT_APP_FILE_S3_URL + path;
    } else {
      return '';
    }
  }
}
