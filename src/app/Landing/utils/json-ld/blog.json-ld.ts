import { IBlogInterface } from 'app/Landing/interfaces/i.blog.interface';
import { NewsArticle } from 'schema-dts';
import { helmetJsonLdProp } from 'react-schemaorg';
import * as _ from 'lodash';

export class BlogJsonLd {
  dto: IBlogInterface | null = null;

  private static instance: BlogJsonLd;

  public static getInstance(): BlogJsonLd {
    if (!BlogJsonLd.instance) {
      BlogJsonLd.instance = new BlogJsonLd();
    }
    return BlogJsonLd.instance;
  }

  public setDto(dto: IBlogInterface | null): void {
    BlogJsonLd.getInstance().dto = dto;
  }

  public getDto(): IBlogInterface | null {
    return _.get(BlogJsonLd.getInstance(), 'dto', null);
  }

  public toJsonLd(): any {
    return [
      helmetJsonLdProp<NewsArticle>({
        '@context': 'https://schema.org',
        '@type': 'NewsArticle',
        mainEntityOfPage: {
          '@type': 'WebPage',
          '@id': _.get(window, 'location.host', ''),
        },
        headline: _.get(
          BlogJsonLd.getInstance().getDto(),
          'translated.name',
          '',
        ),
        image: [_.get(BlogJsonLd.getInstance().getDto(), 'image.path', '')],
        datePublished: _.get(
          BlogJsonLd.getInstance().getDto(),
          'createdAt',
          '',
        ),
        dateModified: _.get(BlogJsonLd.getInstance().getDto(), 'updatedAt', ''),
      }),
    ];
  }
}
