import { NewsArticle } from 'schema-dts';
import { helmetJsonLdProp } from 'react-schemaorg';
import * as _ from 'lodash';
import {
  IBuildingImageInterface,
  IBuildingInterface,
} from 'app/Landing/interfaces/i.building.interface';

export class BuildingJsonLd {
  dto: IBuildingInterface | null = null;

  private static instance: BuildingJsonLd;

  public static getInstance(): BuildingJsonLd {
    if (!BuildingJsonLd.instance) {
      BuildingJsonLd.instance = new BuildingJsonLd();
    }
    return BuildingJsonLd.instance;
  }

  public setDto(dto: IBuildingInterface | null): void {
    BuildingJsonLd.getInstance().dto = dto;
  }

  public getDto(): IBuildingInterface | null {
    return _.get(BuildingJsonLd.getInstance(), 'dto', null);
  }

  public toJsonLd(): any {
    return [
      helmetJsonLdProp<NewsArticle>({
        '@context': 'https://schema.org',
        '@type': 'NewsArticle',
        mainEntityOfPage: {
          '@type': 'WebPage',
          '@id': _.get(window, 'location.host', ''),
        },
        headline: _.get(BuildingJsonLd.getInstance().getDto(), 'name', ''),
        image: _.get(BuildingJsonLd.getInstance().getDto(), 'images', []).map(
          (item: IBuildingImageInterface) => {
            return _.get(item, 'image.path', '');
          },
        ),
        datePublished: _.get(
          BuildingJsonLd.getInstance().getDto(),
          'createdAt',
          '',
        ),
        dateModified: _.get(
          BuildingJsonLd.getInstance().getDto(),
          'updatedAt',
          '',
        ),
      }),
    ];
  }
}
