import { NewsArticle } from 'schema-dts';
import { helmetJsonLdProp } from 'react-schemaorg';
import * as _ from 'lodash';
import {
  IRoomTypeImageInterface,
  IRoomTypeInterface,
} from 'app/Landing/interfaces/i.room.type.interface';

export class RoomTypeJsonLd {
  dto: IRoomTypeInterface | null = null;

  private static instance: RoomTypeJsonLd;

  public static getInstance(): RoomTypeJsonLd {
    if (!RoomTypeJsonLd.instance) {
      RoomTypeJsonLd.instance = new RoomTypeJsonLd();
    }
    return RoomTypeJsonLd.instance;
  }

  public setDto(dto: IRoomTypeInterface | null): void {
    RoomTypeJsonLd.getInstance().dto = dto;
  }

  public getDto(): IRoomTypeInterface | null {
    return _.get(RoomTypeJsonLd.getInstance(), 'dto', null);
  }

  public toJsonLd(): any {
    return [
      helmetJsonLdProp<NewsArticle>({
        '@context': 'https://schema.org',
        '@type': 'NewsArticle',
        mainEntityOfPage: {
          '@type': 'WebPage',
          '@id': _.get(window, 'location.host', ''),
        },
        headline: _.get(RoomTypeJsonLd.getInstance().getDto(), 'name', ''),
        image: _.get(RoomTypeJsonLd.getInstance().getDto(), 'images', []).map(
          (item: IRoomTypeImageInterface) => {
            return _.get(item, 'image.path', '');
          },
        ),
        datePublished: _.get(
          RoomTypeJsonLd.getInstance().getDto(),
          'createdAt',
          '',
        ),
        dateModified: _.get(
          RoomTypeJsonLd.getInstance().getDto(),
          'updatedAt',
          '',
        ),
      }),
    ];
  }
}
