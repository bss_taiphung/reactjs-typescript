/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import React from 'react';
import { colors } from '@material-ui/core';
import BarChartIcon from '@material-ui/icons/BarChart';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import ChatIcon from '@material-ui/icons/ChatOutlined';
import CodeIcon from '@material-ui/icons/Code';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import FolderIcon from '@material-ui/icons/FolderOutlined';
import HomeIcon from '@material-ui/icons/HomeOutlined';
import ListAltIcon from '@material-ui/icons/ListAlt';
import LockOpenIcon from '@material-ui/icons/LockOpenOutlined';
import MailIcon from '@material-ui/icons/MailOutlined';
import PresentToAllIcon from '@material-ui/icons/PresentToAll';
import PeopleIcon from '@material-ui/icons/PeopleOutlined';
import PersonIcon from '@material-ui/icons/PersonOutlined';
import ReceiptIcon from '@material-ui/icons/ReceiptOutlined';
import SettingsIcon from '@material-ui/icons/SettingsOutlined';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import { Label } from 'app/Dashboard/components';
import routeLinksDashboard from '../../../../Dashboard/config/routesLinks';

export default [
  {
    title: 'Pages',
    pages: [
      {
        title: 'Overview',
        href: routeLinksDashboard.overview,
        icon: HomeIcon,
      },
      {
        title: 'Dashboards',
        href: routeLinksDashboard.dashboardRoot,
        icon: DashboardIcon,
        children: [
          {
            title: 'Default',
            href: routeLinksDashboard.dashboardDefault,
          },
          {
            title: 'Analytics',
            href: routeLinksDashboard.analytics,
          },
        ],
      },
      {
        title: 'Management',
        href: '/management',
        icon: BarChartIcon,
        children: [
          {
            title: 'Customers',
            href: routeLinksDashboard.managementCustomers,
          },
          {
            title: 'Customer Details',
            href: routeLinksDashboard.managementCustomers
              .replace(':id', '1')
              .replace(':tab', 'summary'),
          },
          {
            title: 'Projects',
            href: routeLinksDashboard.managementProjects,
          },
          {
            title: 'Orders',
            href: routeLinksDashboard.managementOrders,
          },
          {
            title: 'Order Details',
            href: routeLinksDashboard.managementCustomersId.replace(':id', '1'),
          },
        ],
      },
      {
        title: 'Social Feed',
        href: routeLinksDashboard.socialFeed,
        icon: PeopleIcon,
      },
      {
        title: 'Profile',
        href: routeLinksDashboard.profile,
        icon: PersonIcon,
        children: [
          {
            title: 'Timeline',
            href: routeLinksDashboard.profileIdTab
              .replace(':id', '1')
              .replace(':tab', 'timeline'),
          },
          {
            title: 'Connections',
            href: routeLinksDashboard.profileIdTab
              .replace(':id', '1')
              .replace(':tab', 'connections'),
          },
          {
            title: 'Projects',
            href: routeLinksDashboard.profileIdTab
              .replace(':id', '1')
              .replace(':tab', 'projects'),
          },
          {
            title: 'Reviews',
            href: routeLinksDashboard.profileIdTab
              .replace(':id', '1')
              .replace(':tab', 'reviews'),
          },
        ],
      },
      {
        title: 'Project',
        href: routeLinksDashboard.projects,
        icon: FolderIcon,
        children: [
          {
            title: 'Browse',
            href: routeLinksDashboard.projects,
          },
          {
            title: 'Create',
            href: routeLinksDashboard.projectsCreate,
          },
          {
            title: 'Overview',
            href: routeLinksDashboard.projectsIdTab
              .replace(':id', '1')
              .replace(':tab', 'overview'),
          },
          {
            title: 'Files',
            href: routeLinksDashboard.projectsIdTab
              .replace(':id', '1')
              .replace(':tab', 'files'),
          },
          {
            title: 'Activity',
            href: routeLinksDashboard.projectsIdTab
              .replace(':id', '1')
              .replace(':tab', 'activity'),
          },
          {
            title: 'Subscribers',
            href: routeLinksDashboard.projectsIdTab
              .replace(':id', '1')
              .replace(':tab', 'subscribers'),
          },
        ],
      },
      {
        title: 'Invoice',
        href: routeLinksDashboard.invoicesId.replace(':id', '1'),
        icon: ReceiptIcon,
      },
      {
        title: 'Kanban Board',
        href: routeLinksDashboard.kanbanBoard,
        icon: ListAltIcon,
      },
      {
        title: 'Mail',
        href: routeLinksDashboard.mail,
        icon: MailIcon,
        label: () => (
          <Label color={colors.red[500]} shape="rounded">
            2
          </Label>
        ),
      },
      {
        title: 'Chat',
        href: routeLinksDashboard.chat,
        icon: ChatIcon,
        label: () => (
          <Label color={colors.red[500]} shape="rounded">
            4
          </Label>
        ),
      },
      {
        title: 'Calendar',
        href: routeLinksDashboard.calendar,
        icon: CalendarTodayIcon,
        label: () => <Label color={colors.green[500]}>New</Label>,
      },
      {
        title: 'Settings',
        href: routeLinksDashboard.settings,
        icon: SettingsIcon,
        children: [
          {
            title: 'General',
            href: routeLinksDashboard.settingsTab.replace(':tab', 'general'),
          },
          {
            title: 'Subscription',
            href: routeLinksDashboard.settingsTab.replace(
              ':tab',
              'subscription',
            ),
          },
          {
            title: 'Notifications',
            href: routeLinksDashboard.settingsTab.replace(
              ':tab',
              'notifications',
            ),
          },
          {
            title: 'Security',
            href: routeLinksDashboard.settingsTab.replace(':tab', 'security'),
          },
        ],
      },
      {
        title: 'Authentication',
        href: routeLinksDashboard.authRoot,
        icon: LockOpenIcon,
        children: [
          {
            title: 'Login',
            href: routeLinksDashboard.authLogin,
          },
          {
            title: 'Register',
            href: routeLinksDashboard.authRegister,
          },
        ],
      },
      {
        title: 'Errors',
        href: routeLinksDashboard.errors,
        icon: ErrorIcon,
        children: [
          {
            title: 'Error 401',
            href: routeLinksDashboard.errors401,
          },
          {
            title: 'Error 404',
            href: routeLinksDashboard.errors404,
          },
          {
            title: 'Error 500',
            href: routeLinksDashboard.errors500,
          },
        ],
      },
    ],
  },
  {
    title: 'Support',
    pages: [
      {
        title: 'Presentation',
        href: routeLinksDashboard.presentation,
        icon: PresentToAllIcon,
      },
      {
        title: 'Getting started',
        href: routeLinksDashboard.gettingStarted,
        icon: CodeIcon,
      },
      {
        title: 'Changelog',
        href: routeLinksDashboard.changelog,
        icon: ViewModuleIcon,
        label: () => <Label color={colors.blue['500']}>v1.2.0</Label>,
      },
    ],
  },
];
