import React, { Fragment, memo, Suspense, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { LinearProgress } from '@material-ui/core';
import { Helmet } from 'react-helmet-async';
import { reducer, sliceKey } from './slice';
import { selectDashboardLayout } from './selectors';
import { dashboardLayoutSaga } from './saga';
import 'app/Dashboard/mixins/chartjs';
import 'app/Dashboard/mixins/moment';
import 'app/Dashboard/mixins/validate';
import 'app/Dashboard/mixins/prismjs';
import 'app/Dashboard/mock';
import 'app/Dashboard/assets/scss/index.scss';
import { NavBar, TopBar, ChatBar } from './components';
import {
  ScrollReset,
  GoogleAnalytics,
  CookiesNotification,
} from 'app/Dashboard/components';
import { makeStyles } from '@material-ui/styles';
import { Switch } from 'react-router-dom';
interface Props {
  route: any;
}
const useStyles = makeStyles(() => ({
  root: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
  },
  topBar: {
    zIndex: 2,
    position: 'relative',
  },
  container: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
  },
  navBar: {
    zIndex: 3,
    width: 256,
    minWidth: 256,
    flex: '0 0 auto',
  },
  content: {
    overflowY: 'auto',
    flex: '1 1 auto',
  },
}));
export const DashboardLayout = memo((props: Props) => {
  const { route } = props;
  const classes = useStyles();
  const [openNavBarMobile, setOpenNavBarMobile] = useState(false);

  const handleNavBarMobileOpen = () => {
    setOpenNavBarMobile(true);
  };

  const handleNavBarMobileClose = () => {
    setOpenNavBarMobile(false);
  };

  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: dashboardLayoutSaga });

  const dashboardLayout = useSelector(selectDashboardLayout);
  const dispatch = useDispatch();

  return (
    <Fragment>
      <Helmet defaultTitle="Dashboard">
        <meta name="description" content="Dashboard" />
      </Helmet>
      <div className={classes.root}>
        <TopBar
          className={classes.topBar}
          onOpenNavBarMobile={handleNavBarMobileOpen}
        />
        <div className={classes.container}>
          <NavBar
            className={classes.navBar}
            onMobileClose={handleNavBarMobileClose}
            openMobile={openNavBarMobile}
          />
          <main className={classes.content}>
            {/*<Suspense fallback={<LinearProgress />}>*/}
            {/*  {renderRoutes(route.routes)}*/}
            {/*</Suspense>*/}
            <Switch>{renderRoutes(route.routes)}</Switch>
          </main>
        </div>
        <ChatBar />
      </div>
      <ScrollReset />
      <GoogleAnalytics />
      <CookiesNotification />
    </Fragment>
  );
});
