/**
 *
 * Asynchronously loads the component for App
 *
 */

import { lazyLoad } from 'utils/loadable';

export const LandingLayout = lazyLoad(
  () => import('./index'),
  module => module.LandingLayout,
);
