import React, { memo } from 'react';
import { FooterStyles } from 'app/Landing/styles/components/footer.styles';
import { translations } from 'app/Landing/locales/i18n';
import { useTranslation } from 'react-i18next';

interface Props {}

export const Footer = memo((props: Props) => {
  const { t, i18n } = useTranslation();
  return (
    <>
      <div className="footer-wrapper">
        <div className="container">
          <div className="navbar navbar-footer">
            <a className="navbar-brand" href="#">
              Logo
            </a>
            <nav className="nav mr-auto">
              <a className="nav-link" href="#">
                {t(translations.header.home)}
              </a>
              <a className="nav-link" href="#">
                {t(translations.header.blog)}
              </a>
              <a className="nav-link" href="#">
                {t(translations.header.faq)}
              </a>
              <a className="nav-link" href="#">
                {t(translations.header.contact)}
              </a>
            </nav>
            <nav className="nav">
              <a className="nav-link" href="#">
                <span className="icon-instagram"></span>
              </a>
              <a className="nav-link" href="#">
                <span className="icon-twitter"></span>
              </a>
              <a className="nav-link" href="#">
                <span className="icon-facebook"></span>
              </a>
            </nav>
          </div>
          <div className="navbar navbar-footer">
            <div className="mr-auto">
              <a> {t(translations.footer.terms)}</a> |{' '}
              <a>{t(translations.footer.policy)}</a>
            </div>
            <div className="text-sm-right">
              {t(translations.footer.copyright, {
                year: new Date().getFullYear(),
              })}
            </div>
          </div>
        </div>
      </div>
      <FooterStyles />
    </>
  );
});
