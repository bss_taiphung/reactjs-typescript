import routeLinks from 'app/Landing/config/routesLinks';
import React, { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { HeaderStyles } from 'app/Landing/styles/components/header.styles';
import 'bootstrap/js/src/dropdown';
import 'bootstrap/js/src/collapse';
import history from 'utils/history';
import { translationList, translations } from 'app/Landing/locales/i18n';
import * as _ from 'lodash';
import queryString from 'query-string';
import { LazyLoadImage } from 'app/Landing/components/LazyLoadImage';
import { IBlogCategoryInterface } from 'app/Landing/interfaces/i.blog.category.interface';

interface Props {
  dtoBlogCategories: IBlogCategoryInterface[];
}

export const SelectLang = memo(() => {
  const { t, i18n } = useTranslation();
  const handleLanguageChange = (lng: string) => {
    i18n.changeLanguage(lng);
  };
  const activeLang = _.find(translationList, { value: i18n.language });
  return (
    <ul className="navbar-nav">
      <li className="nav-item dropdown dropdown-language">
        <a
          className="nav-link dropdown-toggle"
          id="dropdownLanguage"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          <LazyLoadImage src={activeLang.icon} />
          {activeLang.label}
        </a>
        <div className="dropdown-menu" aria-labelledby="dropdownLanguage">
          {translationList.map((item, index) => (
            <a
              key={index}
              className="dropdown-item"
              onClick={() => handleLanguageChange(item.value)}
            >
              <LazyLoadImage src={item.icon} />
              {item.label}
            </a>
          ))}
        </div>
      </li>
    </ul>
  );
});
export const Header = memo((props: Props) => {
  const { t, i18n } = useTranslation();
  const { dtoBlogCategories } = props;
  return (
    <>
      <div className={`header-wrapper`}>
        <nav className="navbar navbar-expand-sm navbar-light navbar-coliving">
          <div className="container">
            <a
              className="navbar-brand"
              onClick={() => {
                history.push({
                  pathname: routeLinks.landingRoot,
                });
              }}
            >
              Logo
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarsColiving"
              aria-controls="navbarsColiving"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarsColiving">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <a
                    className="nav-link"
                    onClick={() => {
                      history.push({
                        pathname: routeLinks.landingRoot,
                      });
                    }}
                  >
                    {t(translations.header.home)}
                  </a>
                </li>
                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    id="dropdownBlog"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    {t(translations.header.blog)}
                  </a>
                  <div className="dropdown-menu" aria-labelledby="dropdownBlog">
                    {dtoBlogCategories.map((item: IBlogCategoryInterface) => (
                      <a
                        key={item.id}
                        className="dropdown-item"
                        onClick={() => {
                          history.push({
                            pathname: routeLinks.blogs,
                            search: queryString.stringify({
                              blogCategoryId: item.id,
                            }),
                          });
                        }}
                      >
                        {item?.translated?.name}
                      </a>
                    ))}
                  </div>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    onClick={() => {
                      history.push({
                        pathname: routeLinks.faq,
                      });
                    }}
                  >
                    {t(translations.header.faq)}
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    onClick={() => {
                      history.push({
                        pathname: routeLinks.contact,
                      });
                    }}
                  >
                    {t(translations.header.contact)}
                  </a>
                </li>
              </ul>
              <SelectLang />
            </div>
          </div>
        </nav>
      </div>
      <HeaderStyles />
    </>
  );
});
