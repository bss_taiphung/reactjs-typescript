import 'app/Landing/locales/i18n';
import 'app/Landing/assets/fatihicon/style.scss';
import 'app/Landing/assets/coliving-icon/style.scss';
import 'app/Landing/assets/_fonts.scss';
import 'app/Landing/assets/_variable.scss';
import { Footer } from './components/Footer';
import { Header } from './components/Header';
import { ScrollReset } from 'app/Landing/components/ScrollReset';
import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { Switch } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import { GlobalStyle } from 'app/Landing/styles/global-styles';
import { AppStyles } from 'app/Landing/styles/containers/app.styles';
import { BlogCategoryDtoBuilder } from 'app/Landing/services/builder/blog.category.dto.builder';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { appSaga } from './saga';
import { selectApp, selectAppLoading } from './selectors';
import {
  actions,
  fGetBlogCategoriesRequestSlice,
  reducer,
  sliceKey,
} from './slice';

interface Props {
  route: any;
}

export const LandingLayout = memo((props: Props) => {
  const { route } = props;
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: appSaga });

  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();

  const { dtoBlogCategories } = useSelector(selectApp);
  BlogCategoryDtoBuilder.getInstance().setLocale(i18n.language);
  BlogCategoryDtoBuilder.getInstance().setDtoBlogCategories(dtoBlogCategories);

  // variable in store
  // const loading = useSelector(selectAppLoading);
  useEffect(() => {
    (async () => {
      await dispatch(fGetBlogCategoriesRequestSlice());
    })();
    return () => {
      dispatch(actions.clearData());
    };
  }, []);

  return (
    <>
      <Helmet defaultTitle="Coliving">
        <meta name="description" content="Coliving" />
      </Helmet>
      <Header
        dtoBlogCategories={BlogCategoryDtoBuilder.getInstance().getDtoBlogCategories()}
      />
      <Switch>{renderRoutes(route.routes)}</Switch>
      <Footer />
      <ScrollReset />
      <AppStyles />
      <GlobalStyle />
      <ToastContainer
        closeButton={false}
        hideProgressBar
        className="my-toast-container"
        autoClose={3000}
        position={toast.POSITION.BOTTOM_CENTER}
      />
    </>
  );
});
