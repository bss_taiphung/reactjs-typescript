import { AxiosPost } from 'utils/axios/axiosPost';
import apiLinks, {
  RESOURCE_BLOG_CATEGORY_NAME,
} from 'app/Landing/config/apiLinks';

export function fGetBlogCategoriesRequestSaga() {
  return AxiosPost(apiLinks[RESOURCE_BLOG_CATEGORY_NAME].findAll, {});
}

export function* appSaga() {}
