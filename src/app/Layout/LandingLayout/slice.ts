import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';
import { fGetBlogCategoriesRequestSaga } from './saga';
import { IBlogCategoryInterface } from 'app/Landing/interfaces/i.blog.category.interface';
import { set } from 'lodash';
// The initial state of the App container

export const initialState: ContainerState = {
  loading: false,
  error: false,
  dtoBlogCategories: [],
};

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    clearData: () => initialState,
    showLoading(state, action: PayloadAction<boolean>) {
      set(state, 'isLoading', action.payload);
    },
    setDtoBlogCategories(
      state,
      action: PayloadAction<IBlogCategoryInterface[]>,
    ) {
      set(state, 'dtoBlogCategories', action.payload);
    },
  },
});

export const { actions, reducer, name: sliceKey } = appSlice;

export function fGetBlogCategoriesRequestSlice() {
  return async dispatch => {
    dispatch(actions.showLoading(true));
    try {
      const data = await fGetBlogCategoriesRequestSaga();
      dispatch(actions.setDtoBlogCategories(data.data));
      dispatch(actions.showLoading(false));
      return data;
    } catch (error) {
      dispatch(actions.showLoading(false));
    }
  };
}
