/* --- STATE --- */
import { IBlogCategoryInterface } from 'app/Landing/interfaces/i.blog.category.interface';

export interface AppState {
  loading: boolean;
  error: any;
  dtoBlogCategories: IBlogCategoryInterface[];
}

export interface IFieldValue {
  field: string;
  value: any;
}

export type ContainerState = AppState;
