import React, { Fragment } from 'react';
import { renderRoutes } from 'react-router-config';
import PropTypes from 'prop-types';

const SubRoutesLayout = props => {
  const { route } = props;

  return <Fragment>{renderRoutes(route.routes)}</Fragment>;
};

SubRoutesLayout.propTypes = {
  route: PropTypes.object,
};

export default SubRoutesLayout;
