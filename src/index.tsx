import 'sanitize.css/sanitize.css';
import * as React from 'react';
import { renderRoutes } from 'react-router-config';
import { SnackbarProvider } from 'notistack';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import * as ReactDOM from 'react-dom';
import { HelmetProvider } from 'react-helmet-async';
import { Provider } from 'react-redux';
import * as serviceWorker from 'serviceWorker';
import { configureAppStore } from 'store/configureStore';
import { LandingThemeProvider } from 'styles/theme/LandingThemeProvider';
import { DashboardThemeProvider } from 'styles/theme/DashboardThemeProvider';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Router } from 'react-router';
import history from 'utils/history';
import { Switch } from 'react-router-dom';
import landingRoutes from 'app/Landing/landing.routes';
import dashboardRoutes from 'app/Dashboard/dashboard.routes';

const store = configureAppStore();
const MOUNT_NODE = document.getElementById('root') as HTMLElement;

const ConnectedApp = () => (
  <Provider store={store}>
    <LandingThemeProvider>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <SnackbarProvider maxSnack={3}>
          <HelmetProvider>
            <Router history={history}>
              <Switch>{renderRoutes([...landingRoutes])}</Switch>
            </Router>
          </HelmetProvider>
        </SnackbarProvider>
      </MuiPickersUtilsProvider>
    </LandingThemeProvider>
    <DashboardThemeProvider>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <SnackbarProvider maxSnack={3}>
          <HelmetProvider>
            <Router history={history}>
              <Switch>{renderRoutes([...dashboardRoutes])}</Switch>
            </Router>
          </HelmetProvider>
        </SnackbarProvider>
      </MuiPickersUtilsProvider>
    </DashboardThemeProvider>
  </Provider>
);

const render = () => {
  ReactDOM.render(<ConnectedApp />, MOUNT_NODE);
};

if (module.hot) {
  module.hot.accept(
    [
      'types',
      './app/Dashboard/dashboard.routes.ts',
      './app/Dashboard/components/',
      // './app/Landing',
      './app/Layout/LandingLayout',
      './app/Layout/DashboardLayout',
    ],
    () => {
      ReactDOM.unmountComponentAtNode(MOUNT_NODE);
      render();
    },
  );
}

render();
serviceWorker.unregister();
