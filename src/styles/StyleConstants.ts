export enum StyleConstants {
  DF_FS = 16,
}

export function toRem(pixel: number): string {
  return `${pixel / StyleConstants.DF_FS}rem`;
}

export function fGetDefaultFontSize(): string {
  return `${(StyleConstants.DF_FS / 1440) * 100}vw`;
}

export function fGetFontSizeByMediaSize(mediaSize: number): string {
  return `${(StyleConstants.DF_FS / 375) * 100}vw`;
}
