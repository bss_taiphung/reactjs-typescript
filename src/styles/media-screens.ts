// Update your breakpoints if you want
export const sizes = {
  small: 575,
  medium: 1024,
  large: 1440,
  xlarge: 1920,
};
