import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from 'app/Dashboard/theme';
export const DashboardThemeProvider = (props: {
  children: React.ReactChild;
}) => {
  return (
    <ThemeProvider theme={theme}>
      {React.Children.only(props.children)}
    </ThemeProvider>
  );
};
