import React from 'react';
import { ThemeProvider } from 'styled-components';
import { useSelector } from 'react-redux';
import { reducer, selectTheme, themeSliceKey } from './slice';
import { useInjectReducer } from 'redux-injectors';

export const LandingThemeProvider = (props: { children: React.ReactChild }) => {
  useInjectReducer({ key: themeSliceKey, reducer: reducer });

  const theme = useSelector(selectTheme);
  return (
    <ThemeProvider theme={theme}>
      {React.Children.only(props.children)}
    </ThemeProvider>
  );
};
