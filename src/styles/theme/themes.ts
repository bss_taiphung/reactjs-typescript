const lightTheme = {
  primary: 'rgba(215,113,88,1)',
  text: 'rgba(58,52,51,1)',
  textSecondary: 'rgba(58,52,51,0.7)',
  background: 'rgba(255,255,255,1)',
  backgroundVariant: 'rgba(251,249,249,1)',
  border: 'rgba(58,52,51,0.12)',
  borderLight: 'rgba(58,52,51,0.05)',
  ui1ColorFillColor1: '#2a2c30',
  ui1ColorFillColor2: '#03c3ec',
  ui1ColorFillColor3: '#feae43',
  ui1ColorFillColor4: '#f85c1e',
  ui1ColorFillColor5: '#168b99',
  ui2GreysGrey1: '#4a505b',
  ui2GreysGrey2: '#778699',
  ui2GreysGrey3: '#d7e1ea',
  ui2GreysGrey4: '#f0f4f8',
  ui2GreysGrey5: '#fafbfd',
  uiWhite: '#ffffff',
  ui4ServiceColorsError: '#ffffff',
  darkGrey: '#2a2c30',
  ui2ColorFillColor2: '#f73838',
  //Coliving
  textBody: '#737B7D',
  textBody2: '#323232',
  textmenu: '#06244F',
  textmenuActive: '#3C64B1',
  textWhite: '#ffffff',
  textBlack: '#000000',
  textTitle: '#373F41',
  bgDetail: '#F8F8F8',
  textMuted: '#A9AAAD',
};

const darkTheme: Theme = Object.assign({}, lightTheme);

export type Theme = typeof lightTheme;

export const themes = {
  light: lightTheme,
  dark: darkTheme,
};
