import { AxiosRequestConfig } from 'axios';

export interface IMyAxiosRequestConfig extends AxiosRequestConfig {
  customParameter?: any;
  showPopup?: boolean;
}
