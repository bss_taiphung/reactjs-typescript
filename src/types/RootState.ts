import { AppState } from 'app/Layout/LandingLayout/types';
import { ThemeState } from 'styles/theme/types';
import { HomePageState } from 'app/Landing/containers/HomePage/types';
import { FindYourHomeArrivalPageState } from 'app/Landing/containers/FindYourHomeArrivalPage/types';
import { FindYourHomeMinimumStayPageState } from 'app/Landing/containers/FindYourHomeMinimumStayPage/types';
import { FindYourHomeRoomsForRentPageState } from 'app/Landing/containers/FindYourHomeRoomsForRentPage/types';
import { FindYourHomeRoomsAppointmentSummaryPageState } from 'app/Landing/containers/FindYourHomeRoomsAppointmentSummaryPage/types';
import { FaqPageState } from 'app/Landing/containers/FaqPage/types';
import { ContactPageState } from 'app/Landing/containers/ContactPage/types';
import { BlogsPageState } from 'app/Landing/containers/BlogsPage/types';
import { BlogDetailPageState } from 'app/Landing/containers/BlogDetailPage/types';
import { DashboardLayoutState } from 'app/Layout/DashboardLayout/types';
// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly

/*
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
  Properties are optional because they are injected when the components are mounted sometime in your application's life.
  So, not available always
*/
export interface RootState {
  app?: AppState;
  theme?: ThemeState;
  homePage?: HomePageState;
  findYourHomeArrivalPage?: FindYourHomeArrivalPageState;
  findYourHomeMinimumStayPage?: FindYourHomeMinimumStayPageState;
  findYourHomeRoomsForRentPage?: FindYourHomeRoomsForRentPageState;
  findYourHomeRoomsAppointmentSummaryPage?: FindYourHomeRoomsAppointmentSummaryPageState;
  faqPage?: FaqPageState;
  contactPage?: ContactPageState;
  blogsPage?: BlogsPageState;
  blogDetailPage?: BlogDetailPageState;
  dashboardLayout?: DashboardLayoutState;
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
}
