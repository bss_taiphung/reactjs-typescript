import { RootState } from './RootState';
import { IMyAxiosRequestConfig } from './AxiosState';

export type { RootState, IMyAxiosRequestConfig };
