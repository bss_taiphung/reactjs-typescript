const path = require('path');
const fs = require('fs');
const axios = require('axios');
const url =
  process.env.NODE_ENV === 'production'
    ? 'https://galaxycine.vn/'
    : 'http://coliving-api.web.beesightsoft.com/api/sitemap/';
axios.get(url + 'sitemap.xml').then(data => {
  const dest = path.resolve('./build', 'sitemap.xml');
  fs.writeFileSync(dest, data.data);
});

axios.get(url + 'blogs.xml').then(data => {
  const dest = path.resolve('./build', 'blogs.xml');
  fs.writeFileSync(dest, data.data);
});
axios.get(url + 'roomTypes.xml').then(data => {
  const dest = path.resolve('./build', 'roomTypes.xml');
  fs.writeFileSync(dest, data.data);
});
